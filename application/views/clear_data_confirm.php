<h2>Please confirm you want to delete the following data:</h2>

<br/>All sales records for year: <?php echo $year;?> and month: <?php echo $month;?> will be deleted.

<br/><br/>The total number of sales records that will be deleted is: <?php echo $rs; ?>

<?php echo form_open('process_files/clear_data'); ?> 
<br/>
<input type="hidden" name="action" 	value="clear_data"></input>
<input type="hidden" name="year"	value="<?php echo $year;?>"></input>
<input type="hidden" name="month"	value="<?php echo $month;?>"></input>
<input type="submit" value="Confirm"></input>

<?php echo form_close(); ?>