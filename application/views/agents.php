<link type="text/css" href="<?php echo(base_url());?>css/jquery.ui.all.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo(base_url());?>jquery/ui/jquery-ui-1.8.5.custom.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>jquery/jquery.tablesorter.min.js"></script> 

<script type="text/javascript">	 
	$(document).ready(function(){

		$("#addBtn").click(function() {
			//alert("Click");
			window.location = '<?php echo(base_url());?>agents/add';
			
		});

		//We need to use live here to allow multiple password updates
		$("[name=password]").live('focus',function() {
			var id=($(this).attr("id"));
			var val=$(this).val();
			if(val=="******") {
				//$(this).val("");
				$("#pwd"+id).html('<table border="0"><tr><td>New password    :</td><td><input type="password" name="password1'+id+'"></td></tr><tr><td>Confirm password   :</td><td><input type="password" name="password2'+id+'"></td></tr></table>');
				//We enable the save btn
				if(id!=1) {
					$("#saveBtn"+id).css("background-color","#EFFFFF");
					//$("#updateBtn"+id).css("font-weight","bold");
					$("#saveBtn"+id).removeAttr("disabled");
				}
			}
		});

		$("#myTable").tablesorter(); 
			
		$(".mytext").keyup(function() {
			var id=($(this).attr("id"));
			$("#saveBtn"+id).css("background-color","#EFFFFF");
			//$("#updateBtn"+id).css("font-weight","bold");
			$("#saveBtn"+id).removeAttr("disabled");
		});		

		$(".red").click(function() {
			var id=($(this).attr("id"));
			if($(this).val()=="Save") {
				id=id.replace("saveBtn","");
				//alert("We save ID:" + id);
				
				var hide_pwd = false;

				if($('#'+id+' [name=password]').length == 0) {
					//we check passwords
					var pwd1 = $('[name=password1'+id+']').val();
					var pwd2 = $('[name=password2'+id+']').val();
					if(pwd1.length == 0) {
						alert("Please provide a new password.");
						return;
					}
					if(pwd1 != pwd2) {
						alert("The passwords must be equal.");
						return;
					}
					var hide_pwd = true;	 
				} else {
					var pwd = $('#'+id+' [name=password]').val();
				}			

				if(hide_pwd == true) { //We set the password
					$("#pwd"+id).html('<input style="width: 100px" id="'+id+'" class="mytext" type="text" name="password" value="******"></input>');				
					var data = 
						'id='+id
						+'&title='+$('#'+id+' [name=title]').val()
						+'&name='+$('#'+id+' [name=name]').val()
						+'&surname='+$('#'+id+' [name=surname]').val()
						+'&telnoh='+$('#'+id+' [name=telnoh]').val()
						+'&physicaladdress1='+$('#'+id+' [name=physicaladdress1]').val()
						+'&physicaladdress2='+$('#'+id+' [name=physicaladdress2]').val()
						+'&physicaladdress3='+$('#'+id+' [name=physicaladdress3]').val()
						+'&physicaladdress4='+$('#'+id+' [name=physicaladdress4]').val()
						+'&physicalcode='+$('#'+id+' [name=physicalcode]').val()
						+'&postaladdress1='+$('#'+id+' [name=postaladdress1]').val()
						+'&postaladdress2='+$('#'+id+' [name=postaladdress2]').val()
						+'&postaladdress3='+$('#'+id+' [name=postaladdress3]').val()
						+'&postaladdress4='+$('#'+id+' [name=postaladdress4]').val()
						+'&postalcode='+$('#'+id+' [name=postalcode]').val()
						+'&username='+$('#'+id+' [name=username]').val()
						+'&password='+pwd1;
				} else {
				var data = 
					'id='+id
					+'&title='+$('#'+id+' [name=title]').val()
					+'&name='+$('#'+id+' [name=name]').val()
					+'&surname='+$('#'+id+' [name=surname]').val()
					+'&telnoh='+$('#'+id+' [name=telnoh]').val()
					+'&physicaladdress1='+$('#'+id+' [name=physicaladdress1]').val()
					+'&physicaladdress2='+$('#'+id+' [name=physicaladdress2]').val()
					+'&physicaladdress3='+$('#'+id+' [name=physicaladdress3]').val()
					+'&physicaladdress4='+$('#'+id+' [name=physicaladdress4]').val()
					+'&physicalcode='+$('#'+id+' [name=physicalcode]').val()
					+'&postaladdress1='+$('#'+id+' [name=postaladdress1]').val()
					+'&postaladdress2='+$('#'+id+' [name=postaladdress2]').val()
					+'&postaladdress3='+$('#'+id+' [name=postaladdress3]').val()
					+'&postaladdress4='+$('#'+id+' [name=postaladdress4]').val()
					+'&postalcode='+$('#'+id+' [name=postalcode]').val()
					+'&username='+$('#'+id+' [name=username]').val();

				}
				//alert("We"+data);
				$.ajax({
		         	type:"POST",
		         	url:"<?php echo(base_url());?>agents/save",
		            data: data,
		            success: function (html) {		             					
		            	if(html=="TRUE") {		                            		
		            		//we only do this after a good save
		    				$("#saveBtn"+id).css("background-color","#CECEF6");
		    				$("#saveBtn"+id).attr("disabled","1");     		
		             	}
		             	else
			            	alert("Problem saving entry: " + html);		                            	
		            },
	                error: function (err) {
		            	alert("We got the following error:\n"+err.responseText);
		            }
		         });
							
			}
			else
			if($(this).val()=="Delete") {
				id=id.replace("delBtn","");
				//alert("We delete ID:" + id);
				$("#dialog-confirm").text('This item will be permanently deleted Id: '+id+' Are you sure?' );

				$("#dialog-confirm").dialog({
		            resizable: false,
		            height:230,
		            width: 400,
		            modal: true,
		            title: 'Do you want to delete ?',
		            buttons: {
		                'Delete ?': function() {
		                       
		                       
		                        var data = 'id='+id;
		                        $.ajax({
		                            type:"POST",
		                            url:"<?php echo(base_url());?>agents/delete",
		                            data: data,
		                            success: function (html) {		             					
		             					if(html=="TRUE") {		                            		
		             						$("#dialog-confirm").dialog('close');
		             						$("tr[id="+id+"]").hide();
		             					}
		             					else
			             					alert("Problem deleting entry: " + html);
		                            	
		                            },
	                            	error: function (err) {
		                            	alert("We got the following error:\n"+err.responseText);
		                            }
		                        });
		                },
		                Cancel: function() {
		                   $(this).dialog('close');
		                }
		            }
		        });
				
				
			}	
		});

		

		
		
	});
</script>
<br></br>
<h2>Agents</h2>
<div id="dialog-confirm"></div>
<div id="tblAgent">
<input type="button" id="addBtn" value="Add Agent" class="red"></input>
<br></br>
<br></br>
<table border="1" cellpadding="4" id="myTable">
	<thead>
		<tr>
			<th>Agent Code</th>
			<th>Title</th>
			<th>First Name</th>
			<th>Surname</th>
			<th>Phone Number</th>
			<th width=220px>Physical Address</th>
			<th width=220px>Postal Address</th>	
			<th>Username</th>
			<th>Password</th> 	
			<th>&nbsp;</th>
			<th>&nbsp;</th>			
		</tr>
	</thead>
	<tbody>
<?php

foreach($all_agents as $agent) { ?>
		<tr id="<?php print $agent["agentcode"];?>">
			<td><a href="<?php echo(base_url());?>index.php/agents/edit/<?php echo $agent["agentcode"];?>"><?php echo $agent["agentcode"];?></a></td>
			<td><input style="width:70px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="title" value="<?php print $agent["title"];?>"></input></td>
			<td><input style="width:120px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="name" value="<?php print $agent["name"];?>"></input></td>
			<td><input style="width:120px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="surname" value="<?php print $agent["surname"];?>"></input></td>
			<td><input style="width:100px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="telnoh" value="<?php print $agent["telnoh"];?>"></input></td>						
			<td height=80px>
				<table>
					<tr><td><input style="width:148px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="physicaladdress1" value="<?php print $agent["physicaladdress1"];?>"></input></td></tr>
					<tr><td><input style="width:148px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="physicaladdress2" value="<?php print $agent["physicaladdress2"];?>"></input></td></tr>
					<tr><td><input style="width:148px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="physicaladdress3" value="<?php print $agent["physicaladdress3"];?>"></input></td></tr>
					<tr><td><input style="width:148px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="physicaladdress4" value="<?php print $agent["physicaladdress4"];?>"></input></td></tr>
					<tr><td><input style="width:148px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="physicalcode" value="<?php print $agent["physicalcode"];?>"></input></td></tr>										
				</table>
			</td>
			<td>
				<table>
					<tr><td><input style="width:148px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="postaladdress1" value="<?php print $agent["postaladdress1"];?>"></input></td></tr>
					<tr><td><input style="width:148px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="postaladdress2" value="<?php print $agent["postaladdress2"];?>"></input></td></tr>
					<tr><td><input style="width:148px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="postaladdress3" value="<?php print $agent["postaladdress3"];?>"></input></td></tr>
					<tr><td><input style="width:148px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="postaladdress4" value="<?php print $agent["postaladdress4"];?>"></input></td></tr>
					<tr><td><input style="width:148px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="postalcode" value="<?php print $agent["postalcode"];?>"></input></td></tr>					
				</table>
			</td>						
			<td><input style="width:70px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="username" value="<?php print $agent["username"];?>"></input></td>
			<td><div id="pwd<?php print $agent["agentcode"];?>"><input style="width: 100px" id="<?php print $agent["agentcode"];?>" class="mytext" type="text" name="password" value="******"></input></div></td>
			<td><input id="<?php print $agent["agentcode"];?>" class="red" type="button" value="Delete" style="width:70px"></input></td>
			<td><input disabled id="saveBtn<?php print $agent["agentcode"];?>" class="red" type="button" value="Save" style="width:70px"></input></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
</div>
