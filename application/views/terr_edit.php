<?php //print_r($terr);?>
<table border="1">
	<tr>
		<td>Name:</td>
		<td><input type="text" name="terr_name<?php echo $terr[0]["id"]; ?>" value="<?php echo $terr[0]["name"]; ?>"></input></td>
	</tr>
	<tr>
		<td>Rating:</td>
		<td>		
			<select name="terr_rating<?php echo $terr[0]["id"]; ?>">
				<option <?php if($terr[0]["rating"] == "A") print "selected"; ?> value="A">A</option>
				<option <?php if($terr[0]["rating"] == "B") print "selected"; ?> value="B">B</option>
				<option <?php if($terr[0]["rating"] == "C") print "selected"; ?> value="C">C</option>
				<option <?php if($terr[0]["rating"] == "D") print "selected"; ?> value="D">D</option>
			</select>	
		</td>
	</tr>
	<tr>
		<td width="80px">Phone Number</td>
		<td><input type="text" name="TelNo<?php echo $terr[0]["id"]; ?>" value="<?php print $terr[0]["telno"]; ?>"></input></td>	
	</tr>
	<tr>
		<td width="80px">Phsyical Address</td>
		<td><input type="text" name="PhysicalAddress1<?php echo $terr[0]["id"]; ?>" value="<?php print $terr[0]["PhysicalAddress1"]; ?>"></input><br></br>
			<input type="text" name="PhysicalAddress2<?php echo $terr[0]["id"]; ?>" value="<?php print $terr[0]["PhysicalAddress2"]; ?>"></input><br></br>
			<input type="text" name="PhysicalAddress3<?php echo $terr[0]["id"]; ?>" value="<?php print $terr[0]["PhysicalAddress3"]; ?>"></input><br></br>
			<input type="text" name="PhysicalAddress4<?php echo $terr[0]["id"]; ?>" value="<?php print $terr[0]["PhysicalAddress4"]; ?>"></input>(Area)
		</td>
	</tr>
	<tr>
		<td width="80px">Postal Code</td>
		<td><input type="text" name="PhysicalCode<?php echo $terr[0]["id"]; ?>" value="<?php print $terr[0]["PhysicalCode"]; ?>"></input></td>	
	</tr>
	<?php //check if not agent 
	if(isset($_SESSION['AGENT']) && $_SESSION['AGENT'] != "YES") {	
	?>
	<tr>
		<td>Agent:</td>
		<td>
			<select name="terr_agent<?php echo $terr[0]["id"]; ?>">
				<?php 
					foreach($agents as $agent) {
						if($terr[0]["agent_id"] == $agent["agentcode"]) $s = "selected";
						else $s = "";
						print "<option $s value=".$agent["agentcode"].">".$agent["name"]." ".$agent["surname"]."</option>";
					}
				?>				
			</select>
			<?php } else {// done check agent ?>	
				<input type=hidden name="terr_agent<?php echo $terr[0]["id"]; ?>" value="<?php echo $_SESSION['AGENT_ID'];?>"></input>
			<?php } ?>
		</td>
	</tr>
	
	<tr>
			<td width="80px">Province</td>
		<td>
			<select name="Province<?php echo $terr[0]["id"]; ?>">
				<option value="0">Please select</option>
				<option <?php if($terr[0]["Province"] == "Gauteng (North)") print "selected"; 	?> value="Gauteng (North)"	>Gauteng (North) - Pretoria</option>
				<option <?php if($terr[0]["Province"] == "Gauteng (South)") print "selected"; 	?> value="Gauteng (South)"	>Gauteng (South) - Johannesburg</option>
				<option <?php if($terr[0]["Province"] == "Gauteng (East)") print "selected"; 	?> value="Gauteng (East)"	>Gauteng (East)</option>
				<option <?php if($terr[0]["Province"] == "Western Cape") print "selected"; 		?> value="Western Cape"		>Western Cape</option>
				<option <?php if($terr[0]["Province"] == "KwaZulu-Natal") print "selected"; 	?> value="KwaZulu-Natal"	>KwaZulu-Natal</option>
				<option <?php if($terr[0]["Province"] == "Eastern Cape") print "selected"; 		?> value="Eastern Cape"		>Eastern Cape</option>
				<option <?php if($terr[0]["Province"] == "Free State") print "selected"; 		?> value="Free State"		>Free State</option>
				<option <?php if($terr[0]["Province"] == "Limpopo") print "selected"; 			?> value="Limpopo"			>Limpopo</option>
				<option <?php if($terr[0]["Province"] == "Mpumalanga") print "selected"; 		?> value="Mpumalanga"		>Mpumalanga</option>
				<option <?php if($terr[0]["Province"] == "North West") print "selected"; 		?> value="North West"		>North West</option>
				<option <?php if($terr[0]["Province"] == "Northern Cape") print "selected"; 	?> value="Northern Cape"	>Northern Cape</option>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input onclick="validateInput(<?php echo $terr[0]["id"]; ?>);" type="button" value="Save" name="Save<?php echo $terr[0]["id"]; ?>"></input>
			<input onclick="cancelInput(<?php echo $terr[0]["id"]; ?>);" type="button" value="Cancel" name="Cancel<?php echo $terr[0]["id"]; ?>"></input>			                
		</td>
	</tr>
</table>
