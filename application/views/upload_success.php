
<h3>Your file was successfully uploaded!</h3>

<table border="1" cellpadding="5">
	<tr>
		<td>Filename:</td>
		<td><?php echo $upload_data["orig_name"]?></td>
	</tr>
	<tr>
		<td>File Size:</td>
		<td><?php echo $upload_data["file_size"]?></td>
	</tr>
	<tr>
		<td>Filename on Server:</td>
		<td><?php echo $upload_data["file_name"]?></td>
	</tr>
	
</table>

<p><?php echo anchor('upload', 'Upload another file'); ?></p>
