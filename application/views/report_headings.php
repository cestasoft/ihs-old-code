<link type="text/css" href="<?php echo(base_url());?>css/jquery.ui.all.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo(base_url());?>jquery/ui/jquery-ui-1.8.5.custom.min.js"></script>

<script type="text/javascript">	 
	$(document).ready(function(){

		var add_count = 1000;

		//enable the save btn when data is changed:
		$("body").delegate("input","keyup", function() {
			var id=($(this).attr("id"));
			$("#saveBtn"+id).css("background-color","#EFFFFF");
			//$("#updateBtn"+id).css("font-weight","bold");
			$("#saveBtn"+id).removeAttr("disabled");
		});		

		$(".red").click(function() {
			
			var id=($(this).attr("id"));
			if($(this).val()=="Save") {
				save(id);				
			}
			else
			if($(this).val()=="Delete") {				
				//alert("We delete ID:" + id);
				$("#dialog-confirm").text('This item will be permanently deleted Id: '+id+' Are you sure?' );

				$("#dialog-confirm").dialog({
		            resizable: false,
		            height:220,
		            width: 400,
		            modal: true,
		            title: 'Do you want to delete ?',
		            buttons: {
		                'Delete ?': function() {
		                       
		                       
		                        var data = 'rh_id='+id;
		                        $.ajax({
		                            type:"POST",
		                            url:"<?php echo(base_url());?>reportheadings/delete",
		                            data: data,
		                            success: function (html) {		             					
		             					if(html=="TRUE") {		                            		
		             						$("#dialog-confirm").dialog('close');
		             						$("tr[id="+id+"]").hide();
		             					}
		             					else
			             					alert("Problem deleting entry: " + html);
		                            	
		                            },
	                            	error: function (err) {
		                            	alert("We got the following error:\n"+err.responseText);
		                            }
		                        });
		                },
		                Cancel: function() {
		                   $(this).dialog('close');
		                }
		            }
		        });
				
				
			} else
				if($(this).val()=="Add Heading") { 
					add_count++;
					$('#myTable tr:last').after(
							'<tr id="' + add_count + '">'
							+'<td id="ID' + add_count + '">NEW</td>'
							+'<td><input id="' + add_count + '" class="mytext" type="text" name="rh_name" value=""></input></td>'
							
							+'<td><input id="' + add_count + '" class="mytext" type="text" name="rh_terr_name" value=""></input></td>'
							+'<td><input id="' + add_count + '" class="mytext" type="text" name="rh_product_name" value=""></input></td>'
							+'<td><input id="' + add_count + '" class="mytext" type="text" name="rh_qty" value=""></input></td>'
							+'<td><input id="' + add_count + '" class="mytext" type="text" name="rh_cost" value=""></input></td>'
							+'<td><input id="' + add_count + '" class="mytext" type="text" name="rh_province" value=""></input></td>'
							+'<td><input id="' + add_count + '" class="mytext" type="text" name="rh_invoice_no" value=""></input></td>'
																			
									
							+'<td><input id="' + add_count + '" class="red" type="button" value="Delete" style="width:70px"></input></td>'
							+'<td><input id="saveBtn' + add_count + '" onclick="add(this.id);" class="red" type="button" value="Add" style="width:70px"></input></td>'
							+'</tr>'
					);
			}	
		});

		

		
		
	});

function isNumber(n) {
		  return !isNaN(parseFloat(n)) && isFinite(n);
}

function add(id) {
	//check fields
	//save
	//alert("add");
	save(id, "NEW");
	
}

function save(id, add) {

	id = id.replace("saveBtn","");	
	
	var data = 
		'rh_id='+id
		+'&rh_add='+add
		+'&rh_province='+$('#'+id+' [name=rh_province]').val()
		+'&rh_name='+$('#'+id+' [name=rh_name]').val()
		+'&rh_terr_name='+$('#'+id+' [name=rh_terr_name]').val()
		+'&rh_product_name='+$('#'+id+' [name=rh_product_name]').val()
		+'&rh_qty='+$('#'+id+' [name=rh_qty]').val()
		+'&rh_cost='+$('#'+id+' [name=rh_cost]').val()
		+'&rh_invoice_no='+$('#'+id+' [name=rh_invoice_no]').val();					

	$.ajax({
     	type:"POST",
     	url:"<?php echo(base_url());?>reportheadings/save",
        data: data,
        success: function (html) {		             					
        	if(html=="TRUE") {		                            		
        		//we only do this after a good save
				$("#saveBtn"+id).css("background-color","#CECEF6");
				$("#saveBtn"+id).attr("disabled","1");    	
							 		
         	}
         	else if(html=="FALSE") {
            	alert("Problem saving entry: " + html);
         	} else if(isNumber(html)) {
         		if(add == "NEW") {
         			location.reload(true);
         			return;
					//fix the ID fields
					alert("we fix the ids");
					$('#ID'+id).html(html);
					$("#saveBtn"+id).css("background-color","#CECEF6");
					$("#saveBtn"+id).attr("disabled","1");   
					$('#saveBtn'+id).attr('onclick','save(this.id)');
					$('#saveBtn'+id).val('Save');					
					$('#saveBtn'+id).attr('id','saveBtn' + html);
					$('#saveBtn'+html).click(function() { 
						alert("we save");
						save('saveBtn' + html); 
						}
					);					
					//$('#'+id).each(function () { $(this).attr('id',html); } );
					$('#'+id+' [name=rh_province]').attr('id',html);
					$('#'+id+' [name=rh_name]').attr('id',html);
					$('#'+id+' [name=rh_terr_name]').attr('id',html);
					$('#'+id+' [name=rh_product_name]').attr('id',html);
					$('#'+id+' [name=rh_qty]').attr('id',html);
					$('#'+id+' [name=rh_cost]').attr('id',html);
					$('#'+id+' [name=rh_invoice_no]').attr('id',html);												
				}
         		else {
         			alert("Problem saving entry: " + html);
         		}
         	} 			                            	
        },
        error: function (err) {
        	alert("We got the following error:\n"+err.responseText);
        }
     });
}

</script>
<div id="dialog-confirm"></div>
<div>
<h2>Report Headings</h2>
<input type="button" class="red" value="Add Heading"></input>
<div>

<table border="1" cellpadding="3" id="myTable">
<thead>
	<tr>
		<th>ID</th>
		<th>File Name</th>		
		<th>Customer Name</th>
		<th>Product Name</th>
		<th>Qty Amount</th>
		<th>Sale Amount</th>
		<th>Province (Optional)</th>
		<th>Invoice Number (Optional)</th>
		<th></th>
		<th></th>
	</tr>
</thead>
<tbody>
<?php

foreach($data as $i) {
	?>
	
		<tr id="<?php print $i["rh_id"];?>">
				<td><?php print $i["rh_id"];?></td>
				<td><input id="<?php print $i["rh_id"];?>" class="mytext" type="text" name="rh_name" value="<?php print $i["rh_name"];?>"></input></td>
				
				<td><input id="<?php print $i["rh_id"];?>" class="mytext" type="text" name="rh_terr_name" value="<?php print $i["rh_terr_name"];?>"></input></td>
				<td><input id="<?php print $i["rh_id"];?>" class="mytext" type="text" name="rh_product_name" value="<?php print $i["rh_product_name"];?>"></input></td>
				<td><input id="<?php print $i["rh_id"];?>" class="mytext" type="text" name="rh_qty" value="<?php print $i["rh_qty"];?>"></input></td>
				<td><input id="<?php print $i["rh_id"];?>" class="mytext" type="text" name="rh_cost" value="<?php print $i["rh_cost"];?>"></input></td>
				<td><input id="<?php print $i["rh_id"];?>" class="mytext" type="text" name="rh_province" value="<?php print $i["rh_province"];?>"></input></td>
				<td><input id="<?php print $i["rh_id"];?>" class="mytext" type="text" name="rh_invoice_no" value="<?php print $i["rh_invoice_no"];?>"></input></td>
																
						
				<td><input id="<?php print $i["rh_id"];?>" class="red" type="button" value="Delete" style="width:70px"></input></td>
				<td><input id="saveBtn<?php print $i["rh_id"];?>" disabled="" class="red" type="button" value="Save" style="width:70px"></input></td>
		</tr>
	<?php 
	
}
?>
</tbody>
</table>
</div>