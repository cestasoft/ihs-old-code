<link type="text/css" href="<?php echo(base_url());?>css/jquery.ui.all.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo(base_url());?>jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>jquery/ui/jquery-ui-1.8.5.custom.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>jquery/jquery.tablesorter.min.js"></script> 

<script type="text/javascript">	 
	$(document).ready(function(){

		$("#addBtn").click(function() {
			//alert("Click");
			window.location = '<?php echo(base_url());?>product_groups/add';
			
		});

		$("#myTable").tablesorter(); 
			
		$(".mytext").keyup(function() {
			var id=($(this).attr("id"));
			$("#saveBtn"+id).css("background-color","#EFFFFF");
			//$("#updateBtn"+id).css("font-weight","bold");
			$("#saveBtn"+id).removeAttr("disabled");
		});

		$(".myselect").change(function() {
			//alert("FUN"+$(this).attr("id"));			
			var id=($(this).attr("id"));
			$("#saveBtn"+id).css("background-color","#EFFFFF");
			//$("#updateBtn"+id).css("font-weight","bold");
			$("#saveBtn"+id).removeAttr("disabled");
		});			
		
		$(".red").click(function() {
			//alert($(this).attr("id"));
			var id=($(this).attr("id"));
			if($(this).val()=="Save") {
				id=id.replace("saveBtn","");
				//alert("We save ID:" + id);
				//$("#saveBtn"+id).css("background-color","#CECEF6");
				//$("#saveBtn"+id).attr("disabled","1");
				var data = 
					'id='+id
					+'&name='+$('#'+id+' [name=name]').val()
					+'&principal='+$('#'+id+' [name=principal]').val();

				//alert(data);
				//return; 
				
				$.ajax({
		         	type:"POST",
		         	url:"<?php echo(base_url());?>product_groups/save",
		            data: data,
		            success: function (html) {		             					
		            	if(html=="TRUE") {		                            		
		            		//we only do this after a good save
		    				$("#saveBtn"+id).css("background-color","#CECEF6");
		    				$("#saveBtn"+id).attr("disabled","1");     		
		             	}
		             	else
			            	alert("Problem saving entry: " + html);		                            	
		            },
	                error: function (err) {
		            	alert("We got the following error:\n"+err.responseText);
		            }
		         });
			}
			else
			if($(this).val()=="Delete") {				
				//alert("We delete ID:" + id);
				$("#dialog-confirm").text('This item will be permanently deleted Id: '+id+' Are you sure?' );

				$("#dialog-confirm").dialog({
		            resizable: false,
		            //height:140,
		            //width: 400,
		            modal: true,
		            title: 'Do you want to delete ?',
		            buttons: {
		                'Delete ?': function() {
		                       
		                       
		                        var data = 'Id='+id;
		                        $.ajax({
		                            type:"POST",
		                            url:"<?php echo(base_url());?>product_groups/delete",
		                            data: data,
		                            success: function (html) {		             					
		             					if(html=="TRUE") {		                            		
		             						$("#dialog-confirm").dialog('close');
		             						$("tr[id="+id+"]").hide();
		             					}
		             					else
			             					alert("Problem deleting entry: " + html);
		                            	
		                            },
	                            	error: function (err) {
		                            	alert("We got the following error:\n"+err.responseText);
		                            }
		                        });
		                },
		                Cancel: function() {
		                   $(this).dialog('close');
		                }
		            }
		        });
				
				
			}	
		});

		

		
		
	});
</script>
<br></br>
<h2>Product Groups</h2>
<br></br>
<div id="dialog-confirm"></div>
<div>
<?php 
//$select_principal_group="<option value=\"\"></option>";
$select_principal_group="";

foreach($principal_groups as $pg) {
		$select_principal_group.="<option value=\"".trim($pg['Id'])."\">".$pg['Name']." </option>";
}
?>

<input type="button" id="addBtn" value="Add Product Group" class="red"></input>
<br></br>
<br></br>

<table border="1" cellpadding="3" id="myTable">
<thead>
	<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Principal</th>		
		<th></th>
		<th></th>
	</tr>
</thead>
<tbody>
<?php

//print_r($data);

foreach($data as $i) {
	?>
	
		<tr id="<?php print $i["id"];?>">
				<td><?php print $i["id"];?></td>
				<td><input id="<?php print $i["id"];?>" class="mytext" type="text" name="name" value="<?php print $i["name"];?>"></input></td>
				<td>
					<select name="principal" style="width:120px" class="myselect" id="<?php print $i["id"];?>">
						<?php 
							$str="value=\"".$i['principalid']."\"";
							print str_replace($str,"selected ".$str,$select_principal_group);
							  				
						?>
					</select>
					<?php //print $i['principalid'];?>
				</td>				
				<td><input id="<?php print $i["id"];?>" class="red" type="button" value="Delete" style="width:70px"></input></td>
				<td><input id="saveBtn<?php print $i["id"];?>" disabled="" class="red" type="button" value="Save" style="width:70px"></input></td>
		</tr>
	<?php 
	
}
?>
</tbody>
</table>
</div>