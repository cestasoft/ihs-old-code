<script type="text/javascript" src="<?php echo(base_url());?>jquery/jquery.tablesorter.min.js"></script> 	
<script type="text/javascript">
	$(function() {

 		$("#siteTable").tablesorter(); 

		$("#saving").hide();

		$.expr[':'].containsIgnoreCase = function(e,i,m){
		    return jQuery(e).text().toUpperCase().indexOf(m[3].toUpperCase())>=0;
		};
				
		$('#quickfind').focus().keyup(function() {
			  if($(this).val().length > 3) { 				   
	            $('#sites_table tr:not(:containsIgnoreCase("' + $(this).val() + '"))').hide();
	            $('#sites_table tr:containsIgnoreCase("' + $(this).val() + '")').show();
			  } else {
				$('#sites_table tr').show();
			  }
	    });
						
	$(".checkbox").click(function () {
			if($(this).attr("checked") == true) {
				$(this).hide();	
				var div_id = $(this).attr("div_id");
				$("#"+div_id).text("Saving data ... ");			
				//$(this).attr("sid","23");
				var that = this;
				
				//alert("We add");
				var data = 'date=' + $(this).attr("date") + '&tid=' + $(this).attr("tid") + '&agent_id=' + $(this).attr("agent_id");				
				$.ajax({
	            	type:"POST",
			timeout	: 30000,
	                url:"<?php echo(base_url());?>sites/save_visit",
	                data: data,
	                success: function (my_html) {
				$("#saving").hide();
	                	//alert("Hi" + my_html);	
	                	if(my_html == "0" || !validateInt(my_html)) {
		                	$("#"+div_id).text("Error saving data.");
					alert("Error saving data");
		                	$(that).attr("checked",false);
	                	} else {
	                		$("#"+div_id).text("");
	                		$(that).attr("sid",my_html);
	                	}	                		                	
	                	$(that).show();
	                	//alert(my_html);	             									  	
						//$("#tblTerr").html(my_html);								
	                  },
	              	error: function (err) {
		            	$("#saving").hide();
	                  	alert("We got the following error:\n"+err.responseText);
	                  },
			beforeSend: function(data) {                		     
		            	$("#saving").show();
				positionPopup();
	            	}
	            });	
			} else if($(this).attr("checked") == false) {
				//alert("We remove sid: " + $(this).attr("sid") );
				$(this).hide();	
				var div_id = $(this).attr("div_id");
				$("#"+div_id).text("Saving data ... ");			
				//$(this).attr("sid","23");
				var that = this;
							
				var data = 'sid=' + $(this).attr("sid");				
				$.ajax({
	            	type:"POST",
			timeout	: 30000,
	                url:"<?php echo(base_url());?>/sites/remove_visit",
	                data: data,
	                success: function (my_html) {
				$("#saving").hide();	
	                	if(my_html != "1") {
		                	$("#"+div_id).text("Error saving data.");
					alert("Error saving data");
		                	$(that).attr("checked",true);
	                	} else {
	                		$("#"+div_id).text("");
	                	}	                					                		                		                
	                	$(that).show();								
	                  },
	              	error: function (err) {
				$("#saving").hide();
	                  	alert("We got the following error:\n"+err.responseText);
	                  },
			beforeSend: function(data) {                		     
		            	$("#saving").show();
				positionPopup();
	            	}
	            });
				
			} else alert("We have a problem");  
			//alert("Yes"); 
			//$(this).slideUp(); 
		});
					 
	});

	function validateInt(iString) {
	    // no leading 0s allowed
	    return (("" + parseInt(iString)) == iString);
	}

//position the popup at the center of the page
function positionPopup(){
  if(!$("#saving").is(':visible')){
    return;
  } 
  $("#saving").css({
      left: ($(window).width() - $('#saving').width()) / 2,
      top: screen.height / 2,
      'font-size': '18px',
      position: 'absolute'
  });
}
	
	</script>

<?php 
//echo form_open('sites');
?>
<div id="saving" style="width:800px; margin:0 auto;"><table width="400" bgcolor="#ff0066"><tr width="100%"><td align="center">Saving</td></tr></table></div>
Search: <input type="text" id="quickfind" name="filterTextBox" />
<table border="1" cellpadding="5" class="filterable" id="siteTable">
<thead>
	<tr>
		<th>Customer</th>
		<th>Area</th>		
		<th>Phone Number</th>
		<th>Rating</th>
<?php 
//print_r($weeks);
$i=1;
foreach($weeks as $week) {
	print "<th><small>Week $i <br>";
	$i++;
	print $week[0]["day_of_week"]." ".($week[0]["day_of_month"]+1)."/$month/$year"
	."<br/>  to <br/>";
	print $week[count($week)-1]["day_of_week"]." ".($week[count($week)-1]["day_of_month"]+1)."/$month/$year";
	print "</small></th>";
}
?>
		
	</tr>
</thead>
<tbody id="sites_table">
<?php 

$buffer 	= array();
$lastname 	= "";
$k			= 0;

foreach($all_sites as $item) {
	
	if($lastname != $item["name"]) $k++; 
		
	if(!isset($buffer[$k])) $buffer[$k] = array();
	
	$buffer[$k]["name"] 			= $item["name"];
	$buffer[$k]["tid"]				= $item["tid"];
	$buffer[$k]["physicaladdress4"]	= $item["physicaladdress4"];
	$buffer[$k]["telno"]			= $item["telno"];
	$buffer[$k]["rating"]			= $item["rating"];
		
	foreach($weeks as $week) {
		//$week = $week[0]["day_of_month"]+1;
		$tmp_week = $week;
		$week = $week[count($week)-1]["day_of_month"]+1;
		//var_dump($week);
		//exit;
		//if(isset($buffer[$k]) && isset($buffer[$k][$week]) && isset($buffer[$k][$week]["flag"]) && $buffer[$k][$week]["flag"] == true) continue;
		if(array_key_exists($k,$buffer) &&
			array_key_exists($week,$buffer[$k]) &&
			array_key_exists("flag",$buffer[$k][$week]) &&
			$buffer[$k][$week]["flag"] == true) continue;

		
		//The SQL query is limit to Month so we only need to check day
		if(strlen(trim($item["datevisited"])) == 0) continue;
				
		$the_day = ltrim(substr($item["datevisited"],8,2),"0");
		
		$min_day = $tmp_week[0]["day_of_month"] + 1;
		$max_day = $tmp_week[count($tmp_week)-1]["day_of_month"] + 1;
		
		//$day 	= $week;
		//if(strlen($day)==1) $day = "0".$day;
		//$date 	= $year."-".$month."-".$day;

		//print "\r\nFor ".$item["name"]." and week $week compare $date with ".$item["datevisited"];
		//exit;
		//if($date == $item["datevisited"])
		/*
		if($item["name"] == "ARRIENEL") {
			print "\r\nSTART";
			print "\r\nThe day".$the_day;
			print "\r\nThe min".$min_day;
			print "\r\nThe max".$max_day;
			print "\r\nEND";
		}
		*/
		
		if($the_day >= $min_day && $the_day <= $max_day) {
			if(!isset($buffer[$k][$week])) $buffer[$k][$week] = array();
			$buffer[$k][$week]["flag"]	= true;
			$buffer[$k][$week]["sid"]	= $item["sid"];
		}
		else {
			$buffer[$k][$week]["flag"] 	= false;
			$buffer[$k][$week]["sid"]	= 0;
		}
			 	
	}

	$lastname = $item["name"];	
}
 
//print_r($buffer);

//exit;

//print "<td align=\"center\"><input name=\"visits[]\" value=\"".$item["tid"].":$year-$month-".($week[0]["day_of_month"]+1)."\"type=\"checkbox\" $checked /></td>";
$week_total = array();
$div_id = 0;
foreach($buffer as $item) {	
	print "<tr><td>".$item["name"]."</td>";
	print "<td>".$item["physicaladdress4"]."</td>";
	print "<td>".$item["telno"]."</td>";
	print "<td>".$item["rating"]."</td>";
	$week_cnt = 0;
	foreach($weeks as $week) {
		if ( !isset($week_total[$week_cnt]) ) $week_total[$week_cnt] = 0;
		//$week = $week[0]["day_of_month"]+1;
		$week 	= $week[count($week)-1]["day_of_month"]+1;
		$date 	= $year."-".$month."-".$week;
		if(isset($item[$week]["flag"]) && $item[$week]["flag"] == true) {
			$checked = "checked";
			$week_total[$week_cnt] = $week_total[$week_cnt] + 1; 
		}
		else $checked = "";
		if(isset($item[$week]["sid"])) $sid=$item[$week]["sid"];
		else $sid = 0;						
		print "<td align=\"center\"><div id=\"".$div_id."\"></div><input div_id=\"".$div_id."\" agent_id=\"".$agent_id."\" date=\"".$date."\" tid=\"".$item["tid"]."\" sid=\"".$sid."\"class=\"checkbox\"  name=\"visits[]\" value=\"".$item["tid"].":$year-$month-".($week)."\"type=\"checkbox\" $checked /></td>";
		$div_id++;
		$week_cnt++;	
	}
	print "</tr>";
}

print "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>Totals:</b></td>";

foreach($week_total as $w) {
	print "<td align=\"center\">".$w."</td>";
}

print "</tr>";

?>

</tbody>
</table>
<br></br>
<!-- <input class="btn" type="submit" value="Save"></input>
<input type="hidden" name="action" value="sites_save"></input>
 -->
</body>
