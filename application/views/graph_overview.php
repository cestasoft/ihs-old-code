<div>
<br></br>
<h2>This is where we show all the graphs:</h2>
<?php 
//print_r($total_sales_by_month);
//print getcwd();
?>
<?php 
 
// Standard inclusions     
include("chart/pChart/pData.class");  
include("chart/pChart/pChart.class");  
   
// Dataset definition   
$DataSet = new pData;

$my_y_data=array();
$my_x_data=array();

foreach($total_sales_by_month as $i) {
	$my_y_data[]=$i["month_total"];
	$my_x_data[]=$i["importcode"];
}
$DataSet->AddPoint($my_y_data);
//$DataSet->AddPoint(array(100,400,300,200,300,300));  

$DataSet->AddPoint($my_x_data,"Serie2");  
$DataSet->SetAbsciseLabelSerie("Serie2"); 

$DataSet->AddSerie();  
$DataSet->SetSerieName("Total Sales per Month","Serie1");
$DataSet->SetXAxisName("Month");
$DataSet->SetYAxisName("Rand");     
   
// Initialise the graph  
$Test = new pChart(1200,800);  
$Test->setFontProperties("chart/Fonts/tahoma.ttf",10);  
$Test->setGraphArea(100,30,1000,600);  
$Test->drawGraphArea(252,252,252);  
$Test->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_NORMAL,150,150,150,TRUE,0,2);  
$Test->drawGrid(4,TRUE,230,230,230,255);  
   
// Draw the line graph  
$Test->drawLineGraph($DataSet->GetData(),$DataSet->GetDataDescription());  
$Test->drawPlotGraph($DataSet->GetData(),$DataSet->GetDataDescription(),3,2,255,255,255);  
   
// Finish the graph  
$Test->setFontProperties("chart/Fonts/tahoma.ttf",8);  
$Test->drawLegend(1000,35,$DataSet->GetDataDescription(),255,255,255);  
$Test->setFontProperties("chart/Fonts/tahoma.ttf",10);  
$Test->drawTitle(60,22,"Total Sales per Month",50,50,50,585);  
$Test->Render("chart/tmp/Naked.png");  
 
?>
<img src="/chart/tmp/Naked.png"></img>
</div>
