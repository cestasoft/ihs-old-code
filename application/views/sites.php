<link type="text/css" href="<?php echo(base_url());?>jquery/css/jquery.ui.all.css" rel="stylesheet" />
<link type="text/css" href="<?php echo(base_url());?>jquery/css/jquery.tooltip.css" rel="stylesheet" />

<script type="text/javascript" src="<?php echo(base_url());?>jquery/js/jquery.bgiframe.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>jquery/js/jquery.dimensions.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>jquery/js/jquery.tooltip.min.js"></script>

<script type="text/javascript" src="<?php echo(base_url());?>jquery/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>jquery/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>jquery/ui/jquery.ui.datepicker.js"></script>
	
<script type="text/javascript">
	$(function() {
	
	$('#yearPlus').click(function() {
		//alert("bang");
		$('#year').val(parseInt($('#year').val())+1);
	});

	$('#yearMinus').click(function() {
		//alert("bong");
		$('#year').val(parseInt($('#year').val())-1);
	});
			

	});
	//function removeDiv(n) {
		//n.parentNode.removeChild(n);
	//}
	</script>
</head>
<body>
<br></br>
<h2>Site Visits</h2>
<?php 
echo form_open('sites');
?>
<br></br>
<table border="0">
	<tr>
			<td>Select a Month:</td>
			<td>
			<select name="month">
				<option value="01">Jan</option>
				<option value="02">Feb</option>
				<option value="03">Mar</option>
				<option value="04">Apr</option>
				<option value="05">May</option>
				<option value="06">Jun</option>
				<option value="07">Jul</option>
				<option value="08">Aug</option>
				<option value="09">Sep</option>
				<option value="10">Oct</option>
				<option value="11">Nov</option>
				<option value="12">Dec</option>
			</select>			
	</tr>
	<tr>
			<td>Select a Year:</td>
			<td><input readonly style="width:50px" type="text" id="year" name="year" autocomplete="off" value="<?php echo date('Y');?>"/>
			<input id="yearPlus" type="button" value="+"></input>
			<input id="yearMinus" type="button" value="-"></input></td>
	</tr>
	<tr>
			<td>Select Agent:</td>
			<td><select name="agentId" <?php if($_SESSION['AGENT']=="YES") { echo "disabled=\"\""; }?>>
<?php 
$tmp_id = "";
foreach($all_agents as $agent) {
		if($agent_id == $agent['agentcode']) {
			$tmp = "selected";
			if($_SESSION['AGENT']=="YES") {
				$tmp_id = '<input type="hidden" name="agentId" value="'.$agent_id.'">';
			}
		}
		else $tmp = "";
		print "<option $tmp value=\"".trim($agent['agentcode'])."\">".$agent['title']." ".$agent['name']." ".$agent['surname']."</option>";
}

?>
			</select>
<?php
	print $tmp_id; 
?>
			</td>
	</tr>
</table>
<br></br>
<input class="btn" type="submit" value="Next"></input>
<input type="hidden" name="action" value="sites_table"></input>
</form>
</body>
