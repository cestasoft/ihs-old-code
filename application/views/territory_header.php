<script type="text/javascript"	src="<?php echo(base_url());?>jquery/jquery-1.4.2.min.js"></script>
<link type="text/css" href="<?php echo(base_url());?>css/jquery.ui.all.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo(base_url());?>jquery/ui/jquery-ui-1.8.5.custom.min.js"></script>

<script type="text/javascript">	 
	 $(document).ready(function(){

		var page_total = <?php echo $page_total;?>;

		$('[name=del]').live('click',function() {
			var id 		= $(this).attr("id");
			var name 	= $(this).attr("ter_name");
			
			$("#dialog-confirm").text('Are you sure you want to delete '+name+'?' );

			$("#dialog-confirm").dialog({
	            resizable: false,
	            height:230,
	            width: 400,
	            modal: true,
	            title: 'Do you want to delete ?',
	            buttons: {
	                'Delete ?': function() {
	                       
	                       
	                        var data = 'id='+id;
	                        $.ajax({
	                            type:"POST",
	                            url:"<?php echo(base_url());?>territory/delete",
	                            data: data,
	                            success: function (html) {		             					
	             					if(html=="TRUE") {		                            		
	             						$("#dialog-confirm").dialog('close');
	             						$("tr[id="+id+"]").hide();
	             					}
	             					else
		             					alert("Problem deleting entry: " + html);
	                            	
	                            },
                            	error: function (err) {
	                            	alert("We got the following error:\n"+err.responseText);
	                            }
	                        });
	                },
	                Cancel: function() {
	                   $(this).dialog('close');
	                }
	            }
	        });
		
		});

		$('#showForAgent').click(function() {
			var agent_id = $('#agent_id').val();

			if(agent_id == 'ALL')
				window.location = '<?php echo(base_url());?>territory';
			else {
				//var loc = 
				//alert(loc);
				window.location = '<?php echo(base_url());?>territory/index/' + agent_id;
			}
		});
		 					
		$('#txtFilter').keyup(function(event) {
			if($(this).val().length > 2) {
				//alert($(this).val());
				var filter = $(this).val();
				$("#tblTerr").html("<small>Query in progress...</small>");
				$.post("<?php echo base_url(); ?>/territory/get_territories_via_filter", { filter: filter },
					function(data) {
					$("#tblTerr").html(data);																				
			});
			} else $("#tblTerr").html("<br/>Please enter at least 3 charaters to search for.")
		});

		$('#btnNext').click(function() {			
		
			//alert(page_total);

			var pageNumber = $('#inputPageNumber').val();
			var agentId =  $('#agent').val();

			pageNumber++;		
		
			$("#tblTerr").html("<img src=\"/img/loading.gif\">");
			
			$('#inputPageNumber').val(pageNumber);	
			var data = 'page_number='+pageNumber+'&agent_id='+agentId;
			$.ajax({
            	type:"POST",
                url:"<?php echo(base_url());?>territory/get_territories",
                data: data,
                success: function (my_html) {
					$("#tblTerr").html(my_html);								
					$('#btnPrev').removeAttr("disabled");
					//Disable the next btn 
					if(pageNumber == page_total) $('#btnNext').attr("disabled", true);
                  },
              	error: function (err) {
                  	alert("We got the following error:\n"+err.responseText);
                  }
            });			
		});

		$('#btnPrev').attr("disabled", true);

		$('#btnPrev').click(function() {			
			var pageNumber = $('#inputPageNumber').val();
			var agentId =  $('#agent').val();

			if(pageNumber>1) pageNumber--;



			$("#tblTerr").html("<img src=\"/img/loading.gif\">");

			$('#inputPageNumber').val(pageNumber);		
			var data = 'page_number='+pageNumber+'&agent_id='+agentId;
			$.ajax({
            	type:"POST",
                url:"<?php echo(base_url());?>territory/get_territories",
                data: data,
                success: function (my_html) {
					$("#tblTerr").html(my_html);
					if(pageNumber != page_total) $('#btnNext').removeAttr("disabled");
					if(pageNumber == 1) $('#btnPrev').attr("disabled", true);								
                  },
              	error: function (err) {
                  	alert("We got the following error:\n"+err.responseText);
                  }
            });		

		});

		//$('select[name=rating]').change(function() {
		$('select[name=rating]').live('change',function() {
			var the_terr_id = $(this).attr("terr_id");
			var the_rating = $(this).val();
			$.post("<?php echo base_url(); ?>/territory/set_territory_rating", { terr_id: the_terr_id, rating: the_rating },
					function(data) {
						$('#div'+the_terr_id).html("");						
						$('#div'+the_terr_id).show();
						if(data=="TRUE") $('#div'+the_terr_id).html("<div id=\"div"+the_terr_id+"\"><font color=green>Updated Rating to "+the_rating+"</font></div>");
						else $('#div'+the_terr_id).html("<div id=\"div"+the_terr_id+"\"><font color=red>Problem updating.</font></div>");						
						//$('#'+the_terr_id).hide(3000);
						//$('#'+the_terr_id).fadeIn();	
						$('#div'+the_terr_id).fadeOut(3000);
						//$('#'+the_terr_id).fadeIn(2500);
													
			});
		});
			
		$("a[href='#']").live('click', function(event) {
			event.preventDefault();
		});
			
		
		//$('select[name=agent_id]').change(function() {
		$('select[name=agent_id]').live('change',function() {
			var the_terr_id = $(this).attr("terr_id");
			var the_agent_id = $(this).val();
			//alert("We change" + terr_id + " with val of "+agent_id);			
			$.post("<?php echo base_url(); ?>/territory/set_territory_agent", { terr_id: the_terr_id, agent_id: the_agent_id },
					function(data) {
						$('#div'+the_terr_id).html("");
						$('#div'+the_terr_id).show();
						if(data=="TRUE") $('#div'+the_terr_id).html("<div id=\"div"+the_terr_id+"\"><font color=green>Updated Agent</font></div>");
						else $('#div'+the_terr_id).html("<div id=\"div"+the_terr_id+"\"><font color=red>Problem updating.</font></div>");						
						//$('#'+the_terr_id).hide(3000);
						//$('#'+the_terr_id).fadeIn();
						$('#div'+the_terr_id).fadeOut(3000);
						//$('#'+the_terr_id).fadeIn(2500);	
				//$("#tblTerr").html(data);
				//$('#inputPageNumber').val(pageNumber);															 
			});
			
		});
	});
</script>

<div
	class=""><?php 
	$select_agent="";
	foreach($all_agents as $agent) {
		$select_agent.="<option value=\"".trim($agent['agentcode'])."\">".$agent['title']." ".$agent['name']." ".$agent['surname']."</option>";
	}
	?> 
<table border=0>
	<tr>
		<td style="width:160px">Search by name: </td>
		<td><input style="width:250px" type="text" name="txtFilter" id="txtFilter"></input></td>
	</tr>
	<tr>
		<td><input type="hidden" name="agent_id" id="agent" value="<?php echo $agent_id;?>"></input></td>
	</tr>
<?php 
if($_SESSION['AGENT']=="YES") {
} else {
?>
	<tr>
		<td>Show Customers for:</td> 
		<td>
			<select style="width:250px" name="agent" id="agent_id">				
				<option value="ALL">All Agents</option>
				<?php //print $select_agent;?>
				<?php 
							$str="value=\"".$agent_id."\"";
							print str_replace($str,"selected ".$str,$select_agent);
							  				
						?>
			</select>
		</td>	
		<td><input type="button" value="Go" id="showForAgent"></input></td>
	</tr>
<?php 
}
?>
</table>	

<div id="dialog-confirm"></div>

<br/>
<br/>
<small>
Page : <?php //echo $page["number"]; ?> <input readonly
	style="width:30px; border: 0 solid black; font-size: 0.8em;"
	id="inputPageNumber" name="pageNumber" value=1></input> of <input
	readonly style="width:30px; border: 0 solid black; font-size: 0.8em;"
	id="inputPageNumber" name="pageNumber" value=<?php echo $page_total;?>></input>
page(s)</small>
<button id="btnPrev">Prev</button>
<button id="btnNext">Next</button>
