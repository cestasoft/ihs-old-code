<?php

if(isset($_SESSION["AGENT_ID"])) $agent_id		= $_SESSION["AGENT_ID"];
else $agent_id = "";
$agent_name 	= "";
$select_agent	= "";
//print_r($all_agents);
foreach($all_agents as $agent) {
		$select_agent.="<option value=\"".trim($agent['agentcode'])."\">".$agent['title']." ".$agent['name']." ".$agent['surname']."</option>";
		if(isset($agent) && isset($agent["agentcode"]) && isset($agent_id) && $agent["agentcode"] == $agent_id) {
			$agent_name = $agent["name"]." ".$agent["surname"];
		}
}

$select_rating="
	<option value=\"A\">A</option>
	<option value=\"B\">B</option>
	<option value=\"C\">C</option>	
";

if($_SESSION['AGENT']=="YES") {
	$read_only = true;
} else 
	$read_only = false; 

foreach($territory as $terr) { ?>
	<tr id="<?php echo $terr['id'];?>" title="<?php echo $terr['id'];?>">
		<td><?php echo $terr['id'];?></td>
		<td><?php echo $terr['PhysicalAddress4'];?></td>
		<td><a href='#' onclick="EditTerritory(<?php echo $terr['id'];?>);"><?php echo $terr['name'];?></a></td>
		
		<?php if(isset($show_select)&&$show_select=="select_site_visit") { ?>
						
			<td><select disabled name="agent_id" terr_id="<?php echo $terr['id'];?>">
			<?php
				$str="value=\"".trim($terr['agent_id'])."\"";
				$this_select=str_replace($str,"selected ".$str,$select_agent); 
				echo $this_select;
			?>
		</select>
		</td>
		<td><select disabled name="rating" terr_id="<?php echo $terr['id'];?>">
			<?php
			$str="value=\"".strtoupper(trim($terr['rating']))."\"";
			$this_select=str_replace($str,"selected ".$str,$select_rating); 
			echo $this_select; 
			?>
		</select>		
		</td>
		<td>
			<div id="<?php echo $terr['id'];?>"></div>
			<input type="button" value="Select" onclick="selectSiteVisit(<?php echo $terr['id'];?>,'<?php echo $terr['name'];?>','<?php echo $terr['rating'];?>')" id="<?php echo $show_select;?>" terr_id="<?php echo $terr['id'];?>">			
		</td>
			
		<?php }
		else if(isset($show_select)&&($show_select=="select_parent"||$show_select=="select_child")) { 				
			?>	
			
		<td><select disabled name="agent_id" terr_id="<?php echo $terr['id'];?>">
			<?php
			$str="value=\"".trim($terr['agent_id'])."\"";
			$this_select=str_replace($str,"selected ".$str,$select_agent); 
			echo $this_select;
			?>
		</select>
		</td>
		<td><select disabled name="rating" terr_id="<?php echo $terr['id'];?>">
			<?php
			$str="value=\"".strtoupper(trim($terr['rating']))."\"";
			$this_select=str_replace($str,"selected ".$str,$select_rating); 
			echo $this_select; 
			?>
		</select>		
		</td>
		<td>
			<div id="<?php echo $terr['id'];?>"></div>
			<input type="button" value="Select" onclick="<?php echo $show_select;?>(<?php echo $terr['id'];?>,'<?php echo $terr['name'];?>')" id="<?php echo $show_select;?>" terr_id="<?php echo $terr['id'];?>">			
		</td>	
		
		<?php } else { ?>
		<td>
		<?php 
			if(!$read_only) { ?>
				<select name="agent_id" terr_id="<?php echo $terr['id'];?>">
				<?php
				$str="value=\"".trim($terr['agent_id'])."\"";
				$this_select=str_replace($str,"selected ".$str,$select_agent); 
				echo $this_select;
				?>
				</select>
			<?php } else {
				echo $agent_name;
			}
			?>
			
		</td>
		<td><select name="rating" terr_id="<?php echo $terr['id'];?>">
			<?php
			$str="value=\"".strtoupper(trim($terr['rating']))."\"";
			$this_select=str_replace($str,"selected ".$str,$select_rating); 
			echo $this_select; 
			?>
		</select>		
		</td>
		<td>
			<input ter_name="<?php echo $terr['name'];?>" type="button" name="del" value="Delete" id="<?php echo $terr['id'];?>"/>
		</td>
		<td>
			<div id="div<?php echo $terr['id'];?>"></div>			
		</td>
		<?php } ?>
	</tr>
<?php 	
}

?>
