<?php
//print_r($report_rows);

//exit;
?>
<br></br>

<h2><?php print $report_title;?></h2>
<table border="1" cellpadding="5">
<thead>
	<tr>
		<?php
			$k=0;						
			foreach($report_headings as $item) {
				$k++;							
				if(isset($header_alt_color)&&$header_alt_color=true) {
					if($k%4==0) $k=0;					
					if($k > 1) $style="class=\"headerGrey\"";
					else $style="class=\"headerLightGrey\""; 	
				}				
				else $style="";
				if(trim($item)=="") print "<th $k $style>&nbsp;</th>";
				else print "<th $k nowrap $style>".$item."</th>"; 
			} 		
		?>		
	</tr>
</thead>
<tbody>
	<?php
		foreach($report_rows as $row) {
			?>
				<tr>
					<?php
					$k=0;								
						foreach($row as $item) {
							$k++;							
							if(isset($row_alt_color)&&$row_alt_color=true) {
								if($k%4==0) $k=0;					
								if($k > 1) $style="class=\"grey\"";
								else $style="class=\"lightGrey\""; 	
							}				
							else $style="";
							$item=trim($item); 
							if($item=="") 
								print "<td $style>&nbsp;</td>";
							else if(is_numeric($item)) {
								//preg_match('/[a-zA-Z]/',$item)==0)
								//print "<td nowrap $style>".$item."</td>";
								print "<td nowrap align=\"right\">".number_format($item,2)."</td>";
								//print "<td nowrap align=\"right\" $style>".$item."</td>";
							}
							else 
								print "<td nowrap $style>".$item."</td>";
								//print "<td nowrap align=\"right\">".number_format($item,2)."</td>";			
						}
					?>
				</tr>
	<?php } ?>	
</tbody>
</table>