<?php
//print_r($all_agents);
//print_r($all_product_groups);
//print_r($report_data);
		
?>
<br></br>
<table border="1">
<thead>
	<tr>
		<th>Product Group</th>
		<?php 
			foreach($all_product_groups as $product_group) {
					if(trim($product_group["name"])=="") $product_group["name"]="&nbsp";
					print "<th nowrap>".$product_group["name"]."</th>";
			}
		?>
	</tr>
</thead>
<tbody>
	<?php
		foreach($all_agents as $agent) {
			print "<tr><td>";
			if(trim($agent["name"]." ".$agent["surname"])=="") print "&nbsp";	
			else print  $agent["name"]." ".$agent["surname"];
			print "</td>";			
			foreach($all_product_groups as $product_group) {
				print "<td align=right>";
			//	print "<td>".$report_data[$product_group["id"]][$agent["agentcode"]]["comm"]."</td>";
				if(isset($report_data[$product_group["id"]])&&isset($report_data[$product_group["id"]][$agent["agentcode"]]))
					print "".number_format($report_data[$product_group["id"]][$agent["agentcode"]]["comm"],2)."";
				else print "0";
				//print "<td>".$product_group["id"]." ".$agent["agentcode"]."</td>";
				print "</td>";
			}
			
			print "</tr>";
		} 
	?>
</tbody>
</table>