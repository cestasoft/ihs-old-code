<?php

define("MAX_IDLE_TIME","60000"); //60 * 10 (10 minutes)

session_start();

$this->load->helper('url');

if(isset($_SESSION['USER']) && isset($_SESSION['SYSTEM']) && $_SESSION['SYSTEM'] == "IHS") {
	//Good Auth	
} else {
	//Direct user to login screen	
	$_SESSION['dest']=$_SERVER["REQUEST_URI"];
	header("Location: ".base_url()."login/");	
	exit();	
}

if (!isset($_SESSION['timeout_idle'])) {
    $_SESSION['timeout_idle'] = time() + MAX_IDLE_TIME;
} else {
    if ($_SESSION['timeout_idle'] < time()) {   
        //destroy session
        header("Location: ".base_url()."login/logout");
    } else {
        $_SESSION['timeout_idle'] = time() + MAX_IDLE_TIME;
    }
}

?>
