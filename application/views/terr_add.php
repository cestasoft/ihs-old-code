
<script>
function validateInput() {
	var problem = false;
	if($.trim($("[name=terr_name]").val()) == "") { alert("Please provide a name.");	problem = true; }
	if($.trim($("[name=Province]").val()) == "0") { alert("Please select a province."); problem = true; }
	if(problem == false) document.forms["myform"].submit();
}
</script>
<h2>Add Customer</h2>

Please provide the required Customer information:

<form method="post" name="myform" action="<?php echo(base_url());?>territory/do_add">

<table border="1" cellpadding="5">
	<tr>
		<td width="80px">Name</td>						
		<td><input type="text" name="terr_name" value=""></input></td>		
	</tr>
	<tr>
		<td width="80px">Rating</td>
		<td>
			<select name="terr_rating">
				<option value="A">A</option>
				<option value="B">B</option>
				<option value="C">C</option>
				<option value="D">D</option>
			</select>			
		</td>		
	</tr>
	<tr>
		<td width="80px">Telephone Number</td>						
		<td><input type="text" name="TelNo" value=""></input></td>		
	</tr>
	<tr>
		<td width="80px">Phsyical Address</td>
		<td><input type="text" name="PhysicalAddress1"></input><br></br>
			<input type="text" name="PhysicalAddress2"></input><br></br>
			<input type="text" name="PhysicalAddress3"></input><br></br>
			<input type="text" name="PhysicalAddress4"></input>(City)
		</td>
	</tr>
	
	<tr>
		<td width="80px">Postal Code</td>
		<td><input type="text" name="PhysicalCode"></input></td>
	</tr>
	
	<tr>
		<td width="80px">Province</td>
		<td>
			<select name="Province">
				<option value="0">Please select</option>
				<option value="Gauteng (North)"	>Gauteng (North) - Pretoria</option>
				<option value="Gauteng (South)"	>Gauteng (South) - Johannesburg</option>
				<option value="Western Cape"	>Western Cape</option>
				<option value="KwaZulu-Natal"	>KwaZulu-Natal</option>
				<option value="Eastern Cape"	>Eastern Cape</option>
				<option value="Free State"		>Free State</option>
				<option value="Limpopo"			>Limpopo</option>
				<option value="Mpumalanga"		>Mpumalanga</option>
				<option value="North West"		>North West</option>
				<option value="Northern Cape"	>Northern Cape</option>
			</select>
		</td>
	</tr>
	
<!--
	<tr>
		<td>Agent</td>
		<td><input type="text" name="terr_name" value=""></input></td>		
	</tr>
 -->	
	<tr>
		<td colspan="2" align="center"><input onclick="validateInput();" type="button" value="Add Customer"></input></td>
	</tr>
</table>

</form>