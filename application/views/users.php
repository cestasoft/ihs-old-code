<link type="text/css"
	href="<?php echo(base_url());?>css/jquery.ui.all.css" rel="Stylesheet" />
<script
	type="text/javascript"
	src="<?php echo(base_url());?>jquery/ui/jquery-ui-1.8.5.custom.min.js"></script>
<script
	type="text/javascript"
	src="<?php echo(base_url());?>jquery/jquery.tablesorter.min.js"></script>

<script type="text/javascript">	 
	$(document).ready(function(){

		$("#addBtn").click(function() {
			window.location = '<?php echo(base_url());?>users/add';			
		});

		$("#myTable").tablesorter(); 
			
		$(".mytext").keyup(function() {
			var id=($(this).attr("id"));
			//We make sure you can't edit the Admin user
			if(id!=1) {
				$("#saveBtn"+id).css("background-color","#EFFFFF");
				//$("#updateBtn"+id).css("font-weight","bold");
				$("#saveBtn"+id).removeAttr("disabled");
			}
		});

		$(".mySelect").change(function() {
			var id=($(this).attr("id"));
			//We make sure you can't edit the Admin user
			if(id!=1) {
				$("#saveBtn"+id).css("background-color","#EFFFFF");
				//$("#updateBtn"+id).css("font-weight","bold");
				$("#saveBtn"+id).removeAttr("disabled");
			}
		});			

		//We need to use live here to allow multiple password updates
		$("[name=password]").live('focus',function() {
			var id=($(this).attr("id"));
			var val=$(this).val();
			if(val=="******") {
				//$(this).val("");
				$("#pwd"+id).html('<table border="0"><tr><td>New password    :</td><td><input type="password" name="password1'+id+'"></td></tr><tr><td>Confirm password   :</td><td><input type="password" name="password2'+id+'"></td></tr></table>');
				//We enable the save btn
				if(id!=1) {
					$("#saveBtn"+id).css("background-color","#EFFFFF");
					//$("#updateBtn"+id).css("font-weight","bold");
					$("#saveBtn"+id).removeAttr("disabled");
				}
			}
		});

		$(".red").click(function() {
			var id=($(this).attr("id"));
			if($(this).val()=="Save") {
				//check that the password if not ***** than check pwd 1 = pwd 2
				//check that the passwords are not blank 
				//remove the password inputs
				id=id.replace("saveBtn","");
				//alert("We save ID:" + id);

				var usercode = $('#'+id+' [name=usercode]').val();
				var title = $('#'+id+' [name=title]').val();
				var name = $('#'+id+' [name=name]').val();				
				var surname = $('#'+id+' [name=surname]').val();
				var active = $('#'+id+' [name=active]').val();

				var hide_pwd = false;

				if($('#'+id+' [name=password]').length == 0) {
					//we check passwords
					var pwd1 = $('[name=password1'+id+']').val();
					var pwd2 = $('[name=password2'+id+']').val();
					if(pwd1.length == 0) {
						alert("Please provide a new password.");
						return;
					}
					if(pwd1 != pwd2) {
						alert("The passwords must be equal.");
						return;
					}
					var hide_pwd = true;	 
				} else {
					var pwd = $('#'+id+' [name=password]').val();
				}			

				if(hide_pwd == true) {
						$("#pwd"+id).html('<input style="width: 100px" id="'+id+'" class="mytext" type="text" name="password" value="******"></input>');
						var data = 'password='+pwd1+'&id='+id+'&usercode='+usercode+'&title='+title+'&name='+name+'&surname='+surname+'&active='+active;	
				}
				else {
					var data = 'id='+id+'&usercode='+usercode+'&title='+title+'&name='+name+'&surname='+surname+'&active='+active;
				}
				
				//return;
				//alert("We save "+data);
				//return;
												
				$.ajax({
		         	type:"POST",
		         	url:"<?php echo(base_url());?>users/save",
		            data: data,
		            success: function (html) {		             					
		            	if(html=="TRUE") {		                            		
		            		//we only do this after a good save
		    				$("#saveBtn"+id).css("background-color","#CECEF6");
		    				$("#saveBtn"+id).attr("disabled","1");     		
		             	}
		             	else
			            	alert("Problem saving entry: " + html);		                            	
		            },
	                error: function (err) {
		            	alert("We got the following error:\n"+err.responseText);
		            }
		         });
				
				
			}
			else
			if($(this).val()=="Delete") {
				id=id.replace("delBtn","");
				//alert("We delete ID:" + id);
				$("#dialog-confirm").text('This item will be permanently deleted Id: '+id+' Are you sure?' );

				$("#dialog-confirm").dialog({
		            resizable: false,
		            height:200,
		            width: 300,
		            modal: true,
		            title: 'Do you want to delete ?',
		            buttons: {
		                'Delete ?': function() {
		                       
		                       
		                        var data = 'user_id='+id;
		                        $.ajax({
		                            type:"POST",
		                            url:"<?php echo(base_url());?>users/delete",
		                            data: data,
		                            success: function (html) {		             					
		             					if(html=="TRUE") {		                            		
		             						$("#dialog-confirm").dialog('close');
		             						$("tr[id="+id+"]").hide();
		             					}
		             					else
			             					alert("Problem deleting entry: " + html);
		                            	
		                            },
	                            	error: function (err) {
		                            	alert("We got the following error:\n"+err.responseText);
		                            }
		                        });
		                },
		                Cancel: function() {
		                   $(this).dialog('close');
		                }
		            }
		        });
				
				
			}	
		});

		

		
		
	});
</script>
<br></br>
<h2>Users</h2>
<div id="dialog-confirm"></div>
<div id="tblAgent"><input type="button" id="addBtn" value="Add Users"
	class="red"></input> <br></br>
<br></br>
<table border="1" cellpadding="4" id="myTable">
	<thead>
		<tr>
			<th>Id</th>
			<th>Username</th>
			<th>Title</th>
			<th>First Name</th>
			<th>Surname</th>
			<th>Password</th>
			<th>Active</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<!-- <th>Roles</th>  -->
		</tr>
	</thead>
	<tbody>
	<?php

	foreach($all_users as $user) { ?>
		<tr id="<?php print $user["id"];?>">
		<?php
		//We check for the Admin user
		if($user["id"]=="19") { ?>
			<td>1</td>
			<td><input readonly style="width: 100px"
				id="<?php print $user["id"];?>" class="mytext" type="text"
				name="usercode" value="<?php print $user["usercode"];?>"></input></td>
			<td><input readonly style="width: 100px"
				id="<?php print $user["id"];?>" class="mytext" type="text"
				name="title" value="<?php print $user["title"];?>"></input></td>
			<td><input readonly style="width: 100px"
				id="<?php print $user["id"];?>" class="mytext" type="text"
				name="name" value="<?php print $user["name"];?>"></input></td>
			<td><input readonly style="width: 100px"
				id="<?php print $user["id"];?>" class="mytext" type="text"
				name="surname" value="<?php print $user["surname"];?>"></input></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<!-- <td><select disabled name="active" class="mytext">
				<option value="1">Yes</option>
			</select></td>
			<td><input disabled id="<?php print $user["id"];?>" class="red"
				type="button" value="Delete" style="width: 70px"></input></td>
			<td><input disabled id="saveBtn<?php print $user["id"];?>"
				class="red" type="button" value="Save" style="width: 70px"></input></td>
				 -->
				<?php } else { ?>
			<td><?php print $user["id"]; ?></td>
			<td><input style="width: 100px" id="<?php print $user["id"];?>"
				class="mytext" type="text" name="usercode"
				value="<?php print $user["usercode"];?>"></input></td>
			<td><input style="width: 100px" id="<?php print $user["id"];?>"
				class="mytext" type="text" name="title"
				value="<?php print $user["title"];?>"></input></td>
			<td><input style="width: 100px" id="<?php print $user["id"];?>"
				class="mytext" type="text" name="name"
				value="<?php print $user["name"];?>"></input></td>
			<td><input style="width: 100px" id="<?php print $user["id"];?>"
				class="mytext" type="text" name="surname"
				value="<?php print $user["surname"];?>"></input></td>
			<td>
			<div id="pwd<?php print $user["id"];?>"><input style="width: 100px"
				id="<?php print $user["id"];?>" class="mytext" type="text"
				name="password" value="******"></input></div>
			</td>
			<td><select name="active" id="<?php print $user["id"];?>"
				class="mySelect">
				<option <?php if($user["active"]=="1") print " selected"?> value="1">Yes</option>
				<option <?php if($user["active"]=="0") print " selected"?> value="0">No</option>
			</select></td>
			<td><input id="<?php print $user["id"];?>" class="red" type="button"
				value="Delete" style="width: 70px"></input></td>
			<td><input disabled id="saveBtn<?php print $user["id"];?>"
				class="red" type="button" value="Save" style="width: 70px"></input></td>
				<?php } ?>
		</tr>
		<?php } ?>
	</tbody>
</table>
</div>
