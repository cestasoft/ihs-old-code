<link type="text/css" href="<?php echo(base_url());?>css/jquery.ui.all.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo(base_url());?>jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>jquery/ui/jquery-ui-1.8.5.custom.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>jquery/jquery.tablesorter.min.js"></script> 

<script type="text/javascript">	 
	$(document).ready(function(){

		$("#addBtn").click(function() {
			//alert("Click");
			window.location = '<?php echo(base_url());?>principals/add';
			//document.location.href = '<?php echo(base_url());?>principals/add';
		});

		$("#myTable").tablesorter(); 
			
		$(".mytext").keyup(function() {
			var id=($(this).attr("id"));
			$("#saveBtn"+id).css("background-color","#EFFFFF");
			//$("#updateBtn"+id).css("font-weight","bold");
			$("#saveBtn"+id).removeAttr("disabled");
		});		

		$(".red").click(function() {
			var id=($(this).attr("id"));
			if($(this).val()=="Save") {
				id=id.replace("saveBtn","");
				//alert("We save ID:" + id);
				//$("#saveBtn"+id).css("background-color","#CECEF6");
				//$("#saveBtn"+id).attr("disabled","1");
				var data = 
					'id='+id
					+'&name='+$('#'+id+' [name=Name]').val()
					+'&contact_name='+$('#'+id+' [name=ContactName]').val()
					+'&contact_tel='+$('#'+id+' [name=ContactTelNo]').val()
					+'&contact_fax='+$('#'+id+' [name=ContactFaxNo]').val()
					+'&contact_email='+$('#'+id+' [name=ContactEmail]').val();		   

				//alert(data);
				//return; 
				
				$.ajax({
		         	type:"POST",
		         	url:"<?php echo(base_url());?>principals/save",
		            data: data,
		            success: function (html) {		             					
		            	if(html=="TRUE") {		                            		
		            		//we only do this after a good save
		    				$("#saveBtn"+id).css("background-color","#CECEF6");
		    				$("#saveBtn"+id).attr("disabled","1");     		
		             	}
		             	else
			            	alert("Problem saving entry: " + html);		                            	
		            },
	                error: function (err) {
		            	alert("We got the following error:\n"+err.responseText);
		            }
		         });
			}
			else
			if($(this).val()=="Delete") {
				id=id.replace("delBtn","");
				//alert("We delete ID:" + id);
				$("#dialog-confirm").text('This item will be permanently deleted Id: '+id+' Are you sure?' );

				$("#dialog-confirm").dialog({
		            resizable: false,
		            height:220,
		            width: 400,
		            modal: true,
		            title: 'Do you want to delete ?',
		            buttons: {
		                'Delete ?': function() {
		                       
		                       
		                        var data = 'Id='+id;
		                        $.ajax({
		                            type:"POST",
		                            url:"<?php echo(base_url());?>principals/delete",
		                            data: data,
		                            success: function (html) {		             					
		             					if(html=="TRUE") {		                            		
		             						$("#dialog-confirm").dialog('close');
		             						$("tr[id="+id+"]").hide();
		             					}
		             					else
			             					alert("Problem deleting entry: " + html);
		                            	
		                            },
	                            	error: function (err) {
		                            	alert("We got the following error:\n"+err.responseText);
		                            }
		                        });
		                },
		                Cancel: function() {
		                   $(this).dialog('close');
		                }
		            }
		        });
				
				
			}	
		});

		

		
		
	});
</script>

<br></br>
<h2>Principals</h2>
<br></br>
<div id="dialog-confirm"></div>
<div>
<input type="button" id="addBtn" value="Add Prinicpal" class="red"></input>
<br></br>
<br></br>

<table border="1" cellpadding="3" id="myTable">
<thead>
	<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Contact Name</th>
		<th>Contact Telephone No.</th>
		<th>Contact Fax No.</th>
		<th>Contact Email</th>		
		<th></th>
		<th></th>
	</tr>
</thead>
<tbody>
<?php

//print_r($data);

foreach($data as $i) {
	?>
	
		<tr id="<?php print $i["Id"];?>">
				<td><?php print $i["Id"];?></td>
				<td><input id="<?php print $i["Id"];?>" class="mytext" type="text" name="Name" value="<?php print $i["Name"];?>"></input></td>
				<td><input id="<?php print $i["Id"];?>" class="mytext" type="text" name="ContactName" value="<?php print $i["ContactName"];?>"></input></td>
				<td><input id="<?php print $i["Id"];?>" class="mytext" type="text" name="ContactTelNo" value="<?php print $i["ContactTelNo"];?>"></input></td>
				<td><input id="<?php print $i["Id"];?>" class="mytext" type="text" name="ContactFaxNo" value="<?php print $i["ContactFaxNo"];?>"></input></td>
				<td><input id="<?php print $i["Id"];?>" class="mytext" type="text" name="ContactEmail" value="<?php print $i["ContactEmail"];?>"></input></td>				
				<td><input id="delBtn<?php print $i["Id"];?>" class="red" type="button" value="Delete" style="width:70px"></input></td>
				<td><input disabled=1 id="saveBtn<?php print $i["Id"];?>" class="red" type="button" value="Save" style="width:70px"></input></td>
		</tr>
	<?php 
	
}
?>
</tbody>
</table>
</div>