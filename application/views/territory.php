<script type="text/javascript">

function EditTerritory(id) {

	//$('#edit'+id).html();

	//$('tr[title=' + id + ']').each(function() { $(this).html("Loading..."); });
	
	$.post("<?php echo base_url(); ?>/territory/edit_territory", { id: id },
			function(data) {
				$('tr[title=' + id + ']').each(function() { $(this).hide(); });			
				$('#' + id).after('<tr id="tmp' + id + '"><td colspan="7">' + data + '</td></tr>');
	});
	
}

function SaveTerritory(id) {
	
	$.ajaxSetup({ cache: false });
	
	$.ajax({
	        type	: "POST",
			url 	: "<?php echo base_url(); ?>/territory/save_territory", 
			timeout	: 60000,
			dataType: "text",
			data	: { id : id, name 		: $('[name=terr_name' + id + ']').val(),
						terr_rating			: $('[name=terr_rating' + id + ']').val(),
						PhysicalAddress1	: $('[name=PhysicalAddress1' + id + ']').val(),
						PhysicalAddress2	: $('[name=PhysicalAddress2' + id + ']').val(),
						PhysicalAddress3	: $('[name=PhysicalAddress3' + id + ']').val(),
						PhysicalAddress4	: $('[name=PhysicalAddress4' + id + ']').val(),
						PhysicalCode		: $('[name=PhysicalCode' + id + ']').val(),
						terr_agent			: $('[name=terr_agent' + id + ']').val(),
						Province			: $('[name=Province' + id + ']').val(),
						TelNo				: $('[name=TelNo' + id + ']').val()
				 },
			success	: function(data) {
						data =  jQuery.trim(data);
						if(data == 'TRUE') {
							$('#' + id).html(GetTerritory(id));	
						} else {
							alert("Problem savinging customer.");
							alert("Problem from server.");
		                 	$('[name=Save' + id + ']').val("Save");
		             		$('[name=Cancel' + id +']').val("Cancel");
		             		$('[name=Save' + id + ']').removeAttr("disabled");
		             		$('[name=Cancel' + id +']').removeAttr("disabled");
						}		    
				 },
			error: function (xhr, ajaxOptions, thrownError) {
                 	alert("Problem from server.");
                 	$('[name=Save' + id + ']').val("Save");
             		$('[name=Cancel' + id +']').val("Cancel");
             		$('[name=Save' + id + ']').removeAttr("disabled");
             		$('[name=Cancel' + id +']').removeAttr("disabled");             				                    
                 	//alert(xhr.statusText);		                    
                 	//alert(thrownError);
             	},
           beforeSend: function(data) {
             		$('[name=Save' + id + ']').val("In progress");
             		$('[name=Cancel' + id +']').val("In progress");
             		$('[name=Save' + id + ']').attr("disabled", "true");
             		$('[name=Cancel' + id +']').attr("disabled", "true");
             	}
	});		
}

function validateInput(id) {
	var problem = false;
	if($.trim($('[name=terr_name' + id + ']').val()) == "") { alert("Please provide a name.");	problem = true; }
	if($.trim($('[name=Province' + id + ']').val()) == "0") { alert("Please select a province."); problem = true; }
	if(problem == false) {		
		SaveTerritory(id);
	}	
}


function GetTerritory(id) {
	$.post("<?php echo base_url(); ?>/territory/get_territory", { id: id },
			function(data) {
				//clear the edit block
				$('#tmp' + id).html('');
				$('#tmp' + id).hide();
				//set the current content to blank
				$('#' + id).html('');
				//add the updated data back
				$('#' + id).after(data);
				//show the tr
				$('#' + id).show();
	});
	
}

//function cancelInput(id) {
	//$('#' + id).html(GetTerritory(id));

	//$('#' + id).html('');
//}

function cancelInput(id) {

	$('tr[title=' + id + ']').each(function() { $(this).show(); });
	
	$('#tmp' + id).html('');
	$('#tmp' + id).hide();
}

</script>


<?php

$agent_name 	= "";
$select_agent	= "";
//print_r($all_agents);
foreach($all_agents as $agent) {
		$select_agent.="<option value=\"".trim($agent['agentcode'])."\">".$agent['title']." ".$agent['name']." ".$agent['surname']."</option>";
		if(isset($agent) && isset($agent["agentcode"]) && isset($agent_id) && $agent["agentcode"] == $agent_id) {
			$agent_name = $agent["name"]." ".$agent["surname"];
		}
}

$select_rating="
	<option value=\"A\">A</option>
	<option value=\"B\">B</option>
	<option value=\"C\">C</option>
	<option value=\"D\">D</option>	
";


?>
<div id="tblTerr">
<table border="1" cellpadding="4">
	<thead>
		<tr>
			<th>ID</th>
			<th>Area</th>
			<th>Name</th>
			<th>Agent</th>
			<th>Rating</th>
			<th>&nbsp;</th>
			<th width=150px>&nbsp;</th>					
		</tr>
	</thead>
	<tbody>
<?php

if($_SESSION['AGENT']=="YES") {
	$read_only = true;
} else 
	$read_only = false; 

foreach($all_territories as $terr) { ?>
	<tr id="<?php echo $terr['id'];?>" title="<?php echo $terr['id'];?>">
		<td><?php echo $terr['id'];?></td>
		<td><?php echo $terr['PhysicalAddress4'];?></td>
		<td><a href='#' onclick="EditTerritory(<?php echo $terr['id'];?>);"><?php echo $terr['name'];?></a></td>
		
		<?php if(isset($show_select)&&$show_select=="select_site_visit") { ?>
						
			<td><select disabled name="agent_id" terr_id="<?php echo $terr['id'];?>">
			<?php
				$str="value=\"".trim($terr['agent_id'])."\"";
				$this_select=str_replace($str,"selected ".$str,$select_agent); 
				echo $this_select;
			?>
		</select>
		</td>
		<td><select disabled name="rating" terr_id="<?php echo $terr['id'];?>">
			<?php
			$str="value=\"".strtoupper(trim($terr['rating']))."\"";
			$this_select=str_replace($str,"selected ".$str,$select_rating); 
			echo $this_select; 
			?>
		</select>		
		</td>
		<td>
			<div id="<?php echo $terr['id'];?>"></div>
			<input type="button" value="Select" onclick="selectSiteVisit(<?php echo $terr['id'];?>,'<?php echo $terr['name'];?>','<?php echo $terr['rating'];?>')" id="<?php echo $show_select;?>" terr_id="<?php echo $terr['id'];?>">			
		</td>
			
		<?php }
		else if(isset($show_select)&&($show_select=="select_parent"||$show_select=="select_child")) { 				
			?>	
			
		<td><select disabled name="agent_id" terr_id="<?php echo $terr['id'];?>">
			<?php
			$str="value=\"".trim($terr['agent_id'])."\"";
			$this_select=str_replace($str,"selected ".$str,$select_agent); 
			echo $this_select;
			?>
		</select>
		</td>
		<td><select disabled name="rating" terr_id="<?php echo $terr['id'];?>">
			<?php
			$str="value=\"".strtoupper(trim($terr['rating']))."\"";
			$this_select=str_replace($str,"selected ".$str,$select_rating); 
			echo $this_select; 
			?>
		</select>		
		</td>
		<td>
			<div id="<?php echo $terr['id'];?>"></div>
			<input type="button" value="Select" onclick="<?php echo $show_select;?>(<?php echo $terr['id'];?>,'<?php echo $terr['name'];?>')" id="<?php echo $show_select;?>" terr_id="<?php echo $terr['id'];?>">			
		</td>	
		
		<?php } else { ?>
		<td>
		<?php 
			if(!$read_only) { ?>
				<select name="agent_id" terr_id="<?php echo $terr['id'];?>">
				<?php
				$str="value=\"".trim($terr['agent_id'])."\"";
				$this_select=str_replace($str,"selected ".$str,$select_agent); 
				echo $this_select;
				?>
				</select>
			<?php } else {
				echo $agent_name;
			}
			?>
			
		</td>
		<td><select name="rating" terr_id="<?php echo $terr['id'];?>">
			<?php
			$str="value=\"".strtoupper(trim($terr['rating']))."\"";
			$this_select=str_replace($str,"selected ".$str,$select_rating); 
			echo $this_select; 
			?>
		</select>		
		</td>
		<td>
			<input ter_name="<?php echo $terr['name'];?>" type="button" name="del" value="Delete" id="<?php echo $terr['id'];?>"/>
		</td>
		<td>
			<div id="div<?php echo $terr['id'];?>"></div>			
		</td>
		<?php } ?>
	</tr>
<?php 	
}

?>
	</tbody>
</table>
</div>
</body>
