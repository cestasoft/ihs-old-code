<script type="text/javascript">	 
	$(document).ready ( function() {
		$('#compile').click( function() {
   			document.location.href='process_files/compile/' + $('select[name="year"]').val() + '/' + $('select[name="month"]').val() + '/' + $('select[name="filename"]').val();
		} );
	});
</script>

<h2>Please select a date and file to process:</h2>

<table border="0" cellpadding="10">
	<tr>
		<td>Year</td>
		<td>
			<select name="year">
				<option value="2011">2011</option>
				<option value="2012">2012</option>
				<option value="2013">2013</option>
				<option value="2014">2014</option>
				<option value="2015">2015</option>
				<option value="2016">2016</option>
				<option value="2017">2017</option>
				<option value="2018">2018</option>
				<option value="2019">2019</option>
				<option value="2020">2020</option>				
			</select>
		</td>
	</tr>
	<tr>
		<td>Month</td>
		<td>
			<select name="month">
				<option value="01">January</option>
				<option value="02">Feburary</option>
				<option value="03">March</option>
				<option value="04">April</option>
				<option value="05">May</option>
				<option value="06">June</option>
				<option value="07">July</option>
				<option value="08">August</option>
				<option value="09">September</option>
				<option value="10">October</option>
				<option value="11">November</option>
				<option value="12">December</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>File</td>
		<td>
			<select name="filename">
			<?php foreach($files as $file) { 

				echo '<option value="'.$file["title"].'">'.$file["title"]."</option>\r\n";
			}?>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<button id="compile">Compile Data</button>
		</td>
	</tr>
</table>