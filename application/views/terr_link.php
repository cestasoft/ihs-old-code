<script type="text/javascript" src="<?php echo(base_url());?>jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript">	 
	 $(document).ready(function(){	
		 				
		$('#inputParent').keyup(function(event) {
			if($(this).val().length > 3) {
				//alert($(this).val());
				var filter = $(this).val();
				$("#tblParent").html("<small>Query in progress...</small>");
				$.post("<?php echo base_url(); ?>/territory/get_territories_via_filter", { filter: filter, select: 'select_parent' },
					function(data) {
					$("#tblParent").html(data);																				
			});
			}			
		});

		$('#inputChild').keyup(function(event) {
			if($(this).val().length > 3) {
				//alert($(this).val());
				var filter = $(this).val();
				$("#tblChild").html("<small>Query in progress...</small>");
				$.post("<?php echo base_url(); ?>/territory/get_territories_via_filter", { filter: filter, select: 'select_child' },
					function(data) {
					$("#tblChild").html(data);																				
				});
			}			
		});
		
		$('#btnLink').click(function() {
			//var the_terr_id = $(this).attr("terr_id");
			//alert("We set" + the_terr_id);			
			var parentId = $('#parentId').val();
			var childId = $('#childId').val();
			var childName = $('#childName').val();
			$.post("<?php echo base_url(); ?>/territory/add_link", { parentId: parentId, childId: childId, childName: childName },
					function(data) {
						alert("The Territories have been successfully linked.");	
						window.location = '<?php echo(base_url());?>territory/link';										
				});
		});
		
	});

		function select_parent(id,name) 
		{
			//alert(id);
			$('#tblParent').html('<input type="hidden" id="parentId" name="parentId" value="'+id+'"> Parent: ' + name);
		}

		function select_child(id,name) 
		{
			//alert(id);
			$('#tblChild').html('<input type="hidden" id="childId" name="childId" value="'+id+'"><input type="hidden" id="childName" name="childName" value="'+name+'"> Child: ' + name);
		}
</script>

</head>
<body>
<br>
<br>
<table border="0">
	<tr>
			<td>Type name of Parent Customer:</td>
			<td><input id="inputParent" type="text" name="parent_terr"></input></td>
	</tr> 
	<tr>
			<td colspan="2"><div id="tblParent"></div></td>
	</tr>	
	<tr>
			<td>Type name of Child Customer:</td>
			<td><input id="inputChild" type="text" name="child_terr"></input></td>
	</tr>
	<tr>
			<td colspan="2"><div id="tblChild"></div></td>
	</tr>
	<tr>
			<td>&nbsp;</td>
	</tr> 
	<tr>
		<td colspan="2" align="center"><input type="button" id="btnLink" class="btn" value="Link"></input></td>
	</tr>
</table>
</body>
