<?php
 //print_r($product_sales_by_month);
 
 ?>
 
 <div>
<br></br>
<h2>Product Sales by Month:</h2>
<?php 
//print_r($total_sales_by_month);
//print getcwd();
?>
<?php 
 
// Standard inclusions     
include("chart/pChart/pData.class");  
include("chart/pChart/pChart.class");  
   
// Dataset definition   
$DataSet = new pData;

$my_y_data=array();
$my_x_data=array();

$all_product_groups=array();
$all_months=array();

foreach($product_sales_by_month as $i) {
	$importcode=$i["importcode"];
	switch($importcode) {
		case "01/01_/2009": $importcode="2009 Jan";break;
		case "02/01_/2009": $importcode="2009 Feb";break;
		case "07/01_/2008": $importcode="2008 Jul";break;
		case "08/01_/2008": $importcode="2008 Aug";break;
		case "09/01_/2008": $importcode="2008 Sep";break;
		case "10/01_/2008": $importcode="2008 Oct";break;
		case "11/01_/2008": $importcode="2008 Nov";break;
		case "12/01_/2008": $importcode="2008 Dec";break;
		case "12/01_/2009": $importcode="2009 Dec";break;
	}
	$my_y_data[$importcode][$i["name"]][]=$i["salevalue"];
	$all_product_groups[$i["name"]]=1;
	$all_months[$importcode]=1;
	//$my_x_data[$i["name"]][]=$i["importcode"];
}

$cnt=1;
foreach($all_product_groups as $k=>$i) {
	$data=array();
	foreach($all_months as $month=>$ii) {
		if(isset($my_y_data[$month])&&isset($my_y_data[$month][$k]))
			$data[]=$my_y_data[$month][$k][0];
		else
			$data[]=0;
	}
	//print "Data set for $k";
	//print_r($data);
	$DataSet->AddPoint($data,"Serie".$cnt);
	$DataSet->SetSerieName($k,"Serie".$cnt);
	$cnt++;
}

//return;
//print_r($my_y_data);
//print_r($my_x_data);

$months=array();
foreach($all_months as $month=>$ii) {
	$months[]=$month;
}


$DataSet->AddPoint($months,"Serie20");  
$DataSet->SetAbsciseLabelSerie("Serie20"); 

//return;

//$DataSet->AddPoint($my_y_data);
//$DataSet->AddPoint(array(100,400,300,200,300,300));  

//$DataSet->AddPoint($all_months,"Serie2");  
//$DataSet->SetAbsciseLabelSerie("Serie2"); 

$DataSet->AddAllSeries();

//$DataSet->SetSerieName("Total Sales per Month","Serie1");

$DataSet->SetXAxisName("Month");
$DataSet->SetYAxisName("Rand");     
   
// Initialise the graph  
$Test = new pChart(1200,800);  
$Test->setFontProperties("chart/Fonts/tahoma.ttf",10);  
$Test->setGraphArea(100,30,1000,600);  
$Test->drawGraphArea(252,252,252);  
$Test->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_NORMAL,150,150,150,TRUE,0,2);  
$Test->drawGrid(4,TRUE,230,230,230,255);  
   
// Draw the line graph  
$Test->drawLineGraph($DataSet->GetData(),$DataSet->GetDataDescription());  
$Test->drawPlotGraph($DataSet->GetData(),$DataSet->GetDataDescription(),3,2,255,255,255);  
   
// Finish the graph  
$Test->setFontProperties("chart/Fonts/tahoma.ttf",8);  
$Test->drawLegend(1000,35,$DataSet->GetDataDescription(),255,255,255);  
$Test->setFontProperties("chart/Fonts/tahoma.ttf",10);  
$Test->drawTitle(60,22,"Total Sales per Month",50,50,50,585);  
$Test->Render("chart/tmp/testing.png");  
 
?>
<img src="/chart/tmp/testing.png"></img>
</div>
 