<script type="text/javascript" src="<?php echo(base_url());?>jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>jquery/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>jquery/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="<?php echo(base_url());?>jquery/ui/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
	$(document).ready(function() {					
		mainmenu();
	});

	function mainmenu() {
		$(" #nav ul ").css({display: "none"}); // Opera Fix
		$(" #nav li").hover(function(){
				$(this).find('ul:first').css({visibility: "visible",display: "none"}).show(400);
				},function(){
				$(this).find('ul:first').css({visibility: "hidden"});
				});
	}
</script>
</head>

<body>
<div style="background-color: #F2F2F2;">
<table width="100%" style="border:1px solid;" cellpadding="0" cellspacing="0">
	<tr>
		<td valign="bottom" align="left"><font size="+3">&nbsp;ESMS - IHS FMCG</font>
				<img style="float:right;" width="20%" height="140px" src="<?php echo(base_url());?>img/zen.jpg"></img>
		</td>
	</tr>
</table>
</div>

<div class="menu">
<ul id="nav">

<?php if(isset($_SESSION['AGENT'])&&$_SESSION['AGENT']=='YES') { ?>			
	<li><a href="<?php echo(base_url());?>territory">Customers</a>
		<ul>
			<li><a href="<?php echo(base_url());?>territory">View / Edit</a></li>			
			<li><a href="<?php echo(base_url());?>territory/add">Add</a></li>
			<!--  <li><a href="<?php echo(base_url());?>territory/delete">Delete</a></li> -->
			<!--  <li><a href="<?php echo(base_url());?>territory/undelete">Undelete</a></li>  -->
		</ul>
	</li>			
	<li><a href="<?php echo(base_url());?>sites">Site Visits</a></li>	
	<li><a href="<?php echo(base_url());?>login/logout">Logout</a></li>  									
<?php 		} else { ?>
			<li><a href="<?php echo(base_url());?>territory">Customers</a>
		<ul>
			<li><a href="<?php echo(base_url());?>territory">View / Edit</a></li>			
			<li><a href="<?php echo(base_url());?>territory/link">Link</a></li>
		</ul>
	</li>
	<li class="sub"><a href="<?php echo(base_url());?>graphs">Graphs</a></li>		
	<li class="sub"><a href="<?php echo(base_url());?>agents">Agents</a></li>	
  	<li class="sub"><a href="<?php echo(base_url());?>products">Products</a></li>
	<li class="sub"><a href="<?php echo(base_url());?>product_groups">Product Groups</a></li>
	<li><a href="<?php echo(base_url());?>principals">Principals</a></li>
	<li><a href="<?php echo(base_url());?>sites">Site Visits</a></li>
	<li><a href="<?php echo(base_url());?>reports">Reports</a>
		<ul>
			<li><a href="<?php echo(base_url());?>reports">View Reports</a></li>
			<li><a href="<?php echo(base_url());?>reportheadings">Report Headings</a></li>
			<li><a href="<?php echo(base_url());?>upload">Upload Data</a></li>
			<li><a href="<?php echo(base_url());?>process_files">Process Data</a></li>
			<li><a href="<?php echo(base_url());?>process_files/clear_data">Clear Data</a></li>
		<!-- 	<li><a href="/report_headings">Report Headings</a></li>  -->
		</ul>	
	</li>
	<?php 
	//We need to check here if user is an admin
	?>
	<li><a href="<?php echo(base_url());?>users">Manage Users</a></li>
	<?php 
	//end admin check
	?>
	<li><a href="<?php echo(base_url());?>login/logout">Logout</a></li>  			
<?php 		} ?>		
		
	</ul>
<p class="timer">Page rendered in {elapsed_time} seconds</p>
</div>
<br></br>
<br></br>
