<script type="text/javascript">	 
	 $(document).ready(function(){

		////What to show for each graph
		 		
		 $("#dates").hide();
		 $("#agents").hide();
		 $("#submitBtn").hide();

		$("#report").change(function () {

			$("#report option:selected").each(function(){
				if($(this).val()!='') $("#submitBtn").show();
				else $("#submitBtn").hide(); 
				
				if($(this).attr("csv")==1) {
					$("#csv").show();
				} else {
					//hide csv
					$("#csv").hide();
				}

				if($(this).attr("dates")==1) {
					//show dates
					$("#dates").show();
				} else {
					//hide dates
					$("#dates").hide();
				}

				if($(this).attr("agents")==1) {
					//show dates
					$("#agents").show();
				} else {
					//hide dates
					$("#agents").hide();
				}		
			});
							
		});

		//Date picker stuff
		var dates = $('#from, #to').datepicker({
			defaultDate: "-1w",
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy/mm/dd',
			onSelect: function(selectedDate) {
				var option = this.id == "from" ? "minDate" : "maxDate";
				var instance = $(this).data("datepicker");
				var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
				dates.not(this).datepicker("option", option, date);
				//remDate.datepicker("option", option, date);
			}
		});
				 		
	 });
</script>


<form method="post" action="<?php echo base_url(); ?>graphs/show_graph">
<h2>Select Graph</h2>

<?php 
//foreach($report_list as $k)
	//print_r($k);

?>
<br></br>
<select name="report" id="report">
<option value="">Please select a graph</option>
<?php foreach($report_list as $k=>$report) {
	print "<option value=\"".$k."\" agents=\"".$report["report_select_agent"]."\" csv=\"".$report["report_export_csv"]."\" dates=\"".$report["report_select_dates"]."\">".$report["report_name"]."</option>";
}
?>
</select>
<br></br>
<br></br>
<div id="dates">
	<table border="0">
		<tr>
			<td width="180px">Start Date:</td>
			<td><input autocomplete="off" type="text" name="from" id="from"></input></td>
		</tr>
		<tr>
			<td>End Date:</td>
			<td><input autocomplete="off" type="text" name="to" id="to"></input></td>
		</tr>				
	</table>
</div>
<div id="agents">
	<table border="0">
		<tr>
			<td width="180px">Select agent:</td>
			<td>
	<select name="agent">
		<option value="">All Agents</option>
		<?php 
		foreach($agents as $agent) {
			print "<option value=".$agent["agentcode"].">".$agent["name"]." ".$agent["surname"]."</option>";
		}
		?>
		
	</select>
			</td>
		</tr>
	</table>
</div>
<br/>
<input class="btn" id="submitBtn" type="submit" value="Run Report"></input>

</form>