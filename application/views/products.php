<link type="text/css" href="<?php echo(base_url());?>css/jquery.ui.all.css" rel="Stylesheet" />
<script type="text/javascript" src="<?php echo(base_url());?>jquery/ui/jquery-ui-1.8.5.custom.min.js"></script>

<script type="text/javascript">	 
	$(document).ready(function(){

		//This is the filter stuff
		//add index column with all content.
		 $(".filterable tr:has(td)").each(function(){
		   var t = $(this).text().toLowerCase(); //all row text
		   $("<td class='indexColumn'></td>")
		    .hide().text(t).appendTo(this);
		 });//each tr
		 
		 $("#filterTextBox").keyup(function(){
		   var s = $(this).val().toLowerCase().split(" ");
		   //show all rows.
		   $(".filterable tr:hidden").show();
		   $.each(s, function(){
		       $(".filterable tr:visible .indexColumn:not(:contains('"
		          + this + "'))").parent().hide();
		   });//each
		 });//key up.
		 //End Filter Stuff
				 		
		$(".myselect").change(function() {
			//alert("FUN"+$(this).attr("id"));			
			var id=($(this).attr("id"));
			$("#saveBtn"+id).css("background-color","#EFFFFF");
			//$("#updateBtn"+id).css("font-weight","bold");
			$("#saveBtn"+id).removeAttr("disabled");
		});		

		$(".mytext").keyup(function() {
			var id=($(this).attr("id"));
			$("#saveBtn"+id).css("background-color","#EFFFFF");
			//$("#updateBtn"+id).css("font-weight","bold");
			$("#saveBtn"+id).removeAttr("disabled");
		});		

		$(".red").click(function() {
			//alert($(this).attr("id"));
			var id=($(this).attr("id"));
			if($(this).val()=="Save") {
				id=id.replace("saveBtn","");
				//alert("We save ID:" + id);				
				var data = 
					'id='+id
					+'&product_group='+$('#'+id+' [name=product_group]').val()
					+'&comm='+$('#'+id+' [name=comm]').val();

				//alert(data);
				//return; 
				
				$.ajax({
		         	type:"POST",
		         	url:"<?php echo(base_url());?>products/save",
		            data: data,
		            success: function (html) {		             					
		            	if(html=="TRUE") {		                            		
		            		//we only do this after a good save
		    				$("#saveBtn"+id).css("background-color","#CECEF6");
		    				$("#saveBtn"+id).attr("disabled","1");     		
		             	}
		             	else
			            	alert("Problem saving entry: " + html);		                            	
		            },
	                error: function (err) {
		            	alert("We got the following error:\n"+err.responseText);
		            }
		         });
				
			}
			else
			if($(this).val()=="Delete") {				
				//alert("We delete ID:" + id);
				$("#dialog-confirm").text('This item will be permanently deleted Id: '+id+' Are you sure?' );

				$("#dialog-confirm").dialog({
		            resizable: false,
		            height:220,
		            width: 400,
		            modal: true,
		            title: 'Do you want to delete ?',
		            buttons: {
		                'Delete ?': function() {
		                       
		                       
		                        var data = 'id='+id;
		                        $.ajax({
		                            type:"POST",
		                            url:"<?php echo(base_url());?>products/delete",
		                            data: data,
		                            success: function (html) {		             					
		             					if(html=="TRUE") {		                            		
		             						$("#dialog-confirm").dialog('close');
		             						$("tr[id="+id+"]").hide();
		             					}
		             					else
			             					alert("Problem deleting entry: " + html);
		                            	
		                            },
	                            	error: function (err) {
		                            	alert("We got the following error:\n"+err.responseText);
		                            }
		                        });
		                },
		                Cancel: function() {
		                   $(this).dialog('close');
		                }
		            }
		        });
				
				
			}	
		});

		

		
		
	});
</script>
<div id="dialog-confirm"></div>
<div>
<br></br>
<h2>Products</h2>
<br></br>
<div>
<?php 
$select_product_group="";
//print_r($product_groups);
//exit;
foreach($product_groups as $pg) {
		$select_product_group.="<option value=\"".trim($pg['id'])."\">".$pg['name']." </option>";
}
?>
Filter: <input type="text" id="filterTextBox" name="filterTextBox" />
<table border="1" cellpadding="3" class="filterable">
<thead>
	<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Product Group</th>
		<th>Commision %</th>
		<th></th>
		<th></th>
	</tr>
</thead>
<tbody>
<?php

//print_r($data);

foreach($data as $i) {
	?>
	
		<tr id="<?php print $i["Id"];?>">
				<td><?php print $i["Id"];?></td>
				<td><?php print $i["Name"];?></td>
				<td>
					<select name="product_group" style="width:120px" class="myselect" id="<?php print $i["Id"];?>">
						<?php 
							$str="value=\"".$i['ProductGroupId']."\"";
							print str_replace($str,"selected ".$str,$select_product_group); 
 				
						?>
					</select>
				</td>
				<td align="center"><input class="mytext" id="<?php print $i["Id"];?>" style="width:40px" type="text" value="<?php print $i["Comm"];?>" name="comm"></input></td>				
				<td><input id="<?php print $i["Id"];?>" class="red" type="button" value="Delete" style="width:70px"></input></td>
				<td><input id="saveBtn<?php print $i["Id"];?>" disabled="" class="red" type="button" value="Save" style="width:70px"></input></td>
		</tr>
	<?php 
	
}
?>
</tbody>
</table>
</div>