<?php

class Agent_model extends CI_Model  {

	/*
	function Agent_model() {
		parent::Model();
		$this->load->database();
	}*/
	
	public function __construct()   {
          $this->load->database(); 
   }
	
	function _hash_password($pwd) {
			$salt="This is the Salt";		
    		$this->load->helper('security');
    		$the_hash=sha1($salt.$pwd);
    		return $the_hash;
	}
	
	function check_password($username,$password) {
		//we return false on fail or id on success		
		$username=trim($username);//remove white space from user
		
		//$sql="select agentcode,password from agent where username = ".$this->db->escape($username)." and active = '1'";
		$sql="select agentcode,password from agent where username = ".$this->db->escape($username);
		$query = $this->db->query($sql);		
		$rs=$query->result_array();
		
		if(count($rs)==1) {
			$the_hash=$this->_hash_password($password);		
    		//print "we compare ".$rs[0]["password"]." with ".$the_hash;    
    		if($rs[0]["password"]==$the_hash) return 'AGENT_'.$rs[0]["agentcode"];    		    		
    	}
    	return false;			
	}
	
	function get_agent_details($id) {							
				
		$sql="select agentcode,title,name,surname from agent where agentcode = ".$this->db->escape($id);
			
		$query = $this->db->query($sql);		
    	$rs=$query->result_array();

    	if(count($rs)==1) {
    		return $rs[0];
	   	}
    	return $rs;
	}
	
	function delete_agent($agent_id) {
		//set terr to unalloc
		//$sql="delete from productsales where product_id =".$this->db->escape($id);
		//$this->db->query($sql);		
		$sql="delete from agent where agentcode = ".$this->db->escape($agent_id);
		$this->db->query($sql);		
		if($this->db->affected_rows()=="1") return true;
		else return false;	
	}
	
	function get_agent_name($agent_id) {
		$agent=$this->get_all_agents($agent_id);
			if(count($agent)==1)
		return $agent[0]["name"]." ".$agent[0]["surname"];
		else 
			return "Agent not found.";		
	}
	
	function add_agent(
		$title,
		$name,
		$surname,
		$telnoh,
		$physicaladdress1,
		$physicaladdress2,
		$physicaladdress3,
		$physicaladdress4,
		$physicalcode,
		$postaladdress1,
		$postaladdress2,
		$postaladdress3,
		$postaladdress4,		
		$postalcode,
		$username,
		$password
	) {
		
		$password=$this->_hash_password($password);	
		$sql="insert into agent (
		title,
		name,
		surname,
		telnoh,
		physicaladdress1,
		physicaladdress2,
		physicaladdress3,
		physicaladdress4,
		physicalcode,
		postaladdress1,
		postaladdress2,
		postaladdress3,
		postaladdress4,		
		postalcode,
		username,
		password
		) values (
		".$this->db->escape($title).",
		".$this->db->escape($name).",
		".$this->db->escape($surname).",
		".$this->db->escape($telnoh).",
		".$this->db->escape($physicaladdress1).",
		".$this->db->escape($physicaladdress2).",
		".$this->db->escape($physicaladdress3).",
		".$this->db->escape($physicaladdress4).",
		".$this->db->escape($physicalcode).",
		".$this->db->escape($postaladdress1).",
		".$this->db->escape($postaladdress2).",
		".$this->db->escape($postaladdress3).",
		".$this->db->escape($postaladdress4).",		
		".$this->db->escape($postalcode).",
		".$this->db->escape($username).",
		".$this->db->escape($password)."
		)";
		$this->db->query($sql);
		if($this->db->affected_rows()=="1") return true;
		else return false;		
	}
	
	function update($agent) {
		if(isset($agent["password"])&&$agent["password"]) {
			$password=$this->_hash_password($agent["password"]);	
			$sql="update agent set 
				title = ".$this->db->escape($agent["title"]).",
				name = ".$this->db->escape($agent["name"]).",
				surname = ".$this->db->escape($agent["surname"]).",
				telnoh = ".$this->db->escape($agent["telnoh"]).",
				physicaladdress1 = ".$this->db->escape($agent["physicaladdress1"]).",
				physicaladdress2 = ".$this->db->escape($agent["physicaladdress2"]).",
				physicaladdress3 = ".$this->db->escape($agent["physicaladdress3"]).",
				physicaladdress4 = ".$this->db->escape($agent["physicaladdress4"]).",
				physicalcode = ".$this->db->escape($agent["physicalcode"]).",
				postaladdress1 = ".$this->db->escape($agent["postaladdress1"]).",
				postaladdress2 = ".$this->db->escape($agent["postaladdress2"]).",
				postaladdress3 = ".$this->db->escape($agent["postaladdress3"]).",
				postaladdress4 = ".$this->db->escape($agent["postaladdress4"]).",
				postalcode = ".$this->db->escape($agent["postalcode"]).",
				username = ".$this->db->escape($agent["username"]).",
				password = ".$this->db->escape($password)."											
				where agentcode = ".$this->db->escape($agent["id"]);
		} else {
		$sql="update agent set 
				title = ".$this->db->escape($agent["title"]).",
				name = ".$this->db->escape($agent["name"]).",
				surname = ".$this->db->escape($agent["surname"]).",
				telnoh = ".$this->db->escape($agent["telnoh"]).",
				physicaladdress1 = ".$this->db->escape($agent["physicaladdress1"]).",
				physicaladdress2 = ".$this->db->escape($agent["physicaladdress2"]).",
				physicaladdress3 = ".$this->db->escape($agent["physicaladdress3"]).",
				physicaladdress4 = ".$this->db->escape($agent["physicaladdress4"]).",
				physicalcode = ".$this->db->escape($agent["physicalcode"]).",
				postaladdress1 = ".$this->db->escape($agent["postaladdress1"]).",
				postaladdress2 = ".$this->db->escape($agent["postaladdress2"]).",
				postaladdress3 = ".$this->db->escape($agent["postaladdress3"]).",
				postaladdress4 = ".$this->db->escape($agent["postaladdress4"]).",
				postalcode = ".$this->db->escape($agent["postalcode"]).",
				username = ".$this->db->escape($agent["username"])."											
				where agentcode = ".$this->db->escape($agent["id"]);
		}				
		$this->db->query($sql);
		if($this->db->affected_rows()=="1") return true;
		//We leave this here for debugging
		else return $sql;
		//else return false;
	}
	
	function get_all_agents($agent_id=FALSE) {
		
		$sql="select AgentCode,
				Title,
				Name,
				Surname,
				TelNoH,
				TelNoW,
				FaxNo,
				CellNo,
				PhysicalAddress1,
				PhysicalAddress2,
				PhysicalAddress3,
				PhysicalAddress4,
				PhysicalCode,
				PostalAddress1,
				PostalAddress2,
				PostalAddress3,
				PostalAddress4,
				PostalCode,
				username
 			from agent order by Name, Surname";		
		if($agent_id!==FALSE) $sql.=" where agentcode = $agent_id";
		$query = $this->db->query(strtolower($sql));		
    	return $query->result_array();
	}
}

?>
