<?php

class Product_group_model extends CI_Model {

	public function __construct()   {
          $this->load->database(); 
   }
	
	function get_all_product_groups() {
		
		$sql="select 
			id,
			name, 
			principalid
			from productgroup 
			order by name";		
		$query = $this->db->query($sql);		
    	return $query->result_array();
	}
	
	function delete_product_group($id) {	
		
		$sql="update product set productgroupid = null where productgroupid = ".$this->db->escape($id);
		$this->db->query($sql);		
		$sql="delete from productgroup where Id = ".$this->db->escape($id);
		$this->db->query($sql);
		if($this->db->affected_rows()=="1") return true;
		else return false;	
	}
	
	function add_product_group($name,$principal) {
		
		$sql="insert into productgroup(name,principalid) values (".$this->db->escape($name).",".$this->db->escape($principal).")";
		$this->db->query($sql);
		if($this->db->affected_rows()=="1") return true;
		else return false;
	}
	
	function save_product_group($id,$name,$principal) {
		
		$sql="update productgroup set name = ".$this->db->escape($name).", principalId = ".$this->db->escape($principal)." where id =".$this->db->escape($id);
		$this->db->query($sql);
		if($this->db->affected_rows()=="1") return true;
		else return false;
	}
}

?>