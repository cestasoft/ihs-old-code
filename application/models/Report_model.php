<?php

class Report_model extends CI_Model {

	public function __construct()   {
          $this->load->database(); 
   }

	function get_all_agents() {
		
		$sql="select AgentCode,
				Title,
				Name,
				Surname,
				TelNoH,
				TelNoW,
				FaxNo,
				CellNo,
				PhysicalAddress1,
				PhysicalAddress2,
				PhysicalAddress3,
				PhysicalAddress4,
				PhysicalCode,
				PostalAddress1,
				PostalAddress2,
				PostalAddress3,
				PostalAddress4,
				PostalCode
 			from agent";		
		$query = $this->db->query(strtolower($sql));
		return $query->result_array();
	}
	
	function get_site_visit_summary($from,$to) {
		$agents = $this->get_all_agents();
		
		$sql = "select count(*) as cnt,s.agent_id, t.rating, a.name, a.surname from sitevisits s, territory t, agent a 
 			where s.territory_id = t.id
			and s.agent_id = a.agentcode
			and s.datevisited >= '$from 00:00:00' and s.datevisited <= '$to 23:59:59'
			group by s.agent_id, t.rating";
		
		$query = $this->db->query(strtolower($sql));
		$rs=$query->result_array();
		return $rs;
	}
	
	function get_site_visits_agent($from,$to,$agent) {
		
		
		if($agent=="") {
		$sql="select a.name as name,a.surname as surname,s.datevisited as datevisited, t.name as t_name 
			from sitevisits as s, territory as t, agent as a
			where s.territory_id = t.id
			and t.agent_id = a.agentcode
			and s.datevisited >= '$from 00:00:00' and s.datevisited <= '$to 23:59:59'
			order by a.name,a.surname,datevisited,t_name";
		} else {
		$sql="select a.name as name,a.surname as surname,s.datevisited as datevisited, t.name as t_name 
			from sitevisits as s, territory as t, agent as a
			where s.territory_id = t.id
			and t.agent_id = a.agentcode
			and s.datevisited >= '$from 00:00:00' and s.datevisited <= '$to 23:59:59'
			and t.agent_id = $agent
			order by a.name,a.surname,datevisited,t_name";			
		}
		
		$query = $this->db->query(strtolower($sql));
		$rs=$query->result_array();
		return $rs;
		$ret=array();
		foreach($rs as $line) {			
			/*$ret=array_merge
			($ret,array
				($line["product_group_id"]=>array
					($line["agent_code"]=>array
						("comm"=>$line["comm"],"sales"=>$line["sales"])
					)
				)
			);	
			*/
			//$ret[$line["product_group_id"]][$line["agent_code"]]=array("comm"=>$line["comm"],"sales"=>$line["sales"]);
			//$ret[]=array()
		}
		return $ret;
	}
	
	function get_total_sales_and_comm_per_agent($from,$to,$agent_id) {
		
		$sql="select sum(productsales.saleamount) as sales, 
			sum(productsales.saleamount * product.comm/100) as comm,						
			agent.agentcode as agent_code,
			agent.name as name,
			agent.surname as surname			
			from productsales, product, agent, territory
 			where datesold >= '$from 00:00:00' and datesold <= '$to 23:59:59' 			 
			and productsales.product_id = product.id
			and productsales.Territory_Id = territory.Id 
			and territory.agent_id = agent.agentcode
			group by agent.agentcode";
		//print $sql;
		
		if(is_numeric($agent_id)) $tmp = "and agent.agentcode = ".$agent_id;
		else $tmp = "";
		$sql = "select agent.name as name,
			agent.surname as surname,
			territory.name as customer,
			product.name as product,
			sum(productsales.saleamount) as sales,
			sum((productsales.saleamount * product.comm/100)) as comm
			from productsales, product, agent, territory
			where 
			productsales.product_id = product.id and
			productsales.territory_id = territory.id and
			territory.agent_id = agent.agentcode and 
			productsales.product_id = product.id and
			datesold >= '$from 00:00:00' and datesold <= '$to 23:59:59' 
			$tmp	
			group by customer,product
			order by name,surname,customer";
		
		//time for some joins and to fix this bad sql
		$sql = "select agent.name as name,
				agent.surname as surname,
				territory.name as customer,
				product.name as product,
				sum(productsales.saleamount) as sales,
				sum((productsales.saleamount * product.comm/100)) as comm
				from productsales left join product on product_id = product.id
				left join territory on territory_id = territory.id
  				left join agent on territory.agent_id = agent.agentcode				
				where datesold >= '$from 00:00:00' and datesold <= '$to 23:59:59'
				$tmp
				group by customer,product
				order by name,surname,customer";
		$query = $this->db->query(strtolower($sql));
		$rs=$query->result_array();
		return $rs;
		$ret=array();
		foreach($rs as $line) {			
			/*$ret=array_merge
			($ret,array
				($line["product_group_id"]=>array
					($line["agent_code"]=>array
						("comm"=>$line["comm"],"sales"=>$line["sales"])
					)
				)
			);	
			*/
			$ret[$line["product_group_id"]][$line["agent_code"]]=array("comm"=>$line["comm"],"sales"=>$line["sales"]);
		}
		return $ret;
	}

	function get_agent_comm_per_product_group($from,$to) {				
		
		
		$sql="select sum(productsales.saleamount) as sales, 
			sum(productsales.saleamount * product.comm/100) as comm,
			productgroup.id as product_group_id,			
			agent.agentcode as agent_code
			from productsales, productgroup, product, agent, territory
 			where datesold >= '$from 00:00:00' and datesold <= '$to 23:59:59' 
			and product.ProductGroupId = productgroup.Id 
			and productsales.product_id = product.id
			and productsales.Territory_Id = territory.Id 
			and territory.agent_id = agent.agentcode
			group by agent.agentcode,productgroup.id";
		
		$sql = "select sum(productsales.saleamount) as sales, 
       sum(productsales.saleamount * product.comm/100) as comm,
       productgroup.id as product_group_id,			
       agent.agentcode as agent_code
       from productsales left join product on product_id = product.id
       left join productgroup on product.ProductGroupId = productgroup.Id
       left join territory on territory_id = territory.id
       left join agent on territory.agent_id = agent.agentcode	
 			where datesold >= '$from 00:00:00' and datesold <= '$to 23:59:59' 
			and product.ProductGroupId = productgroup.Id 
			and productsales.product_id = product.id
			and productsales.Territory_Id = territory.Id 
			and territory.agent_id = agent.agentcode
			group by agent.agentcode,productgroup.id";
		$query = $this->db->query(strtolower($sql));
		$rs=$query->result_array();
		$ret=array();
		foreach($rs as $line) {			
			/*$ret=array_merge
			($ret,array
				($line["product_group_id"]=>array
					($line["agent_code"]=>array
						("comm"=>$line["comm"],"sales"=>$line["sales"])
					)
				)
			);	
			*/
			$ret[$line["product_group_id"]][$line["agent_code"]]=array("comm"=>$line["comm"],"sales"=>$line["sales"]);
		}
		return $ret;
	}
	
	
	function get_sales_vs_site_visits_per_agent_per_month($from,$to,$agent_id) {
		//This is a complicated one 
		//We create the first view of sales data
		$sql="create or replace view v1 as
			select t.id as s_t_id, t.name as s_territory_name, sum(p.saleamount) as s_amount, year(datesold) as s_year, month(datesold) as s_month
			from territory as t 
			left join productsales as p on (t.id = p.territory_id)
			where t.agent_id = ".$this->db->escape($agent_id)."
			and p.datesold >= '$from 00:00:00' and p.datesold <= '$to 23:59:59'
			group by t.id, s_year, s_month";
			//order by s_territory_name, s_year, s_month";
		$query = $this->db->query($sql);
		
		//We create the second view of site visits
		$sql="create or replace view v2 as
			select t.id as v_t_id, t.name as v_territory_name, count(s.id) as v_num, year(datevisited) as v_year, month(datevisited) as v_month
			from territory as t, sitevisits s
			where t.agent_id = ".$this->db->escape($agent_id)."
			and t.id = s.territory_id
			and s.datevisited >= '$from 00:00:00' and s.datevisited <= '$to 23:59:59'
			group by t.id, v_year, v_month";
			//order by v_territory_name, v_year, v_month";
		$query = $this->db->query($sql);
		
		//Now we join the two views together
		$sql="(select ifnull(s_t_id,v_t_id) as terr_id, ifnull(v_territory_name,s_territory_name) as terr_name,
				ifnull(s_year,v_year) as the_year,
				ifnull(s_month,v_month) as the_month,
				s_amount,v_num from v1
				left join v2 on (v1.s_t_id = v2.v_t_id and v1.s_year = v2.v_year and v1.s_month = v2.v_month))
			union 
				(select ifnull(s_t_id,v_t_id) as terr_id, ifnull(v_territory_name,s_territory_name) as terr_name,
				ifnull(s_year,v_year) as the_year,
				ifnull(s_month,v_month) as the_month,
				s_amount,v_num from v1
				right join v2 on (v1.s_t_id = v2.v_t_id and v1.s_year = v2.v_year and v1.s_month = v2.v_month))
			order by terr_name,the_year,the_month";
		$query = $this->db->query($sql);
		$rs=$query->result_array();
		$ret=array();
		foreach($rs as $line) {
			if(trim($line["s_amount"])=="") 
				$ret[$line["terr_name"]][$line["the_year"]][$line["the_month"]]["sale_amount"]="0";			
			else
				$ret[$line["terr_name"]][$line["the_year"]][$line["the_month"]]["sale_amount"]=$line["s_amount"];
			if(trim($line["v_num"])=="") 
				$ret[$line["terr_name"]][$line["the_year"]][$line["the_month"]]["visit_num"]="0";
			else
				$ret[$line["terr_name"]][$line["the_year"]][$line["the_month"]]["visit_num"]=$line["v_num"];
		}		
		return $ret;
	}
}