<?php

class Sales_model extends CI_Model {

	
	public function __construct()   {
          $this->load->database(); 
   }
	
	function get_total_sales_for_month($month="ALL") {
		if($month=="ALL") {
			
			$sql="select importcode, sum(saleamount) as month_total from productsales group by importcode";		
			$query = $this->db->query($sql);		
    		return $query->result_array();
		}
	}
	
	function get_product_sales_by_month($month="ALL") {
	if($month=="ALL") {
		
			$sql="select sum(saleamount) as salevalue,productgroup.name,importcode from productsales, productgroup, product where 
				productsales.product_id = product.id and
				product.productgroupid = productgroup.id and
				productgroup.comm > 0 
				group by importcode, productgroup.name
				order by importcode";		
			$query = $this->db->query($sql);		
    		return $query->result_array();
		}
	}
	
	function get_sales_per_month_per_territory_by_agent($agent_id) {
		$sql="";
		$query = $this->db->query($sql);		
    	return $query->result_array();
	}
	
	function insert_sales_data($data,$date_sold,$import_code) {
		
		if(count($data>0)) {	
		$sql="insert into productsales(territory_id,product_id,quantity,saleamount,importcode,datesold) "
		." values ";
		$tmp="";
		foreach($data as $i) { 
			$tmp.="\n(".$i["territory_id"].",".$i["product_id"].",".$i["item_count"].",".$i["sales_total"].",'".$import_code."','".$date_sold."'),";			
		}
		$tmp=substr($tmp,0,-1);
		//print $sql.$tmp;
		$query = $this->db->query($sql.$tmp);	
		return $this->db->affected_rows();	    	
		}
		return 0;
	}
}