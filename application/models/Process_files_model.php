<?php

class Process_files_model extends CI_Model {
	
	public function __construct()   {
          $this->load->database(); 
	}
	
	function get_all_headings() {
		$sql = "select rh_name,rh_terr_name,rh_product_name,rh_qty,rh_cost from report_headings";	
		$query = $this->db->query($sql);		
    	return $query->result_array();		
	}
	
	function insert_load_data($terr_name, $product_name, $product_qty, $sale_amount, $year, $month, $import_id, $file_name) {
		$sql = "insert into load_data(ld_terr_name, ld_product_name, ld_product_qty, ld_sale_amount, ld_year, ld_month, ld_import_id, ld_file_name) 
		   values (?, ?, ?, ?, ?, ?, ?, ?)";
		$this->db->query($sql, array($terr_name, $product_name, $product_qty, $sale_amount, $year, $month, $import_id, $file_name));
		if($this->db->affected_rows() == 1) return true;
		return false;
	}
	
	function get_load_data($import_id) {
		$sql = "select ld_id, ld_terr_name, ld_product_name, ld_product_qty, ld_sale_amount, ld_year, ld_month, ld_import_id, ld_file_name from load_data
		where ld_import_id = ?";
		$query = $this->db->query($sql, array($import_id));
		return $query->result_array();
	}
	
	function insert_sale_record($date_sold, $qty, $sale_amount, $t_id, $p_id, $import_code) {
		$sql = "insert into productsales(DateSold, Quantity, SaleAmount, Territory_Id, Product_Id, ImportCode)
			values (?, ?, ?, ?, ?, ?)";
		$this->db->query($sql, array($date_sold, $qty, $sale_amount, $t_id, $p_id, $import_code));
		if($this->db->affected_rows() == 1) return true;
		return false;		
	}
	
	function clear_data_get_data($year, $month) {
		
		$ret 	= 0;
		$date 	= $year."-".$month."-01 00:00:00";
		$sql 	= "select count(*) as cnt from productsales where datesold = ?";
		$query 	= $this->db->query($sql, array($date));
		$rs 	= $query->result_array();
			
		if(count($rs) > 0) $ret = $rs[0]["cnt"];
		
		return $ret;			
	}
	
	function clear_data($year, $month) {
		
		$sql 	= "delete from productsales where datesold = ?";
		$date 	= $year."-".$month."-01 00:00:00";
		
		$this->db->query($sql, array($date));
		
		return $this->db->affected_rows();
	}
}

?>