<?php

class Principal_model extends CI_Model {
	
	public function __construct()   {
          $this->load->database(); 
   }

	function get_all_principals($start=0,$max=10000) {
		//$this->load->database();				
		$sql="select
				Id,Name,ContactName,ContactTelNo,ContactFaxNo,ContactEmail
				from principal order by Name
				limit $start,$max";		
		$query = $this->db->query($sql);		
    	return $query->result_array();
	}
	
	function delete_principal($id) {		
		//$this->load->database();
		//We set on delete to IHS
		$sql="update productgroup set principalId = '11' where principalId = ".$this->db->escape($id);
		$this->db->query($sql);		
		$sql="delete from principal where Id = ".$this->db->escape($id);
		$this->db->query($sql);
		if($this->db->affected_rows()=="1") return true;
		else return false;
	}
	
	function add_principal($name,$contact_name,$contact_tel,$contact_fax,$contact_email) {
		
		$sql="insert into principal(Name,ContactName,ContactTelNo,ContactFaxNo,ContactEmail) values ("
		.$this->db->escape($name).","
		.$this->db->escape($contact_name).","
		.$this->db->escape($contact_tel).","
		.$this->db->escape($contact_fax).","
		.$this->db->escape($contact_email).")";
		$this->db->query($sql);
		if($this->db->affected_rows()=="1") return true;
		else return false;
	}
	
	function save_principal($id,$name,$contact_name,$contact_tel,$contact_fax,$contact_email) {
		$sql="update principal set
		name = ".$this->db->escape($name).",
		ContactName = ".$this->db->escape($contact_name).",
		ContactTelNo = ".$this->db->escape($contact_tel).",
		ContactFaxNo = ".$this->db->escape($contact_fax).",
		ContactEmail = ".$this->db->escape($contact_email)."
		where id = ".$this->db->escape($id);
		
		$this->db->query($sql);
		if($this->db->affected_rows()=="1") return true;
		else return false;
	}
		
}
	
?>