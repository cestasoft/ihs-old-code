<?php

class Motd_model extends CI_Model {

	/*function Motd_model() {
		parent::Model();
		$this->load->database();
	}*/
	
	public function __construct()   {
          $this->load->database(); 
   }
	
	function get_message() {
		$ret	= "";		
		$sql 	= "select saying from sayings order by rand() limit 1";		
		$query 	= $this->db->query($sql);
		$rs 	= $query->result_array();	
		if(count($rs)>0) {
			if(isset($rs[0]["saying"])) $ret = $rs[0]["saying"];
		}
    	return $ret;
	}
	
}