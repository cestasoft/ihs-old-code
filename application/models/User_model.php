<?php

class User_model extends CI_Model {

	/*
	function User_model() {
		parent::Model();
		$this->load->database();
	}*/
	
	public function __construct()   {
          $this->load->database(); 
   }
	
	/*function _hash_password($pwd) {
		$iterations = 1000;	
		$salt = "VERY_SALTY2019!";
		$hash = hash_pbkdf2("sha256", $pwd, $salt, $iterations, 60);
		return $hash;	
	}*/
	
	function _hash_password($pwd) {
			$salt="This is the Salt";		
    		$this->load->helper('security');
    		$the_hash=sha1($salt.$pwd);
    		return $the_hash;
	}
	
	function check_password($username, $password) {
		//we return false on fail or id on success		
		$username=trim($username);//remove white space from user
		
		$sql="select id,password from users where usercode = ".$this->db->escape($username)." and active = '1'";
		$query = $this->db->query($sql);		
		$rs=$query->result_array();
		
		if(count($rs)==1) {
			$the_hash=$this->_hash_password($password);	
			log_message('error', 'We have ui password: ' . $password);			
    		log_message('error', 'We have ui hash: ' . $the_hash);
			log_message('error', 'We have db hash: ' . $rs[0]["password"]);
    		if($rs[0]["password"]==$the_hash) return $rs[0]["id"];    		    		
    	}
    	return false;			
	}
	
	function get_user_details($id="") {							
		if($id=="") 
			$sql="select id,title,name,surname,usercode,active,dateadded from users order by id";
		else 
			$sql="select id,title,name,surname,usercode,active,dateadded from users where id = ".$this->db->escape($id);
			
		$query = $this->db->query($sql);		
    	$rs=$query->result_array();

    	if(count($rs)==1) {
    		return $rs[0];
	   	}
    	return $rs;
	}
	
	function get_agent_name($agent_id) {
		$agent=$this->get_all_agents($agent_id);
			if(count($agent)==1)
		return $agent[0]["name"]." ".$agent[0]["surname"];
		else 
			return "Agent not found.";		
	}
	
	function get_all_users($users_id=FALSE) {
		
		$sql="select AgentCode,
				Title,
				Name,
				Surname,
				TelNoH,
				TelNoW,
				FaxNo,
				CellNo,
				PhysicalAddress1,
				PhysicalAddress2,
				PhysicalAddress3,
				PhysicalAddress4,
				PhysicalCode,
				PostalAddress1,
				PostalAddress2,
				PostalAddress3,
				PostalAddress4,
				PostalCode
 			from agent";		
		if($agent_id!==FALSE) $sql.=" where agentcode = $agent_id";
		$query = $this->db->query(strtolower($sql));		
    	return $query->result_array();
	}
	
	function delete_user($user_id) {
		$sql="delete from users where id = ".$this->db->escape($user_id);
		$this->db->query($sql);
		if($this->db->affected_rows()=="1") return true;
		else return false;
	}
	
	function create_user($user) {
		//set the password
		$pwd=$this->_hash_password($user["password"]);	
		
		
		$sql="insert into users (password,usercode,title,name,surname,active) values 
				('$pwd', 
				".$this->db->escape($user["usercode"]).",
				".$this->db->escape($user["title"]).",
				".$this->db->escape($user["name"]).",
				".$this->db->escape($user["surname"]).",
				".$this->db->escape($user["active"]).")";					
		$this->db->query($sql);
		if($this->db->affected_rows()=="1") return true;
		else return false;
	}
	
	function update_user($user) {
		//check if it is the admin user
		if($user["id"]=="19") return true;
		//check is the password is set	
		if(isset($user["password"])&&$user["password"]!="") {
			$pwd=$this->_hash_password($user["password"]);
			$sql="update users set
				password = '".$pwd."', 
				usercode = ".$this->db->escape($user["usercode"]).",
				title = ".$this->db->escape($user["title"]).",
				name = ".$this->db->escape($user["name"]).",
				surname = ".$this->db->escape($user["surname"]).",
				active = ".$this->db->escape($user["active"]).",
				usercode = ".$this->db->escape($user["usercode"])."		
				where id = ".$this->db->escape($user["id"]);		
		}
		else { 
			$sql="update users set 
				usercode = ".$this->db->escape($user["usercode"]).",
				title = ".$this->db->escape($user["title"]).",
				name = ".$this->db->escape($user["name"]).",
				surname = ".$this->db->escape($user["surname"]).",
				active = ".$this->db->escape($user["active"]).",
				usercode = ".$this->db->escape($user["usercode"])."		
				where id = ".$this->db->escape($user["id"]);
		}
		$this->db->query($sql);
		//print $sql;
		if($this->db->affected_rows()=="1") return true;
		else return false;
	}
}

?>
