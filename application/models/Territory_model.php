<?php

define("DB_MAX_RESULTS",200000);

class Territory_model extends CI_Model {
	
	public function __construct()   {
          $this->load->database(); 
   }
	
	function get_all_territories($start=0,$max=DB_MAX_RESULTS,$agent_id="") {
		
		
		if($agent_id!=""&&$agent_id!="ALL") 
			$sql="select telno, PhysicalAddress4, id,name,agent_id,rating from territory where isActive = '1' and agent_id = ".$this->db->escape($agent_id)." order by PhysicalAddress4, name limit $start,$max";
		else
			$sql="select telno, PhysicalAddress4, id,name,agent_id,rating from territory where isActive = '1' order by PhysicalAddress4, name limit $start,$max"; 
		$query = $this->db->query($sql);		
    	return $query->result_array();
	}
	
	function get_territories_for_agent_via_filter($filter,$agent_id) {
		return $this->get_territories_via_filter($filter,"agent",DB_MAX_RESULTS,$agent_id);
	}
	
	function get_territories_via_filter($filter,$type="",$max=DB_MAX_RESULTS,$agent_id="") {
		if(strlen($filter) < 3) return array();
		$filter = '%'.$filter.'%';
		$sql = "select telno, PhysicalAddress4, id,name,agent_id,rating from territory where isActive = '1' and name like ".$this->db->escape($filter)." order by name limit $max";
		if($type == "unalloc") 
			$sql = "select telno, PhysicalAddress4, id,name,agent_id,rating from territory where isActive = '1' and agent_id = 19 name like ".$this->db->escape($filter)." order by name limit $max";
		else if($type == "agent")
			$sql = "select telno, PhysicalAddress4, id,name,agent_id,rating from territory where isActive = '1' and agent_id = ".$this->db->escape($agent_id)." and name like ".$this->db->escape($filter)." order by name limit $max";
		$query = $this->db->query($sql);		
    	return $query->result_array();
	}
	
	function get_terr($terr_id) {
		$sql = "select telno, id, name, agent_id, rating, PhysicalAddress1, PhysicalAddress2, PhysicalAddress3, PhysicalAddress4, PhysicalCode, Province 
				from territory where id = ?";
		$query = $this->db->query($sql, array($terr_id));
		return $query->result_array();
	}
	
	function update_territory($data) {
		$ret = false;
		$sql = "update territory set name = ?,
					rating = ?,
					PhysicalAddress1 = ?,
					PhysicalAddress2 = ?,
					PhysicalAddress3 = ?,
					PhysicalAddress4 = ?,
					PhysicalCode = ?,
					agent_id = ?,
					Province = ?,
					TelNo = ?
				where id = ?";	
		$query = $this->db->query($sql,$data);
		if($this->db->affected_rows() == 1) $ret = true;
		return $ret;
	}
	
	function add_terr($name,
				$agent_id,
				$rating,
				$PhysicalAddress1,
				$PhysicalAddress2,
				$PhysicalAddress3,
				$PhysicalAddress4,
				$PhysicalCode,
				$Province,
				$TelNo,
				$AgentCreated) {
		$ret = false;
		$sql = "insert into territory( name, agent_id, rating, PhysicalAddress1, PhysicalAddress2, PhysicalAddress3, PhysicalAddress4, PhysicalCode, Province, TelNo, AgentCreated )
				values (
				".$this->db->escape($name).",
				".$this->db->escape($agent_id).",
				".$this->db->escape($rating).",
				".$this->db->escape($PhysicalAddress1).",
				".$this->db->escape($PhysicalAddress2).",
				".$this->db->escape($PhysicalAddress3).",
				".$this->db->escape($PhysicalAddress4).",
				".$this->db->escape($PhysicalCode).",
				".$this->db->escape($Province).",
				".$this->db->escape($TelNo).",
				".$this->db->escape($AgentCreated)."				
				)";					
		$query = $this->db->query($sql);
		if($this->db->affected_rows()==1) $ret = true;
		return $ret;		
	}
	
	function add_territory($name) {
		//19 is unalloc , default rating is C
		$sql="insert into territory(name,agent_id,rating) values 
			(".$this->db->escape($name).",'19','C')";
		$query = $this->db->query($sql);				
    	//$rs=$query->result_array();
    	if($this->db->affected_rows()==1) {    		
    		$sql="select @@identity as theid";
    		$query = $this->db->query($sql);
    		$rs=$query->result_array();
    		if(count($rs)==1) return $rs[0]["theid"];
    	}
    	else return false;
		
	}
	
	function get_territory_id_by_name($name) {
		$sql="select id from territory where name = ".$this->db->escape($name);		
		$query = $this->db->query($sql);		
    	$rs=$query->result_array();
    	//we have buggy data so we do the 1st one
    	if(count($rs)>=1) return $rs[0]["id"];
    	else {
    		$sql="select territory_id as id from territory_child, territory where territory.id = territory.isActive = '1' and child_name = ".$this->db->escape($name);
    		$sql = "select territory.id as id 
				from territory_child, territory 
				where territory.id = territory_child.territory_id
				and territory.isActive = '1' 
				and territory_child.child_name = ?";			
    		$query = $this->db->query($sql,array($name));		
    		$rs = $query->result_array();
    		if(count($rs)>=1) return $rs[0]["id"];
    	}
    	return false;
	}
	
	function get_total_num_territories($agent_id="") {
		
		if($agent_id!="")
			$sql="select count(*) as total from territory where isActive = '1' and agent_id = ".$this->db->escape($agent_id);
		else			
			$sql="select count(*) as total from territory";
		$query = $this->db->query($sql);		
    	return $query->result_array();
	}
	
	function set_territory_agent($terr_id,$agent_id) {
		
		$sql="update territory set agent_id = ".$this->db->escape($agent_id)." where id = ".$this->db->escape($terr_id);
		$query = $this->db->query($sql);		
    	return $this->db->affected_rows();
	}
	
	function set_territory_rating($terr_id,$rating) {
		
		$sql="update territory set rating = ".$this->db->escape($rating)." where id = ".$this->db->escape($terr_id);
		$this->db->query($sql);		
    	return $this->db->affected_rows();
	}
	
	function link_territory($parent,$child,$childName) {
		//site visits
		log_message('info',"We link with the following values: 
					Parent		: $parent
					Child		: $child
					Child Name	: $childName");
		$this->load->database();
		$sql="update sitevisits set territory_id = ".$parent." where territory_id = ".$child;
		$this->db->query($sql);		
    	log_message('info',"link territor". $sql." - affected rows: ".$this->db->affected_rows());
		//sales
		$sql="update productsales set territory_id = ".$parent." where territory_id = ".$child;
		$this->db->query($sql);		
    	log_message('info',"link territor". $sql." - affected rows: ".$this->db->affected_rows());
    	//add link
		$sql="insert into territory_child(territory_id,child_name) values ('$parent','$childName')";
		$this->db->query($sql);		
    	log_message('info',"link territor". $sql." - affected rows: ".$this->db->affected_rows());    	
    	//delete the terr
    	$sql="delete from territory where id = ".$child;
		$this->db->query($sql);		
    	log_message('info',"link territor". $sql." - affected rows: ".$this->db->affected_rows());
		return TRUE;
	}
	
	function get_all_territories_for_agent($agent_id) {
		
		$sql="select telno, PhysicalAddress4, name,id from territory where isActive = '1' and agent_id =".$this->db->escape($agent_id)." order by name";
		$query = $this->db->query($sql);		
    	return $query->result_array();
	}
	
	function hide_territory($terr_id,$un=false) {
		if($un) $status = 1;
		else 	$status = 0;
		$sql = "update territory set isActive = '".$status."' where id = ".$this->db->escape($terr_id);
		$this->db->query($sql);		
    	if($this->db->affected_rows() == 1) return true;;
    	return false;
	}
			
}

?>