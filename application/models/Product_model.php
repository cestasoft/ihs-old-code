<?php

class Product_model extends CI_Model {

	public function __construct()   {
          $this->load->database(); 
	}

	function get_all_products($start=0,$max=10000000) {
						
		$sql="select
				Id,Name,ProductGroupId,Comm
				from product order by Name
				limit $start,$max";		
		$query = $this->db->query($sql);		
    	return $query->result_array();
	}
	
	function delete_product($id) {
		
		$sql="delete from productsales where product_id =".$this->db->escape($id);
		$this->db->query($sql);		
		$sql="delete from product where Id = ".$this->db->escape($id);
		$this->db->query($sql);		
		if($this->db->affected_rows()=="1") return true;
		else return false;	
		
	}
	
	/*
	function get_all_product_groups($start=0,$max=10000) {
		$this->load->database();				
		$sql="select
				Id,Name,PrincipalId
				from productgroup order by Name
				limit $start,$max";		
		$query = $this->db->query($sql);		
    	return $query->result_array();
	}
	*/
	
	function get_product_id_by_name($name) {
		//We use dataname for product incase they want 'print' friendly names for reports
		$sql="select id from product where dataname = ".$this->db->escape($name);		
		$query = $this->db->query($sql);		
    	$rs=$query->result_array();
    	if(count($rs)>=1) return $rs[0]["id"];
    	return false;
	}
	
	function add_product($name) {
		//19 is unalloc , default rating is C
		//$sql="insert into product(name,agent_id,rating) values
		$sql="insert into product(name,comm,productgroupid,dataname) values 
		(".$this->db->escape($name).",0,76,".$this->db->escape($name).")"; 			
		$query = $this->db->query($sql);		
    	//$rs=$query->result_array();
    	if($this->db->affected_rows()==1) {
    		$sql="select @@identity as theid";
    		$query = $this->db->query($sql);
    		$rs=$query->result_array();
    		if(count($rs)==1) return $rs[0]["theid"];
    	}
    	else return false;
		
	}
	
	function save_product($id,$product_group,$comm) {
		$sql="update product set productgroupid = ".$this->db->escape($product_group)." , comm = ".$this->db->escape($comm)." where id = ".$this->db->escape($id);
		$this->db->query($sql);		
		if($this->db->affected_rows()=="1") return true;
		else return false;
	}
	
}
	
?>
