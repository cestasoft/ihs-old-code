<?php

class Reportheading_model extends CI_Model {
	
	public function __construct()   {
          $this->load->database(); 
   }

	function get_all_headings() {
		
		$sql="select * from report_headings";		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function update_headings($post_data) {				
		$sql="update report_headings set
			rh_province = ?,  
			rh_name = ?, 
			rh_terr_name = ?, 
			rh_product_name = ?,
 			rh_qty = ?,
 			rh_cost = ?, 
 			rh_invoice_no = ? 
 			where rh_id = ?";
		//print_r($post_data);
		//return false;
		$this->db->query($sql, array(
			$post_data["rh_province"],
			$post_data["rh_name"],
			$post_data["rh_terr_name"],
			$post_data["rh_product_name"],
			$post_data["rh_qty"],
			$post_data["rh_cost"],
			$post_data["rh_invoice_no"],
			$post_data["rh_id"]		
		));
		if($this->db->affected_rows()=="1") return true;
		else return false;
	}
	
	function delete_headings($id) {
		$sql="delete from report_headings where rh_id = ".$this->db->escape($id);
		$this->db->query($sql);
		if($this->db->affected_rows()=="1") return true;
		else return false;
	}
	
	function add_headings($post_data) {				
		$sql="insert into report_headings 
			(rh_province,  
			rh_name, 
			rh_terr_name, 
			rh_product_name,
 			rh_qty,
 			rh_cost, 
 			rh_invoice_no) 
 			values (?, ?, ?, ?, ?, ?, ?)";		
		
		$this->db->query($sql, array(
			$post_data["rh_province"],
			$post_data["rh_name"],
			$post_data["rh_terr_name"],
			$post_data["rh_product_name"],
			$post_data["rh_qty"],
			$post_data["rh_cost"],
			$post_data["rh_invoice_no"]			
		));
		return $this->db->insert_id(); 		
	}
	
}