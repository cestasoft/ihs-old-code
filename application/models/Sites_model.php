<?php

class Sites_model extends CI_Model {
	
	public function __construct()   {
          $this->load->database(); 
   }
	
	function get_site_list($year,$month,$agent_id) {		
		$sql="select territory.id as tid, sitevisits.id as sid, territory.name, SUBSTRING(sitevisits.datevisited,1,10) as datevisited from territory left OUTER JOIN sitevisits on territory.Id = sitevisits.Territory_Id
				where territory.Agent_Id = ".$agent_id." 
				and (DateVisited like '".$year."-".$month."-%' or DateVisited is NULL) ORDER BY `Name`,datevisited ASC";
		//print $sql;
		//These SQL queries are getting more and more complex
		//I need to do this because it was not included territories where there where site visits and we select a different month
		$sql = "select distinct rating, tid, name, telno, physicaladdress4, physicaladdress3, sid, datevisited from (
			select rating, telno, physicaladdress4, physicaladdress3, territory.id as tid, territory.name, NULL as sid, NULL as datevisited
			from territory
			where territory.Agent_Id  = ".$agent_id."
			and isactive != 0
			and rating != 'D'
			union
			select rating, telno, physicaladdress4, physicaladdress3, territory.id as tid, territory.name, sitevisits.id as sid,  SUBSTRING(sitevisits.datevisited,1,10) as datevisited 
			from territory  right OUTER JOIN sitevisits on territory.Id = sitevisits.Territory_Id
			where territory.Agent_Id = ".$agent_id."
			and territory.isactive != 0
			and territory.rating != 'D'			
			and (DateVisited like '".$year."-".$month."-%' or DateVisited is NULL) 
			) as T 
			ORDER BY name, datevisited asc";

		//Request to order by name so we remove this
		//			ORDER BY physicaladdress4, name, datevisited asc";
		//print $sql;
		$query = $this->db->query($sql);
		$ret = $query->result_array();
		$this->db->close();
		return $ret;
	}

	function get_all_sites($start=0,$max=100) {		
		$sql="select id,datevisited,comment,agent_id,territory_id from sitevisits 			
			order by datevisited limit $start,$max";		
		$query = $this->db->query($sql);
		$ret = $query->result_array();
		$this->db->close();
		return $ret;
	}
	
	function get_site_visits_by_agent($from,$to) {

		$sql="select a.name, a.surname, count(*) as cnt from sitevisits as s, territory as t, agent as a
			where s.territory_id = t.id
			and t.agent_id = a.agentcode	
			and isactive != 0		
			and s.datevisited >= '$from 00:00:00' and s.datevisited <= '$to 23:59:59' 
			group by a.agentcode
			order by name, surname";
		$query = $this->db->query($sql);
		$ret = $query->result_array();
		$this->db->close();
		return $ret;
	}
	
	function save_visit($agent_id,$tid,$date) {
		$sql = "insert into sitevisits (datevisited,territory_id,agent_id) values ('".$date."','".$tid."','".$agent_id."')";
		$query = $this->db->query($sql);		
		if($this->db->affected_rows() == 1) {
			$ret = $this->db->insert_id();
			$this->db->close();		
			return $ret;  
		}
		$this->db->close();
		return "0"; 		
	}
	
	function remove_visit($sid) {
		$sql = "delete from sitevisits where id = ".$this->db->escape_str($sid);
		$query = $this->db->query($sql);		
		if($this->db->affected_rows() == 1) {
			$this->db->close();
			return "1";  
		}
		$this->db->close();
		return "0"; 		
	}
	
	function save_visits($visits) {
		return;
		//Not used
		if(count($visits)>0) {
			$values="";
			foreach($visits as $visit) {
				$tmp=explode(":",$visit);
				$values.="(".$this->db->escape($tmp[1]).",".$this->db->escape($tmp[0])."),";		
			}
			$values=substr($values,0,-1); //we remove the last ,
			$sql="insert into sitevisits(DateVisited,Territory_Id) values ".$values;
			//print $sql;
			//return;
			$query = $this->db->query($sql);		
    		if($this->db->affected_rows()==count($visits)) return true;
		}		
		return false;
	}
}
	
?>
