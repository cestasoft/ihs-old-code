<?php

class Process_Files extends CI_Controller {

	var $base_dir;
	var $row_places;

	var $chars;
	
	var $total_add_territories;
	var $total_add_products;
	var $total_items;
	var $total_sales_amount;
	var $total_lines;
	
	var $sales_lines;
	var $import_code;
	var $date_sold;
	
	function __construct() {
		parent::__construct();
		
		$this->total_add_territories=0;
		$this->total_add_products=0;
		$this->total_items=0;
		$this->total_sales_amount=0;
		$this->total_lines=0;
		$this->sales_lines=array();
		$this->import_code="testing";
		$this->date_sold="2010-10-10 10:10:10";
		
		$this->load->helper(array('form', 'url'));
// Hetzner fix		$this->base_dir = "/var/www/system/uploads";
		//$this->base_dir = "/usr/www/users/ihssms/uploads";
		$this->base_dir = "/usr/wwws/users/ihssms/";
		$this->row_places=array();
		
		$this->load->model('Territory_model','terr');
		$this->load->model('Product_model','product');
		$this->load->model('Sales_model','sales');
		$this->load->model('Process_files_model','process');
		
		$this->chars=array("PHARMACY"
		,"APTEEKMC"
		,"PHARMACYAPTEEK"
		,"APTEEKNEW"
		,"PHARMACYMC"
		,"PHARMACYNEW"
		,"PHARMACYNEWACC"
		,"PHARMACYMEDICINEDEPOT"
		,"NEWACC"
		,"PHARMACYPMAMC"
		,"PHARMACYNEWACCMC"
		,"PHARMACYPTYLTD"
		,"PHCYMEDICINEDEPOTMC"
		,"apteek"
		,"phy"
		,"otc"
		,"apt"
		,"mc"
		,"new"
		,"pty"
		,"pma"
		,"usap"
		,"scr"
		,"ltd"
		,"pharmac"
		,"ph"
		,"old"
		,"pharmacies"
		,"sl"
		,"sn"
		,"cc"
		,"ptyltd"
		,"cod"
		,"newacc"
		,"phyapteek"
		,"pharmacymc"
		,"pharmacynew"
		,"pharm"
		,"edms"
		,"PHCYMC"
		,"PHYMC"
		,"PHARMACYMC"
		,"PHCY"
		,"APTEEKMC"
		,"PHARMACYNEWACCMC"
		,"PHARMACYPTYLTD"
		,"PHCYPMAMC"
		,"CHEMIST"
		,"PMAMC"
		,"NEWOWNERCOD"
		,"PHCYNEWACC"
		,"NOTDISPENSING"
		,"APTEEKSAPTYLTD"
		,"DISPENSARYPTYLTD"
		,"PHARMACYSBHYATSCCSCR"
		,"PHARMACYRPMMC"
		,"APTEEKSCRIPTNET"
		,"PHARMACYINCORPORATED"
		,"PHARMACYCASHONLY"
		,"PHARMACIE"
		,"PHARMACYNEWOWNERSMC"
		,"CHEMICALCORPORATION"
		,"APTEEKRPM"
		,"PHARMACYSCRIPTNE"
		,"PHARMACYAPTEEK"
		,"APTEEKPHARMACY"
		,"DISPAFROX"
		,"DISPLIFEPHYMNGSERV"
		,"PHARMACYMEDICYNDEPOT"
		,"PHCYMEDICINEDEPOT"
		,"PHARMACYPMA"
		,"PHARMACYMCPMC"
		,"PHARMACYSCRIPTNET"
		,"SUPPLIES"
		,"SUPPLIESCC"
		,"PHYLIFEPHYMNGSERVPTY"
		,"DEPOTKBY"
		,"MEDDEP"
		,"PHYNEWMC"
		,"FACILITY"
		,"CASHWITHORDER"
		,"HEALTHSHOPCOD"
		,"INGELYF"
		,"MEDICINEDISPENSARY"
		,"PHYMIAKHARWARAYSC"
		,"NEWACCOUNT"
		,"WHOLESALERS"
		,"WHOLESALERSCC"
		,"PHYSCRIPTNE"
		,"OTC"
		,"PHYACACARIMCCSCRIP"
		,"PHARMACYMEDICINEDEPOTKBY"
		,"PHARMACYNEWOWNERSMC"
		,"DISPLIFEPHYMNGSERVPTYLTD"
		,"PHARMACYCODONLYCOLLECT"
		,"PHYMEDICALDEPOTPRTMC"
		,"PHARMNEWOWNERMC"
		,"MEDICINEDEPOT"
		,"PHARMACYDCOLI"
		,"PHARMACYDKDAHYACC"
		,"PHYMNGSERVPT"
		,"APTEEKNOSCRIPTN"
		,"SCRIP"
		,"APTEEKEDMSBPKMC"
		,"PHARMACEUTICALS"
		,"PHARMACIESPTYLTD"
		,"DISPENSINGAL"
		,"PHARMACYLINK"
		,"APTEEKUSAP"
		,"PHARMAC"
		,"PHYSCRIP"
		,"MEDCNT"
		,"PHYSC"
		,"PHYSCRI"
		,"PH"
		,"DIST"
		,"DISTRIBUTORS"
		,"PHARMACYCASHUPFRONT"
		,"INGELYFDISPENSING"
		,"PHYENKOSICASHW"
		,"PHYMNGSERVPTY"
		,"PHYTASCIOCATTIOLSENMC"
		,"APTEEKMEDISYNEDEPOTMC"
		,"PATENTSONLY"
		,"MNGSERVPT"
		,"PHARMACYMEDICINEDEPOTNO"
		,"NOTDISPENSING"
		,"DRUGSTORE"
		,"PHARMACYCLOSED"
		,"COLLECTCHEQUE"
		,"NEW"
		,"PHYNEW"
		,"ASSCC"
		,"PHYMEDDEPMC"
		,"PTYLT"
		,"PHARMACYRPM"
		,"PARTNERSDRSINC"
		,"PHYRZWANECASHWI"
		,"PHARMACYLINK"
		,"PHARMACEUTICALSCODONLYCOLLEC"
		,"PHARMACYCOD"
		,"PHARMACYCODCOLLECT"
		,"PHYAL"
		,"MEDDEPOT"
		,"PHARMACYPTYLTDMC"
		,"PHCYSCRIPTN"
		,"CODUPFRONTONLY"
		,"PHARMACYNEWMC"
		,"CCSCRIP"
		,"APTEEKMCCODUPRFRONTONLY"
		,"PHARMACYMCAL"
		,"PHARMACYAPTEEKKBY"
		,"PMA"
		,"DELIVER"
		,"DISPENSARY"
		,"DISPENSARYMC"
		,"DISPENSE"
		,"PHARMACYMEDICINEDEPOTMC"
		,"NEWDISPENSING"
		,"APT"
		,"PHARMACYMEDDEPOTSMI"
		,"PHARMACYMEDDEPOT"
		,"PHARMACEUTICALSCCGP"
		,"CASHU"
		,"PHARMACYCCMC"
		,"APTEEKEDMSBPK"
		,"BKSC"
		,"PHARMACYPTYLTDSCR"
		,"strictlyCOD"
		,"PHYPURCHASEM"
		,"CCSCRIPT"
		,"SCRIPTN"
		,"CLOSEDISPENSARYCOD"
		,"APTEEKDASELLODIPEPTA"
		,"PHARMAWHOLESALERS"
		,"PHARMY"
		,"PHARMACYNEWOWNER");
		 
	}
	 
	function index() {
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');

		$this->load->library('listfiles', array('txt', 'csv'));
		$data['files'] = $this->listfiles->getFiles($this->base_dir);
		$data['error'] = ' ';

		$this->load->view('upload_select_date',$data);


		//print_r($data);

		$this->load->view('footer');
	}
	
	
	 
	function _match_headings($items) {
		$ret = false;
		
		//print_r($items);
		
		//get headings from database - we going to need a model here
		$headings = $this->process->get_all_headings();
		
		//print_r($headings);
		
		//reformat array base on previous code
		$tmp_heading_array = array();
		foreach($headings as $heading) {
			$tmp_heading_array[] = array('TERRITORY' 	=> trim(strtoupper($heading["rh_terr_name"])),
										'PRODUCT'		=> trim(strtoupper($heading["rh_product_name"])),
										'SHIP'			=> trim(strtoupper($heading["rh_qty"])),
										'SALE'			=> trim(strtoupper($heading["rh_cost"]))
										);
		}
		//$headings[]=array('TERRITORY'=>'XCUSNAME', 'PRODUCT'=>'XITMDSCR', 'SHIP'=>'XQTYSHIP', 'SALE'=>'XEXTCOST');
		//$headings[]=array('TERRITORY'=>'SNAMA', 'PRODUCT'=>'ITDSCB', 'SHIP'=>'SHPQTB', 'SALE'=>'LNSBUB');
			
		//print_r($tmp_heading_array);
		
		$cnt = 0;
		foreach($tmp_heading_array as $heading) {
			$row_number = 0;
			foreach($items as $item) {
				$needle = trim(strtoupper($item));
				$haystack = $heading;
				//print "We look for $needle in \r\n";print_r($haystack);
				$tmp = array_search($needle, $haystack);
				//print "WE get back $tmp \r\n";
				if($tmp !== false) {
					//print "We found ".$item." in row ".$row_number." with tmp ".$tmp;
					$this->row_places[$tmp] = $row_number;
					$cnt++;
				}				
				$row_number++;
			}
			
			//We must only match 4 columns, once we've matched we return
			if($cnt == 4) return true;			
		}
		
		return $ret;
	
	}
	 
	function _process_line($line) {
		//$this->row_places;
		
		
		if(!isset($line[$this->row_places['TERRITORY']])||trim($line[$this->row_places['TERRITORY']])=="") return; 
		if(!isset($line[$this->row_places['PRODUCT']])||trim($line[$this->row_places['PRODUCT']])=="") return;
		
		$territory_name=$line[$this->row_places['TERRITORY']];
		//We remove all but a-z and 0-9
		$territory_name=strtoupper(preg_replace("/[^a-zA-Z]/", "", $territory_name));
		//Now we remove as requested
		$territory_name=$this->_remove_chars_from_terr($territory_name);
		
		
		//echo "<br/>T:".$territory_name;
		    	
    	$t_id=$this->terr->get_territory_id_by_name($territory_name);
    	if($t_id===false) {
    		$t_id=$this->terr->add_territory($territory_name);
    		if($t_id!==false) {
    			$this->total_add_territories++;
    		}
    		else {
    			print "Problem adding territory.";
    			return;	
    		}
    	}
    	//else echo "T_ID: ".$t_id;
		//echo " P: ".$line[$this->row_places['PRODUCT']];
		
    	$product_name=trim($line[$this->row_places['PRODUCT']]);
		$p_id=$this->product->get_product_id_by_name($product_name);
    	if($p_id===false) {
    		$p_id=$this->product->add_product($product_name);
    		if($p_id!==false) {
    			$this->total_add_products++;
    		}
    		else {
    			print "Problem adding product.";
    			return;	
    		}
    	}
    	
		//else echo "P_ID: ".$p_id;
		//echo " S: ".$line[$this->row_places['SHIP']];
		//echo " S: ".$line[$this->row_places['SALE']];
				
		//check terr
		//check product
		$this->sales_lines[]=array("territory_id"=>$t_id,"product_id"=>$p_id,"item_count"=>$line[$this->row_places['SHIP']],
			"sales_total"=>$line[$this->row_places['SALE']]);
		//$tmp.="(".$i["territory_id"].",".$i["product_id"].",".$i["item_count"].",".$i["sales_total"].",'".$i["import_code"]."),";
		//insert sales
	}
	 
	function _remove_chars_from_terr($terr)
	{
		foreach($this->chars as $k)
		{
			$k="/".strtoupper($k)."\$/";			
			if(preg_match($k,$terr))
			{
				return trim(preg_replace($k,'',$terr));
			}
		}
		return $terr;
	}	
	
	function compile($year, $month, $filename) {
		$error = "";		
		//rename b4 we compile
		
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		
		$fh = @fopen($this->base_dir."/".$filename, "r");
		
		if($fh===false) {
			$this->load->view('error',array('err'=>"Failed to open file: ".$filename));
			$this->load->view('footer');
			return;
		}
		
		$k 			= 0;
		$import_id 	= time();
		
		while (($line = fgetcsv($fh, 8096, ",","\"")) !== FALSE) {
			if($k == 0) {
				$ret = $this->_match_headings($line);
				if($ret === false) {
					$error = "Could no match headings.";		
					$this->load->view('error', array('err' => $error));
					$this->load->view('footer');
					return;							
				}
			} else {
				//we going to need the model here
				//insert data into load table
				//print "we insert data";
				if(trim($line[ $this->row_places['TERRITORY'] ]) == "" || trim($line[ $this->row_places['PRODUCT'] ]) == "") {
					//we ignore lines with no terr names	
				} else {
				$sale_amount = 	trim(preg_replace("/[^0-9.-]/", "", $line[$this->row_places['SALE']]));
				$ship_amount = 	trim(preg_replace("/[^0-9.-]/", "", $line[$this->row_places['SHIP']]));
				$rs = $this->process->insert_load_data(	trim($line[ $this->row_places['TERRITORY'] ]), 
													trim($line[ $this->row_places['PRODUCT'] ]), 
													trim($ship_amount),
													trim($sale_amount), 
													trim($year), 
													trim($month), 
													trim($import_id), 
													trim($filename)
												);
					//if ($rs === false)
				}  
				//$this->_process_line($items);
				//$this->total_lines++;
			}
			//print "\n<br>";
				
			//if($k>510) break;
			$k++;
		}
	
		$this->compile_data($import_id);
	}
	
	function clear_data() {
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		
		$this->load->helper('form');
		
		$year 	= $this->input->post('year');
		$month	= $this->input->post('month');
		$action = $this->input->post('action');
		
		if($year !== false && $month !== false && $action !== false) {
			//show clear data
			if($action == "clear_data" ) {
				$rs = $this->process->clear_data($year, $month);
				$this->load->view('clear_data_complete', array("rs" => $rs));
			}
		} else if( $year !== false && $month !== false) {
			//show confirm
			$rs = $this->process->clear_data_get_data($year, $month);
			$this->load->view('clear_data_confirm', array("rs" => $rs, "year" => $year, "month" => $month));
		} else {
			//show select dates
			$this->load->view('clear_data');
		}
		$this->load->view('footer');
	}
	
	function insert_data($import_id) {
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		$cnt_good 	= 0;
		$cnt_bad	= 0;
		$items = $this->process->get_load_data($import_id);
		$data = array();
		if(count($items) > 0) {			
			foreach($items as $item) {

				$territory_name = $item["ld_terr_name"];
				
				//We remove all but a-z and 0-9 and convert to upper case
				$territory_name = strtoupper(preg_replace("/[^a-zA-Z]/", "", $territory_name));
				
				//Now we remove as requested
				$territory_name = $this->_remove_chars_from_terr($territory_name);
				    	
		    	$t_id=$this->terr->get_territory_id_by_name($territory_name);
    			if($t_id===false) {
    				$t_id=$this->terr->add_territory($territory_name);
    			}
    			
				$product_name=trim($item["ld_product_name"]);
				$p_id=$this->product->get_product_id_by_name($product_name);
    			if($p_id===false) {    				
    				$p_id=$this->product->add_product($product_name);
    			}
    			
    			$date_sold = $item["ld_year"]."-".$item["ld_month"]."-01 00:00:00";
    			
    			$rs = $this->process->insert_sale_record($date_sold, $item["ld_product_qty"], $item["ld_sale_amount"], $t_id, $p_id, $import_id);
    			//<td style=\"background-color: $t;\">".$item["ld_terr_name"]."</td>    	
    			if($rs === true) $cnt_good ++;
    			else $cnt_bad++;	
    		}    		    
		}
		$data = array("cnt_bad" => $cnt_bad, "cnt_good" => $cnt_good);
		
		$this->load->view('process_files_done',$data);
		$this->load->view('footer');
		
		
	}
	
	function compile_data($import_id) {
		$items = $this->process->get_load_data($import_id);
		$data = array();
		if(count($items) > 0) {			
			foreach($items as $item) {

				$territory_name = $item["ld_terr_name"];
				
				//We remove all but a-z and 0-9 and convert to upper case
				$territory_name = strtoupper(preg_replace("/[^a-zA-Z]/", "", $territory_name));
				
				//Now we remove as requested
				$territory_name = $this->_remove_chars_from_terr($territory_name);
				    	
		    	$t_id=$this->terr->get_territory_id_by_name($territory_name);
    			if($t_id===false) {
    				$t = "#FF1111";
    			} else {
    				$t = "#FFFFFF";
    			}
    			
				$product_name=trim($item["ld_product_name"]);
				$p_id=$this->product->get_product_id_by_name($product_name);
    			if($p_id===false) {
    				$p = "#FF1111";
    			} else {
    				$p = "#FFFFFF";
    			}
    			
    			//<td style=\"background-color: $t;\">".$item["ld_terr_name"]."</td>
    			$data[]= "<tr>
    						<td style=\"background-color: $t;\">".$territory_name."</td>    						
    						<td style=\"background-color: $p;\">".$item["ld_product_name"]."</td>
    						<td>".$item["ld_product_qty"]."</td>
    						<td>".$item["ld_sale_amount"]."</td>
    				</tr>";
    		}    		    
		}
		$this->load->view('process_files', array("data" => $data, "import_id" => $import_id));
	}
	
	function process($filename) {
		//post file name

		$error="";
		$fh = @fopen($this->base_dir."/".$filename, "r");
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		if($fh===false) {
			$this->load->view('error',array('err'=>"Failed to open file: ".$filename));
			$this->load->view('footer');
			return;
		}
		$k=0;
		while (($items = fgetcsv($fh, 8096, ",","\"")) !== FALSE) {
			if($k==0) {
				$ret=$this->_match_headings($items);
				if($ret===false) {
					$error="Could no match headings.";
					break;
				}
			} else {
				$this->_process_line($items);
				$this->total_lines++;
			}
			//print "\n<br>";
				
			if($k>510) break;
			$k++;
		}
		
		fclose($fh);
		
		rename($this->base_dir."/".$filename, $this->base_dir."/processed_".$filename);
		
		//print "<br/>Summary data	";
		//print "<br/>Total Territories Added				: ".$this->total_add_territories;
		//print "<br/>Total Products Added				: ".$this->total_add_products;
		//print "<br/>Inserted Sales Records				: ".$this->sales->insert_sales_data($this->sales_lines,$this->date_sold,$this->import_code);
		
		
		$data=array("total_add_territories"=>$this->total_add_territories,
			"total_add_products"=>$this->total_add_products,
			"total_sales_records"=>$this->sales->insert_sales_data($this->sales_lines,$this->date_sold,$this->import_code)
		);
		
		
		$this->load->view('process_files_summary',$data);
		$this->load->view('footer');
		//Process files get moved
		rename($this->base_dir."/".$filename, $this->base_dir."/processed/".$filename);
	}
	 	 
}



