<?php
class Graph extends CI_Controller {
 		
	function index()
	{
		$this->load->model('Sales_model','sales');
		$total_sales_by_month=$this->sales->get_total_sales_for_month();
		$data=array("total_sales_by_month"=>$total_sales_by_month);
		
		$this->load->view('authheader');
		$this->load->view('header');
    	$this->load->view('menu');
    	$this->load->view('graph_overview',$data);
    	$this->load->view('footer');
	}
	
	function product_group_per_month()
	{
		$this->load->model('Sales_model','sales');
		$product_sales_by_month=$this->sales->get_product_sales_by_month();
		$data=array("product_sales_by_month"=>$product_sales_by_month);
		
		$this->load->view('authheader');
		$this->load->view('header');
    	$this->load->view('menu');
    	$this->load->view('graph_product_group_per_month',$data);
    	$this->load->view('footer');
	}
}
?>