<?php

class Upload extends CI_Controller {
  	
  	function index() {  		  	
  		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');					
		$this->load->view('upload_form',array('error' => ' '));		
		$this->load->view('footer');
		//echo getcwd();
  	}
  	
  	function do_upload()
	{
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');	
// Hetzner fix		$config['upload_path'] = '/var/www/system/uploads/';
		$config['upload_path'] = '/usr/wwws/users/ihssms/';
//usr/wwws/users/ihssms/ihs

		
		$config['allowed_types'] = 'csv|txt';
		$config['max_size']	= '1000000';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		
		$this->load->library('upload', $config);
	
		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			
			$this->load->view('upload_form', $error);
		}	
		else
		{
			$data = array('upload_data' => $this->upload->data());
			
			$this->load->view('upload_success', $data);
		}
		$this->load->view('footer');
	}	
	
  	
}
