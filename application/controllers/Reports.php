<?php

class Reports extends CI_Controller {

	var $report_list;

	function __construct() {		
		parent::__construct();
					
		$report_list=array();
			
		$this->report_list[1] = array(
		"report_name"     => "Agent Comm per Product Group",
		"report_function" => "agent_comm_per_product_group",		 
		"report_export_csv" => true,
		"report_select_dates" => true,
    	"report_select_agent" => false
		);

		$this->report_list[2] = array(
		"report_name"     => "Total Sales and Comm per Agent",
		"report_function" => "total_sales_and_comm_per_agent",
		"report_export_csv" => true,
		"report_select_dates" => true,
		"report_select_agent" => true
		);

		$this->report_list[3] = array(
		"report_name"     => "Agent Comm per Product Group",
		"report_function" => "agent_comm_per_product_group",
		"report_export_csv" => false,
		"report_select_dates" => false,
		"report_select_agent" => true
		);

		$this->report_list[4] = array(
		"report_name"     => "Site Visits per Agent",
		"report_function" => "site_visits_per_agent",
		"report_export_csv" => true,
		"report_select_dates" => true,
		"report_select_agent" => true
		);

		$this->report_list[5] = array(
		"report_name"     => "Sales vs Site Visits per Agent per Month",
		"report_function" => "sales_vs_site_visits_per_agent_per_month",
		"report_export_csv" => true,
		"report_select_dates" => true,
		"report_select_agent" => true
		);
		
		$this->report_list[6] = array(
		"report_name"     => "Complete Territory List",
		"report_function" => "complete_territory_list",
		"report_export_csv" => true,
		"report_select_dates" => false,
		"report_select_agent" => true
		);
		
		$this->report_list[7] = array(
		"report_name"			=> "Site Visit Summary",
		"report_function"		=> "get_site_visit_summary",
		"report_export_csv"		=> true,
		"report_select_dates" 	=> true,
		"report_select_agent" 	=> false
		);
	}

	function index() {
		//plz select report
		//click for csv
		//select dates where applicable


		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');

		$this->load->model('Agent_model','agent');
		$all_agents=$this->agent->get_all_agents();

		$this->load->view('reports/select_report',$data=array("report_list"=>$this->report_list,"agents"=>$all_agents));
		$this->load->view('footer');
	}

	function show_report() {

		$csv 	= $this->input->post('csv');
		$report	= $this->input->post('report');

		//$agent	= $this->input->post('agent');

		if(isset($this->report_list[$report])) {
			if($this->report_list[$report]["report_export_csv"]===true) {
				if($csv=="on") $csv=true;
				else $csv=false;
			}
			//ref: http://php.net/manual/en/functions.variable-functions.php
			$function=$this->report_list[$report]["report_function"];
			$this->$function($csv);
		}

	}
	
	function complete_territory_list($csv=false) {
		//Here we just export the territory data
	}

	function total_sales_per_territory_for_agent($csv) {
		//We should be able to do this per month to get nice trend data
		//Very cool quert show sales per tettiroy
		//select t.name as territory_name, sum(p.saleamount)
		//from territory as t
		//left join productsales as p on (t.id = p.territory_id)
		//where t.agent_id = 1
		//group by t.id
		//order by territory_name
	}

	function _get_months($date1, $date2) {
		$time1  = strtotime($date1);
		$time2  = strtotime($date2);
		$my     = date('mY', $time2);

		$months = array(date('M Y', $time1));

		while($time1 < $time2) {
			$time1 = strtotime(date('Y/m/d', $time1).' +1 month');
			if(date('mY', $time1) != $my && ($time1 < $time2))
			$months[] = date('M Y', $time1);
		}

		$months[] = date('M Y', $time2);
		return $months;
	}

	function sales_vs_site_visits_per_agent_per_month($csv=false) {
		$from 	= $this->input->post('from');
		$to 	= $this->input->post('to');
		$agent 	= $this->input->post('agent');
		
				
		//Validate data
		if(!isset($agent)||trim($agent)=="") {
			$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('error', array("err"=>"Please select a Agent."));
			$this->load->view('footer');
			return;
		}
	

		$this->load->model('Agent_model','agent');
		$agent_name = $this->agent->get_agent_name($agent);

		$report_title = "Sales vs Site Visits for $agent_name for $from to $to";

		//We check the input data
		if(isset($from)&&trim($from)!=""&&isset($to)&&trim($to)!="") {

			$this->load->model('Report_model','report');
			$report_data=$this->report->get_sales_vs_site_visits_per_agent_per_month($from,$to,$agent);

			$months = $this->_get_months($from,$to);
				
			//Create Headings
			$report_headings=array();
			$report_headings[]="Territory";
			foreach($months as $m) {
				$report_headings[]=$m."<br/> Sales";
				$report_headings[]=$m."<br/> Site Visits";
			}
				
			//Create Row Data
			$report_rows=array();
			foreach($report_data as $k=>$i) {
				$row=array();
				$row[]=$k;

				foreach($months as $m) {
					$t  = strtotime($m);

					if(isset($i[date('Y', $t)][date('n', $t)])) {
						$row[]=$i[date('Y', $t)][date('n', $t)]["sale_amount"];
						$row[]=$i[date('Y', $t)][date('n', $t)]["visit_num"];
					}
					else {
						$row[]="0";
						$row[]="0";
					}
				}
				$report_rows[]=$row;
			}
				
			//Calculate Totals and Add to Row data
			$report_totals=array();
			for($i=0;$i<count($report_rows);$i++) {
				for($j=0;$j<count($report_rows[$i]);$j++) {
					if(is_numeric($report_rows[$i][$j])) {
						if(isset($report_totals[$j])&&$report_totals[$j]>=0) $report_totals[$j]+=$report_rows[$i][$j];
						else $report_totals[$j]=$report_rows[$i][$j];
					}
					else {
						if(isset($report_totals[$j])&&$report_totals[$j]>=0) ;
						else $report_totals[$j]=0;
					}
				}
			}
			$report_totals[0]="Totals:";
			$cnt=count($report_rows);
			$report_rows[$cnt]=$report_totals;
				
			$data = array("report_title"=>$report_title,
					"report_headings"=>$report_headings,
					"report_rows"=>$report_rows,
					"header_alt_color"=>true,
					"row_alt_color"=>true
			);

			$this->load->view('authheader');
			if($csv===true) {				
				$this->_process_data($report_title,$report_headings,$report_rows);
			}
			else {
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('reports/standard', $data);
				$this->load->view('footer');
			}
							
		}

	}

	function total_sales_and_comm_per_agent($csv=false) {

		$from 	= $this->input->post('from');
		$to 	= $this->input->post('to');
		$agent 	= $this->input->post('agent');

		$report_title = "Total Sales and Comm per Agent";
		$this->load->model('Agent_model','agent');
		$all_agents=$this->agent->get_all_agents();

		$this->load->model('Product_group_model','pg');
		$all_product_groups=$this->pg->get_all_product_groups();

		$this->load->model('Report_model','report');
		$report_data=$this->report->get_total_sales_and_comm_per_agent($from,$to,$agent);

		//print_r($report_data);

		////Create Headings
		$report_headings=array("Agent","Customer","Product","Sales Amount","Comm Amount");

		//Create Row Data
		$report_rows=array();
		foreach($report_data as $i) {
			//$report_rows[]=array($i["name"]." ".$i["surname"],$i["customer"],$i["product"],number_format($i["sales"],2),number_format($i["comm"],2));
			$report_rows[]=array($i["name"]." ".$i["surname"],$i["customer"],$i["product"],number_format($i["sales"],2,'.',''),number_format($i["comm"],2,'.','') );
		}

		//Calculate Totals and Add to Row data
		$report_totals=array();
		for($i=0;$i<count($report_rows);$i++) {
			for($j=0;$j<count($report_rows[$i]);$j++) {
				if(is_numeric($this->str2num($report_rows[$i][$j]))) {
					if(isset($report_totals[$j]) && $report_totals[$j] >= 0) $report_totals[$j] += $this->str2num($report_rows[$i][$j]);
					else $report_totals[$j] = $this->str2num($report_rows[$i][$j]);
				}
				else {
					if(isset($report_totals[$j]) && $report_totals[$j] >= 0) ;
					else $report_totals[$j] = 0;
				}
			}
		}
		
		$report_totals[0]="Totals:";
		//for($i=1;$i<count($report_totals);$i++) {
		//	if(is_numeric($report_totals[$i])) $report_totals[$i] = number_format($report_totals[$i],2);
		//}
		
		$cnt=count($report_rows);
		$report_rows[$cnt]=$report_totals;

		//print_r($report_rows);
		$data = array("report_title"=>$report_title,
					"report_headings"=>$report_headings,
					"report_rows"=>$report_rows
		);

		$this->load->view('authheader');
		if($csv===true) {
			$this->_process_data($report_title,$report_headings,$report_rows);
		}
		else {
			$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('reports/standard', $data);
			$this->load->view('footer');
		}
	}

	function total_sales_report() {
		/*
		 * select sum(productsales.saleamount) as sales,
			sum(productsales.saleamount * product.comm/100) as comm,
			agent.agentcode as agent_code,
			agent.name as name,
			agent.surname as surname,
			territory.name as terr_name
			from productsales, product, agent, territory
			where datesold >= '2008/10/01 00:00:00' and datesold <= '2008/11/01 23:59:59'
			and productsales.product_id = product.id
			and productsales.Territory_Id = territory.Id
			and territory.agent_id = agent.agentcode
			group by agent.agentcode,territory.id
			order by name asc,surname,terr_name
		 */
	}

	function territories_per_agent() {

	}

	//also do this per agent
	function site_visits_per_agent($csv=false) {
		//get_site_visits_agent

		$from 	= $this->input->post('from');
		$to 	= $this->input->post('to');
		$agent	= $this->input->post('agent');

		$report_title="Site Visits per Agent";
		//$this->load->model('Agent_model','agent');
		//$all_agents=$this->agent->get_all_agents();

		//$this->load->model('Product_group_model','pg');
		//$all_product_groups=$this->pg->get_all_product_groups();

		$this->load->model('Report_model','report');
		$report_data=$this->report->get_site_visits_agent($from,$to,$agent);

		//print_r($report_data);

		////Create Headings
		$report_headings=array("Agent","Date Visited","Territory");

		//Create Row Data
		$report_rows=array();
		foreach($report_data as $i) {
			$report_rows[]=array($i["name"]." ".$i["surname"],substr($i["datevisited"],0,10),$i["t_name"]);
		}

		//print_r($report_rows);
		$data = array("report_title"=>$report_title,
					"report_headings"=>$report_headings,
					"report_rows"=>$report_rows
		);

		$this->load->view('authheader');
		if($csv===true) {
			$this->_process_data($report_title,$report_headings,$report_rows);
		}
		else {
			$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('reports/standard', $data);
			$this->load->view('footer');
		}

	}


	function agent_comm_per_product_group($csv=false) {

		$from 	= $this->input->post('from');
		$to 	= $this->input->post('to');

		$report_title="Agent comm per product group";
		$this->load->model('Agent_model','agent');
		$all_agents=$this->agent->get_all_agents();

		$this->load->model('Product_group_model','pg');
		$all_product_groups=$this->pg->get_all_product_groups();

		$this->load->model('Report_model','report');
		$report_data=$this->report->get_agent_comm_per_product_group($from,$to);
		
		////Create Headings
		$report_headings=array();
		$report_headings[0]="";
		foreach($all_product_groups as $product_group) {
			$report_headings[]=$product_group["name"];
		}

		////Create Row Data
		$report_rows=array();
		$l=0;
		foreach($all_agents as $agent) {
			$report_rows[$l][0]=$agent["name"]." ".$agent["surname"];
			$k=1;
			foreach($all_product_groups as $product_group) {
				if(isset($report_data[$product_group["id"]]) && isset($report_data[$product_group["id"]][$agent["agentcode"]])) {
					$report_rows[$l][$k]=$report_data[$product_group["id"]][$agent["agentcode"]]["comm"];
					$report_rows[$l][$k]=number_format($report_rows[$l][$k],2,'.','');
					//print "".number_format($report_data[$product_group["id"]][$agent["agentcode"]]["comm"],2)."";
				}
				else $report_rows[$l][$k]="0";
				$k++;
			}
			$l++;
		}

		//Calculate Totals and Add to Row data
		$report_totals=array();
		for($i=0;$i<count($report_rows);$i++) {
			for($j=0;$j<count($report_rows[$i]);$j++) {
				if(is_numeric($this->str2num($report_rows[$i][$j]))) {
					if(isset($report_totals[$j])&&is_numeric($report_totals[$j])) 
						$report_totals[$j] += $this->str2num($report_rows[$i][$j]);
					else $report_totals[$j] = $this->str2num($report_rows[$i][$j]);
				}
				else {
					if (isset($report_totals[$j]) && is_numeric($report_totals[$j])) ;
					else $report_totals[$j]=0;
				}
			}
		}

		$report_totals[0]="Totals:";
		$cnt=count($report_rows);
		$report_rows[$cnt]=$report_totals;

		$data = array("report_title"=>$report_title,
					"report_headings"=>$report_headings,
					"report_rows"=>$report_rows
		);

		$this->load->view('authheader');
		if($csv===true) {
			$this->_process_data($report_title,$report_headings,$report_rows);
		}
		else {
			$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('reports/standard', $data);
			$this->load->view('footer');
		}
	}

	function _process_data($report_title,$report_headings,$report_rows) {
		$csv_data="";
		foreach($report_headings as $i) {
			$csv_data.='"'.str_replace("<br/>","\n",$i).'",';
		}
		foreach($report_rows as $i) {
			$csv_data.="\n";
			foreach($i as $j) {
				$csv_data.='"'.$j.'",';
			}
		}

		$this->_download_csv($csv_data,$report_title);
	}

	function _download_csv($data,$filename) {
		$this->output->set_header("Content-Type: text/csv");
		//replace  space with underscore
		//add time and .csv
		$filename=str_replace(" ","_",$filename);
		$filename=$filename.time().".csv";
			
		$this->output->set_header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		$this->output->set_header("Expires: 0");
		//IE is the biggest pile of @##$@#@@#@$
		if (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
		{
			$this->output->set_header('Content-Disposition: inline; filename="'.$filename.'"');
			$this->output->set_header('Pragma: public');
		}
		else
		{
			$this->output->set_header('Content-Disposition: attachment; filename="'.$filename.'"');
			$this->output->set_header('Pragma: no-cache');
		}
		$this->output->set_output($data);
	}
	
	function get_site_visit_summary($csv=false) {

		$from 	= $this->input->post('from');
		$to 	= $this->input->post('to');

		$report_title="Site Visit Summary - from $from to $to";
		$this->load->model('Agent_model','agent');
		$all_agents=$this->agent->get_all_agents();

		$this->load->model('Report_model','report');
		$report_data=$this->report->get_site_visit_summary($from,$to);

		////Create Headings
		$report_headings=array();
		$report_headings[0]="";
		foreach($all_agents as $agent) {
			$report_headings[]=$agent["name"]." ".$agent["surname"];
		}
		
		//print_r($report_data);
		//return;

		$ratings = array ("A", "B", "C");
		$visit_total = array();
		
		//Reformating the data for the for loop
		foreach($report_data as $d) {
			$visit_total[$d["agent_id"]][$d["rating"]] = $d["cnt"];
		}
		
		////Create Row Data
		$report_rows=array();
		$l=0;
		foreach($ratings as $rating) {
			$report_rows[$l][0]=$rating;
			$k=1;
			foreach($all_agents as $agent) {	
				if(isset($visit_total[$agent["agentcode"]][$rating])) {
					$report_rows[$l][$k] = $visit_total[$agent["agentcode"]][$rating];
				}
				else $report_rows[$l][$k]="0";
				$k++;
			}
			$l++;
		}
		
		//print_r($report_rows);
		//return;

		//Calculate Totals and Add to Row data
		$report_totals = array();
		for($i=0;$i<count($report_rows);$i++) {
			for($j=0;$j<count($report_rows[$i]);$j++) {
				if(is_numeric($report_rows[$i][$j])) {
					if(isset($report_totals[$j])&&$report_totals[$j]>=0) $report_totals[$j]+=$report_rows[$i][$j];
					else $report_totals[$j]=$report_rows[$i][$j];
				}
				else {
					if(isset($report_totals[$j])&&$report_totals[$j]>=0) ;
					else $report_totals[$j]=0;
				}
			}
		}

		$report_totals[0]="Totals:";
		$cnt=count($report_rows);
		$report_rows[$cnt]=$report_totals;

		$data = array("report_title"=>$report_title,
					"report_headings"=>$report_headings,
					"report_rows"=>$report_rows
		);

		$this->load->view('authheader');
		if($csv===true) {
			$this->_process_data($report_title,$report_headings,$report_rows);
		}
		else {
			$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('reports/standard', $data);
			$this->load->view('footer');
		}
	}
	
	function str2num($str) { 
  		if(strpos($str, '.') < strpos($str,',')){ 
            $str = str_replace('.','',$str); 
            $str = strtr($str,',','.');            
        } 
        else{ 
            $str = str_replace(',','',$str);            
        } 
        return (float)$str; 
	}
	
}