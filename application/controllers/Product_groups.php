<?php
class Product_groups extends CI_Controller {
	
	function index() {
		$this->load->view('authheader');
		$this->load->view('header');
    	$this->load->view('menu');
    	
    	$this->load->model('Product_group_model','pg');    	
    	$product_groups=$this->pg->get_all_product_groups();
    	$this->load->model('Principal_model','pm');    	
    	$principal_groups=$this->pm->get_all_principals();
    	$this->load->view('product_groups',array("data"=>$product_groups,"principal_groups"=>$principal_groups));
    	    	
    	$this->load->view('footer');
	}
	
	function delete() {				
		$id=$this->input->post('Id');		
		$this->load->model('Product_group_model','p');    	
    	$ret=$this->p->delete_product_group($id);    	
		if($ret===true) print "TRUE";
		else print "FALSE";	
	}
	
	function add() {
		
		$this->load->view('authheader');
		$this->load->view('header');
    	$this->load->view('menu');
    	$this->load->model('Principal_model','pm');    	
    	$principal_groups=$this->pm->get_all_principals();
    	
    	$this->load->view('product_group_add',array("principal_groups"=>$principal_groups));
    	$this->load->view('footer');
	}
	
	function do_add() {
		$this->load->view('authheader');
		$this->load->view('header');
    	$this->load->view('menu');
    	
    	$this->load->model('Product_group_model','p');    	
    	$ret=$this->p->add_product_group($this->input->post('name'),$this->input->post('principal'));    	
		if($ret===true) $this->load->view('error',array("err"=>"The Product Group has been added.")); 
		else $this->load->view('error',array("err"=>"There was a problem adding the Product Group."));
    	
    	$this->load->view('footer');
	}
	
	function save() {
		$id=$this->input->post('id');
		$this->load->model('Product_group_model','p');    	
    	$ret=$this->p->save_product_group($id,$this->input->post('name'),$this->input->post('principal'));    	
		if($ret===true) print "TRUE";
		else print "FALSE";
	}
}
?>
