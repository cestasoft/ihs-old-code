<?php

class Agents extends CI_Controller {
  	  	
	function index() {
		
		$this->load->model('Agent_model','agents');
		$all_agents=$this->agents->get_all_agents();
		
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		$data = array("all_agents"=>$all_agents);		
		$this->load->view('agents', $data);		
		$this->load->view('footer');		
	}
	
	function edit($agent_id) {	
		$this->load->model('Agent_model','agents');
		$all_agents=$this->agents->get_all_agents($agent_id);
		
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		$data = array("all_agents"=>$all_agents);		
		$this->load->view('agent_edit', $data);		
		$this->load->view('footer');		
	}
	
	function save() {
		$agent=array(
		"id" => $this->input->post('id'),		
		"title" => $this->input->post('title'),
		"name" => $this->input->post('name'),
		"surname" => $this->input->post('surname'),
		"telnoh" => $this->input->post('telnoh'),
		"physicaladdress1" => $this->input->post('physicaladdress1'),
		"physicaladdress2" => $this->input->post('physicaladdress2'),
		"physicaladdress3" => $this->input->post('physicaladdress3'),
		"physicaladdress4" => $this->input->post('physicaladdress4'),
		"physicalcode" => $this->input->post('physicalcode'),
		"postaladdress1" => $this->input->post('postaladdress1'),
		"postaladdress2" => $this->input->post('postaladdress2'),
		"postaladdress3" => $this->input->post('postaladdress3'),
		"postaladdress4" => $this->input->post('postaladdress4'),		
		"postalcode" => $this->input->post('postalcode'),
		"username" => $this->input->post('username'),
		"password" => $this->input->post('password')
		);
						
		$this->load->model('Agent_model','agents');
		$ret=$this->agents->update($agent);
		if($ret===TRUE) {
			print "TRUE";
		} else {			
			$this->load->view('msg', array("msg" => "Problem updating agent."));
		}
		//$this->load->view('footer');		
	}
	
	function delete() {
		$this->load->view('authheader');
		$id=$this->input->post('id');		
		//$this->load->model('Agent_model','a');    
		$this->load->model('Agent_model','agent');	
    	$ret=$this->agent->delete_agent($id);    	
		if($ret===true) print "TRUE";
		else print "FALSE";	
	}
	
	function add() {
					
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');		
		$this->load->view('agent_add');		
		$this->load->view('footer');	
	}
	
	function do_add() {
			
		$this->load->model('Agent_model','agent');
		$ret=$this->agent->add_agent(
		$this->input->post('title'),
		$this->input->post('name'),
		$this->input->post('surname'),
		$this->input->post('telnoh'),
		$this->input->post('physicaladdress1'),
		$this->input->post('physicaladdress2'),
		$this->input->post('physicaladdress3'),
		$this->input->post('physicaladdress4'),
		$this->input->post('physicalcode'),
		$this->input->post('postaladdress1'),
		$this->input->post('postaladdress2'),
		$this->input->post('postaladdress3'),
		$this->input->post('postaladdress4'),		
		$this->input->post('postalcode'),
		$this->input->post('username'),
		$this->input->post('password')
		);    
			
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');		
		if($ret===true) {
			$this->load->view('error',array('err'=>"The agent has been added"));
		} else {
			$this->load->view('error',array('err'=>"There was a problem adding the agent"));
		}		
		$this->load->view('footer');	
	}
}


?>