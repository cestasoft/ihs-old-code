<?php
class Products extends CI_Controller {

	function index() {
		//$this->load->view('authheader');
		$this->load->view('header');
    	$this->load->view('menu');
    	
    	$this->load->model('Product_model','product');
    	$this->load->model('Product_group_model','product_group');
    	$products=$this->product->get_all_products();
    	//print_r($products);
    	$product_groups=$this->product_group->get_all_product_groups();    	
    	$this->load->view('products',array("data"=>$products,"product_groups"=>$product_groups));
    	    	
    	$this->load->view('footer');
	}
	
	function delete() {				
		$id=$this->input->post('id');		
		$this->load->model('Product_model','p');    	
    	$ret=$this->p->delete_product($id);    	
		if($ret===true) print "TRUE";
		else print "FALSE";	
	}
	
	function save() {
		$id=$this->input->post('id');
		$this->load->model('Product_model','p');    	
    	$ret=$this->p->save_product($id,$this->input->post('product_group'),$this->input->post('comm'));    	
		if($ret===true) print "TRUE";
		else print "FALSE";
	}
}
?>
