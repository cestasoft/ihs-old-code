<?php

class Welcome extends CI_Controller {
	
	function index()
	{
		$this->load->view('authheader');
		$this->load->view('header');		
		$this->load->view('menu');	
		$this->load->model('Motd_model','m');    	
		$this->load->view('welcome_message',array("MOTD"=>$this->m->get_message()));		
		$this->load->view('footer');	
		
	}
	
}

?>