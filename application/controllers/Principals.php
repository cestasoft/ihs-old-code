<?php
class Principals extends CI_Controller {
	
	function index() {
		//$this->load->view('authheader');
		$this->load->view('header');
    	$this->load->view('menu');
    	
    	$this->load->model('Principal_model','p');    	
    	$princiapls=$this->p->get_all_principals();    	
    	$this->load->view('principals',array("data"=>$princiapls));
    	    	
    	$this->load->view('footer');
	}
	
	function add() {
		$this->load->view('header');
    	$this->load->view('menu');
    	        	
		$action=$this->input->post('action');
		if(isset($action)&&$action=="ADD") {
			$this->load->model('Principal_model','p');
		} else 
		{
			$this->load->view('principals_add');    	
		}    	
		$this->load->view('footer');
    	
	}
	
	function delete() {
		$id=$this->input->post('Id');		
		$this->load->model('Principal_model','p');    	
    	$ret=$this->p->delete_principal($id);    	
		if($ret===true) print "TRUE";
		else print "FALSE";
	}
	
	function do_add() {
		$id=$this->input->post('Name');
		$this->load->model('Principal_model','p');
		$ret=$this->p->add_principal(
		$this->input->post('Name'),
		$this->input->post('ContactName'),
		$this->input->post('ContactTelNo'),
		$this->input->post('ContactFaxNo'),
		$this->input->post('ContactEmail'));    	
		if($ret===true) $this->index();
		else print "Problem adding Principal";
							
	}
	
	function save() {
		$id=$this->input->post('id');
		$this->load->model('Principal_model','p');    				
    	$ret=$this->p->save_principal($this->input->post('id'),
    					$this->input->post('name'),
    					$this->input->post('contact_name'),
    					$this->input->post('contact_tel'),
    					$this->input->post('contact_fax'),
    					$this->input->post('contact_email'));    	
		if($ret===true) print "TRUE";
		else print "FALSE";
	}
}
?>
