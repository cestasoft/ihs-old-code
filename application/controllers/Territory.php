<?php
define("NUMBER_PER_PAGE",50);

class Territory extends CI_Controller {
	  	  
	function index($agent_id="")	
	{			
		$this->load->view('authheader');
		if($_SESSION['AGENT']=="YES") {
			$agent_id=$_SESSION['AGENT_ID'];
		} else {
			$agent_id=trim($agent_id);
		}
		$this->load->model('Agent_model','agent');
		$all_agents=$this->agent->get_all_agents();
		$this->load->model('Territory_model','terr');
		//Watch out here, we needed intval
		if($agent_id!=""&&is_int(intval($agent_id))) {
			//print "We filter";
			$all_territories=$this->terr->get_all_territories(0,NUMBER_PER_PAGE,$agent_id);
			$total_terr=$this->terr->get_total_num_territories($agent_id);
		}
		else {
			//print "We do not filter: $agent_id";
			$all_territories=$this->terr->get_all_territories(0,NUMBER_PER_PAGE);
			$total_terr=$this->terr->get_total_num_territories();			
		}
		if($total_terr[0]["total"] > NUMBER_PER_PAGE) 
			$page_total=ceil($total_terr[0]["total"] / NUMBER_PER_PAGE);
		else $page_total=1;
		$data=array("all_territories"=>$all_territories,
					"all_agents"=>$all_agents,
					"total_territories"=>$total_terr,
					"page_total"=>$page_total,
					"agent_id"=>$agent_id);		
		$this->load->view('header');
		$this->load->view('menu');
		$this->load->view('territory_header', $data);
		$this->load->view('territory', $data);
		$this->load->view('footer');
	}
	
	function get_territories_via_filter() {
		
		$this->load->view('authheader');	
		
		$filter=$this->input->post('filter');
		$select=$this->input->post('select');
		
		//we make sure filter is at least 4 chars
		if(strlen($filter) < 4) return;
		$this->load->model('Agent_model','agent');
		$all_agents=$this->agent->get_all_agents();
		$this->load->model('Territory_model','terr');
	
		if($_SESSION['AGENT']=="YES") {
			$agent_id 		 = $_SESSION['AGENT_ID'];
			$all_territories = $this->terr->get_territories_for_agent_via_filter($filter,$agent_id);
		} else {
			$all_territories = $this->terr->get_territories_via_filter($filter);
			$agent_id 		 = "";	
		}
		
		$cnt=count($all_territories);
		if($cnt==0) {
			$this->load->view('error', array("err"=>"No results found."));
			return;
		}
		else {			
			$this->load->view('error', array("err"=>"$cnt entries found."));			
			if($cnt>100) {
				$this->load->view('error', array("err"=>"Entries limited to 100."));
				$all_territories=array_slice($all_territories,0,100);
			}
		}		
		$data=array("all_territories"=>$all_territories,
					"all_agents"=>$all_agents,
					"show_select"=>$select,
					"agent_id"=>$agent_id);	
		$this->load->view('territory', $data);
	}
	
	function edit_territory() {
		$this->load->view('authheader');
		$terr_id 	= 	$this->input->post('id');
		if(isset($terr_id) && is_numeric($terr_id) ) {
			$this->load->model('Agent_model','agent');
			$all_agents=$this->agent->get_all_agents();
		
			$this->load->model('Territory_model','terr');
			$terr = $this->terr->get_terr($terr_id);
			if(count($terr) == 1) {
				$this->load->view('terr_edit', 
					array("terr" => $terr, "agents" => $all_agents
					));
			}
		}
	}
	
	function save_territory() {
		$terr_id = $this->input->post('id');
		if(isset($terr_id) && is_numeric($terr_id) ) {
			$data = array(  
						$this->input->post('name'),
						$this->input->post('terr_rating'),
						$this->input->post('PhysicalAddress1'),
						$this->input->post('PhysicalAddress2'),
						$this->input->post('PhysicalAddress3'),
						$this->input->post('PhysicalAddress4'),
						$this->input->post('PhysicalCode'),
						$this->input->post('terr_agent'),
						$this->input->post('Province'),
						$this->input->post('TelNo'),
						$this->input->post('id')
			); 
			$this->load->model('Territory_model','terr');
			if($this->terr->update_territory($data) === true) {
				print "TRUE";
			} else {
				print "FALSE";
			}
		}
	}
	
	function get_territories() {		
		$page		= 	$this->input->post('page_number');
		$agent_id	=	$this->input->post('agent_id');
		//print "We have agent_id $agent_id";
		$this->load->model('Agent_model','agent');
		$all_agents=$this->agent->get_all_agents();
		$this->load->model('Territory_model','terr');
		$all_territories=$this->terr->get_all_territories(($page-1)*NUMBER_PER_PAGE,NUMBER_PER_PAGE,$agent_id);		
		$data=array("all_territories"=>$all_territories,
					"all_agents"=>$all_agents,
					"agent_id"=>$agent_id);		
		$this->load->view('authheader');
		$this->load->view('territory', $data);
	}
	
	function get_territory() {
		$this->load->view('authheader');
		$terr_id 	= 	$this->input->post('id');
		if(isset($terr_id) && is_numeric($terr_id) ) {
			$this->load->model('Agent_model','agent');
			$all_agents=$this->agent->get_all_agents();
			$this->load->model('Territory_model','terr');
			$rs = $this->terr->get_terr($terr_id);
			if(count($rs) == 1) {
				$data=array("territory" => $rs,
								"all_agents"  => $all_agents);		
				$this->load->view('territory_line', $data);
			}	
		}
		
	}
	
	function set_territory_agent() {
		$terr_id=$this->input->post('terr_id');
		$agent_id=$this->input->post('agent_id');
		$this->load->model('Territory_model','terr');
		$rs = $this->terr->set_territory_agent($terr_id,$agent_id);
		if($rs==1)
			print "TRUE";
		else
			print "FALSE";
	}
	
	function set_territory_rating() {			
		$terr_id=$this->input->post('terr_id');
		$rating=$this->input->post('rating');
		$this->load->model('Territory_model','terr');
		$rs = $this->terr->set_territory_rating($terr_id,$rating);
		if($rs==1)
			print "TRUE";
		else
			print "FALSE";
	}
	
	function link() {
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		$this->load->view('terr_link');		
		$this->load->view('footer');
	}
	
	function delete() {
		$this->load->view('authheader');		
		$id=$this->input->post('id');	
		if(!is_numeric($id)) { print "FALSE"; return; }		
		$this->load->model('Territory_model','terr');
    	$ret=$this->terr->hide_territory($id);    	
		if($ret===true) print "TRUE";
		else print "FALSE";	
		
	}
	
	function undelete() {
		$this->load->view('authheader');
		
	}
	
	function add_link() {		
		$this->load->model('Territory_model','terr');		
		$ret=$this->terr->link_territory($this->input->post('parentId'),$this->input->post('childId'),$this->input->post('childName'));
		if($ret===TRUE) print "Link good";
		else print "Link failed";			
	}
	
	function add() {
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		$this->load->view('terr_add');		
		$this->load->view('footer');
	}
	
	function do_add() {
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		
		if($_SESSION['AGENT']=="YES") {
			$AgentCreated 	= 1;
			$agent_id		= $_SESSION['AGENT_ID'];
		} else {
			//Assign to unallocated
			$agent_id		= "19";
			$AgentCreated	= 0;
			$agent_id		= $this->input->post('agent_id');
		}
		
		
		//Limit Input based on DB limits
		//name 60
		//add 60
		//code 10
		//rating A B C
		
		$name 				= substr($this->input->post('terr_name'),0,60);
		$rating				= $this->input->post('terr_rating');
		$PhysicalAddress1	= substr($this->input->post('PhysicalAddress1'),0,60);
		$PhysicalAddress2	= substr($this->input->post('PhysicalAddress2'),0,60);
		$PhysicalAddress3	= substr($this->input->post('PhysicalAddress3'),0,60);
		$PhysicalAddress4	= substr($this->input->post('PhysicalAddress4'),0,60);
		$PhysicalCode   	= substr($this->input->post('PhysicalCode'),0,10);
		$Province			= substr($this->input->post('Province'),0,50);
		$TelNo				= substr($this->input->post('TelNo'),0,20);
		
		if($rating == "A" || $rating == "B" || $rating == "C" || $rating == "D") {
			$this->load->model('Territory_model','terr');
			$this->terr->add_terr(
				$name,
				$agent_id,
				$rating,
				$PhysicalAddress1,
				$PhysicalAddress2,
				$PhysicalAddress3,
				$PhysicalAddress4,
				$PhysicalCode,
				$Province,
				$TelNo,
				$AgentCreated			
			);		
			$this->load->view('terr_add_done');
			$this->load->view('terr_add');
		} else {
			//show error : invalid rating
		}
		$this->load->view('footer');
	}
}
	
?>