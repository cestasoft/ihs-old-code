<?php

class Users extends CI_Controller {
	  	  	
	function index() {
		
		$this->load->model('User_model','user');
		$all_users = $this->user->get_user_details();		
										
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		$data = array("all_users"=>$all_users);		
		$this->load->view('users', $data);		
		$this->load->view('footer');		
		
		
	}
	
	function add() {					
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');			
		$this->load->view('user_add');		
		$this->load->view('footer');		
	}
	function _check_field($v) {		
		if(isset($v)&&trim($v)!="") return false;
		else return true;
	}
	
	function do_add() {
		
		$user=array(			
			"usercode" 	=> $this->input->post('usercode'),		
			"title" 	=> $this->input->post('title'),
			"name" 		=> $this->input->post('name'),
			"surname" 	=> $this->input->post('surname'),
			"password1" 	=> $this->input->post('password1'),
			"password2" 	=> $this->input->post('password2'),
			"password" 	=> $this->input->post('password1'),
			"active" 	=> $this->input->post('active')		
		);
		
		//validate
		$msg="";
		if($this->_check_field($user["usercode"])) $msg.="<br/>Please provide a username.";
		if($this->_check_field($user["title"])) $msg.="<br/>Please provide a title.";
		if($this->_check_field($user["name"])) $msg.="<br/>Please provide a first name.";
		if($this->_check_field($user["surname"])) $msg.="<br/>Please provide a surname.";
		if($this->_check_field($user["password1"])) $msg.="<br/>Please provide a password.";
		else if($this->_check_field($user["password2"])) $msg.="<br/>Please provide a password.";
		if($user["password1"]!=$user["password2"]) $msg.="<br/>The passwords must be equal.";
		if($this->_check_field($user["active"])) $msg.="<br/>Please provide an active status.";
		
 
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');

		if($msg=="") {
		
		$this->load->model('User_model','user');
		if($this->user->create_user($user)===TRUE) {			
			$this->load->view('error', array("err" => "The user has been added successfully."));			
		} else {
			$this->load->view('error', array("err" => "Problem updating user."));
		}

		} else $this->load->view('error', array("err" => $msg));
				
		$this->load->view('footer');
		
	}
	
	function save() {
		$user=array(
			"id" 		=> $this->input->post('id'),
			"usercode" 	=> $this->input->post('usercode'),		
			"title" 	=> $this->input->post('title'),
			"name" 		=> $this->input->post('name'),
			"surname" 	=> $this->input->post('surname'),
			"password" 	=> $this->input->post('password'),
			"active" 	=> $this->input->post('active')		
		);
		
		$this->load->view('authheader');
		//$this->load->view('header');
		//$this->load->view('menu');
			
		$this->load->model('User_model','user');
		if($this->user->update_user($user)===TRUE) {
			print "TRUE";			
		} else {
			$this->load->view('msg', array("msg" => "Problem updating user."));
		}				
		//$this->load->view('footer');		
	}
	
	function delete() {
		$user_id = $this->input->post('user_id');
		$this->load->model('User_model','user');
		$ret=$this->user->delete_user($user_id);
		if($ret===true) print "TRUE";
		else print "FALSE";
	}
}


?>