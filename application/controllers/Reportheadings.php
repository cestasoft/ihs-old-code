<?php
class Reportheadings extends CI_Controller {

 	function __construct() {    	
		parent::__construct();
    	$this->load->model('Reportheading_model','headings');
  	}
	
	function index()
	{
		$this->load->view('authheader');
		$this->load->view('header');
    	$this->load->view('menu');
    	
    	$headings = $this->headings->get_all_headings();    
    	    	
    	$this->load->view('report_headings',array("data"=>$headings));    	    	
    	$this->load->view('footer');
	}
	
	function delete() {				
		$id = $this->input->post('rh_id');		
		$ret = $this->headings->delete_headings($id);    	    	
		if($ret===true) print "TRUE";
		else print "FALSE";	
	}
	
	function save() {
		$post_data = array(
			"rh_province"  		=> $this->input->post('rh_province'),
			"rh_name"  			=> $this->input->post('rh_name'),
			"rh_terr_name"  	=> $this->input->post('rh_terr_name'),
			"rh_product_name"  	=> $this->input->post('rh_product_name'),
			"rh_qty"  			=> $this->input->post('rh_qty'),
			"rh_cost"  			=> $this->input->post('rh_cost'),
			"rh_invoice_no"  	=> $this->input->post('rh_invoice_no'),
			"rh_id"  			=> $this->input->post('rh_id')					
		);
		if($this->input->post('rh_add') == "NEW") {
			$ret = $this->headings->add_headings($post_data);
		} else {   	
    		$ret = $this->headings->update_headings($post_data);
		}    	
		if($ret===true) print "TRUE";
		else if(is_numeric($ret)) print $ret;
		else print "FALSE";
	}
		
}
?>
