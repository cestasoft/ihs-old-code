<?php

class Bulk extends CI_Controller {
	  	  	
	function index() {
		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');			
		$this->load->view('bulkget');		
		$this->load->view('footer');	
	}
	
	function process_data() {
		$dataLines=explode("\n",$this->input->post('load_data'));
		if(count($dataLines)>0) {
			foreach ($dataLines as $i=>$dataLine) {
					$dataLine = str_replace('"','',$dataLine);
					$items = explode(",",$dataLine);
					
					for($i=0;$i<10;$i++) {
						if(!isset($items[$i])) $items[$i] = '';
					}
					
					/*This is the order of items
					agent_id, rating, refid, name, PhysicalAddress1, PhysicalAddress2, PhysicalAddress4
					
					f
					*/
					//We can't insert items without names	
					if(trim($items[2]) == "") continue;			
					//If we don't have a grade we set to C
					//if(!isset($items[3])) $items[3]="C";
					//else if(trim($items[3]) == "") $items[3]="C";  
					//else $items[3]=strtoupper($items[3]);
					$rating = "C";																		
					print "<br>insert into territory 
					(agent_id, province, rating, refid, name, PhysicalAddress1, PhysicalAddress2, PhysicalAddress4) 
					values ('".$this->_mysql_escape_mimic(trim($items[0]))."',
							'".$this->_mysql_escape_mimic(trim($items[1]))."',
							'".$this->_mysql_escape_mimic(trim($items[2]))."',
							'".$this->_mysql_escape_mimic(trim($items[3]))."',
							'".$this->_mysql_escape_mimic(trim($items[4]))."',
							'".$this->_mysql_escape_mimic(trim($items[5]))."',
							'".$this->_mysql_escape_mimic(trim($items[6]))."',
							'".$this->_mysql_escape_mimic(trim($items[7]))."'					
							);";
			}
		} else print "NO DATA";
	}
	
	function _mysql_escape_mimic($inp) {
    if(is_array($inp))
        return array_map(__METHOD__, $inp);

    if(!empty($inp) && is_string($inp)) {
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
    }

    return $inp;
} 
}

?>