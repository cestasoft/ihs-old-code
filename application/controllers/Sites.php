<?php

class Sites extends CI_Controller {
		
	function __construct() {    	
		parent::__construct();
    	$this->load->helper(array('form', 'url'));
  	}

	function index() {
		//set agent data
		$this->load->view('authheader');
		if($_SESSION['AGENT']=="YES") {
			$agent_id=$_SESSION['AGENT_ID'];
		} else {
			$agent_id=false;
		}

		//check post data
		$action 	= $this->input->post('action');
		switch($action) {
			case "sites_table"	:	$this->sites_table();break;
			case "sites_save"	:	$this->sites_save();break;
			default 			:	$this->select_sites($agent_id);break;
		}

	}
	
	function save_visit() {
		
		$this->load->view('authheader');
		
		if($_SESSION['AGENT']=="YES") {
			$agent_id = $_SESSION['AGENT_ID'];
		} else {
			$agent_id = $this->input->post('agent_id');
		}
		//$agent_id	= $_SESSION['AGENT_ID'];
		$tid 		= $this->input->post('tid');		
		$date		= $this->input->post('date');
		
		$this->load->model('Sites_model','sites');
		
		//print "We save for date:".$date." agent:".$agent_id." terr.".$tid;
		
		$rs = $this->sites->save_visit($agent_id,$tid,$date);
		
		if(is_numeric($rs) && $rs != 0) {
			print $rs;	
		} else {
			print "0";
		}
	}
	
	function remove_visit() {
		
		$this->load->view('authheader');
		
		$sid 		= $this->input->post('sid');
		
		$this->load->model('Sites_model','sites');
				
		print $this->sites->remove_visit($sid);
	}
	
	function sites_save() {
		$visits 	= $this->input->post('visits');
		
		//$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		
		if(isset($visits)&&is_array($visits)&&count($visits)>0) {
		
			$this->load->model('Sites_model','sites');
			$ret = $this->sites->save_visits($visits);
		
			if($ret===true) {
				$cnt=count($visits);			
				$this->load->view('sites_done',array("number"=>$cnt));
			}
			else $this->load->view('sites_done');
		} else $this->load->view('sites_done');

		$this->load->view('footer');
	
	}

	function select_sites($agent_id) {
		$this->load->model('Sites_model','sites');
		$all_sites = $this->sites->get_all_sites();

		$this->load->model('Agent_model','agent');
		$all_agents=$this->agent->get_all_agents();

		//$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		$data = array("all_sites"=>$all_sites,
						"all_agents"=>$all_agents,
						"agent_id"=>$agent_id);
		$this->load->view('sites', $data);
		$this->load->view('footer');
	}

	function _get_day($i) {
		$ret="";		
		switch($i) {
			case 0:$ret="Sun";break;
			case 1:$ret="Mon";break;
			case 2:$ret="Tue";break;
			case 3:$ret="Wed";break;
			case 4:$ret="Thu";break;
			case 5:$ret="Fri";break;
			case 6:$ret="Sat";break;
			case 7:$ret="Sun";break;
		}
		return $ret;
	}
	
	function sites_table() {
			
		$this->load->helper('date');
		
		$year 		= $this->input->post('year');
		$month 		= $this->input->post('month');
		$agent_id 	= $this->input->post('agentId');
							
		//$this->load->model('Territory_model','terr');
		//$territories=$this->terr->get_all_territories_for_agent($agent_id);
				
		$this->load->model('Sites_model','sites');
		$all_sites = $this->sites->get_site_list($year,$month,$agent_id);
		
		$days_in_month=days_in_month($month,$year);
		
		$weeks=array();
		
		//$day=date("D",mktime(0,0,0,$month,1,$year));
		$day=date("w",mktime(0,0,0,$month,1,$year));
		//$day=1;
		$i=0;
		while($i<$days_in_month) {
			$tmp=array();
			for($j=$day;$j<8&&$i<$days_in_month;$j++) {
				$tmp[]=array("day_of_week"=>$this->_get_day($j),"day_of_month"=>$i);
				$i++;					
			}	
			$day=1;
			$weeks[]=$tmp;			
		}
		
		//$num_weeks=$this->_numWeeks($year,$month);

		//$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		//$data = array("all_sites"=>$all_sites,
		//			"all_agents"=>$all_agents);
		//$this->load->view('sites_table', $data);
		$data = array("weeks"=>$weeks,"month"=>$month,"year"=>$year,"all_sites"=>$all_sites,"agent_id"=>$agent_id);
		$this->load->view('sites_table',$data);
		$this->load->view('footer');
	}

	function _numWeeks($year, $month, $start=0){
		$unix = strtotime("$year-$month-01");
		$numDays = date('t', $unix);
		if ($start===0){
			$dayOne = date('w', $unix); // sunday based week 0-6
		} else {
			$dayOne = date('N', $unix); //monday based week 1-7
			$dayOne--; //convert for 0 based weeks
		}

		//if day one is not the start of the week then advance to start
		$numWeeks = floor(($numDays - (6 - $dayOne))/7);
		return $numWeeks;
	}

}


?>