<?php

class Upload extends CI_Controller {

 	function __construct() () {    
		parent::__construct();	
    	$this->load->helper(array('form', 'url'));
  	}
  	
  	function index() {  		  	
  		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');					
		$this->load->view('upload_form',array('error' => ' '));
		$this->load->library('listfiles', array('txt', 'csv'));
		$data['files'] = $this->listfiles->getFiles('/var/www/system/uploads');

		print_r($data);
		
		$this->load->view('footer');
  	}
  	
}



  