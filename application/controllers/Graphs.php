<?php

class Graphs extends CI_Controller {

	var $graph_list;

	function __construct() {
		 parent::__construct();

		//  require_once('./graphe/jpgraph_line.php');
		//  require_once('./graphe/jpgraph_line.php');
		//  require_once('./graphe/jpgraph_line.php');
		 
		require_once(getcwd() . "/application/libraries/jpgraph/jpgraph.php");
		require_once(getcwd() . "/application/libraries/jpgraph/jpgraph_line.php");
		require_once(getcwd() . "/application/libraries/jpgraph/jpgraph_bar.php");
		 
		$graph_list=array();
		 
		$this->graph_list[1] = array(
			"report_name"     => "Total Site Visits per Agent",
			"report_function" => "total_site_visits_per_agent",
			"report_select_dates" => true,
    		"report_select_agent" => false
		);

		
		 $this->graph_list[2] = array(
		 "report_name"     => "Total Sales per Agent",
		 "report_function" => "total_sales_per_agent",		 
		 "report_select_dates" => true,
		 "report_select_agent" => false
		 );
		 
		 $this->graph_list[3] = array(
		 "report_name"     => "Total Comm per Agent",
		 "report_function" => "total_comm_per_agent",		 
		 "report_select_dates" => true,
		 "report_select_agent" => false
		 );
			
	}
		
	function index() {
		//plz select report
		//click for csv
		//select dates where applicable


		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');

		$this->load->model('Agent_model','agent');
		$all_agents=$this->agent->get_all_agents();

		$this->load->view('graphs/select_graph',$data=array("report_list"=>$this->graph_list,"agents"=>$all_agents));
		$this->load->view('footer');
	}
	
	function total_comm_per_agent($title) {
		$this->total_sales_per_agent($title,true);
	}
	
	function total_sales_per_agent($title,$comm=false) {
		
		
		$from 	= $this->input->post('from');
		$to 	= $this->input->post('to');

		$report_title="Total Sales and Comm per Agent";
		$this->load->model('Agent_model','agent');
		$all_agents=$this->agent->get_all_agents();

		$this->load->model('Product_group_model','pg');
		$all_product_groups=$this->pg->get_all_product_groups();

		$this->load->model('Report_model','report');
		$report_data=$this->report->get_total_sales_and_comm_per_agent($from,$to);
		
		//print_r($report_data);
		//return;		
		
		//We give the graph a name
		$name=time().".png"; 
		
		//We reformat the data for the graph
		$gdata=array();
		$qdata=array();
		$labels=array();
		foreach($report_data as $i) {
			$gdata[]=$i["sales"];
			$qdata[]=$i["comm"];
			$labels[]=$i["name"]." ".$i["surname"];
		}
		
		if(count($qdata)==0) {
			$this->load->view('authheader');
			$this->load->view('header');
			$this->load->view('menu');			
			$this->load->view('error',array('err'=>"No data found for the selected criteria."));		
			$this->load->view('footer');
			return;
		} 
		
		// Create the graph. These two calls are always required
		// Size of graph
		$width=1024;
		$height=768;
		$graph = new Graph($width,$height,'auto');
		$graph->SetScale('textlin');
		$top = 60;
		$bottom = 30;
		$left = 230;
		$right = 30;
		$graph->Set90AndMargin($left,$right,$top,$bottom);
		
		//Add a drop shadow
		$graph->SetShadow();		
		
		// Setup labels
		$graph->xaxis->SetTickLabels($labels);
		//Label align for X-axis
		$graph->xaxis->SetLabelAlign('right','center','right');
		//Label align for Y-axis
		$graph->yaxis->SetLabelAlign('center','bottom');
		
		// Title
		$graph->title->Set($title);
		if($comm===true)					
			$bplot = new BarPlot($qdata);
		else
			$bplot = new BarPlot($gdata);
		//$bplot = new GroupBarPlot(array($gdata,$qdata));
		//Show the values on the graph
		$bplot->SetFillColor('orange');
		$bplot->SetShadow();
		
		$format="%d";
		$bplot->value->SetFormat($format);
		$bplot->value->Show();
		
		$bplot->SetWidth(0.5);
		$bplot->SetYMin(0);
		
		//Fonts
		$bplot->value->SetFont(FF_ARIAL,FS_BOLD,8);
		
		// Create the bar plots
		//$b1plot = new BarPlot($gdata);
		//$b1plot->SetFillColor("orange");
		//$b2plot = new BarPlot($qdata);
		//$b2plot->SetFillColor("blue");

		//Create the grouped bar plot
		//$bplot = new GroupBarPlot(array($b1plot,$b2plot));
		
		
		$graph->title->SetFont(FF_ARIAL,FS_BOLD,18);
		$graph->yaxis->title->SetFont(FF_ARIAL,FS_BOLD,10);
		$graph->xaxis->title->SetFont(FF_ARIAL,FS_BOLD,6);
		
		$graph->yaxis->scale->SetGrace(5);
		$graph->Add($bplot);
		
		// Add the plot to the graph
		//$graph->Add($lineplot);

		// Display the graph
		$graph->Stroke(getcwd() . "/img/graphs/".$name);

		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		//http://localhost/img/graphs/1286607662.png
		$this->load->view('graphs/show_graph',array("name"=>$name,"title"=>$title));
		$this->load->view('footer');		
	}

	function total_site_visits_per_agent($title) {
		$this->load->model('Sites_model','sites');
		
		$from 	= $this->input->post('from');
		$to 	= $this->input->post('to');

		$data=$this->sites->get_site_visits_by_agent($from,$to);
		
		if(count($data)==0) {
			$this->load->view('authheader');
			$this->load->view('header');
			$this->load->view('menu');			
			$this->load->view('error',array('err'=>"No data found for the selected criteria."));		
			$this->load->view('footer');
			return;
		} 
		
		//We give the graph a name
		$name=time().".png"; 
		
		//We reformat the data for the graph
		$gdata=array();
		$labels=array();
		foreach($data as $i) {
			$gdata[]=$i["cnt"];
			$labels[]=$i["name"]." ".$i["surname"];
		}
		
		// Create the graph. These two calls are always required
		// Size of graph
		$width=1024;
		$height=768;
		$graph = new Graph($width,$height,'auto');
		$graph->SetScale('textlin');
		$top = 60;
		$bottom = 30;
		$left = 230;
		$right = 30;
		$graph->Set90AndMargin($left,$right,$top,$bottom);
		
		//Add a drop shadow
		$graph->SetShadow();		
		
		// Setup labels
		$graph->xaxis->SetTickLabels($labels);
		//Label align for X-axis
		$graph->xaxis->SetLabelAlign('right','center','right');
		//Label align for Y-axis
		$graph->yaxis->SetLabelAlign('center','bottom');
		
		// Title
		$graph->title->Set($title);
				

		$bplot = new BarPlot($gdata);
		//Show the values on the graph
		$bplot->SetFillColor('orange');
		$bplot->SetShadow();
		
		$format="%d";
		$bplot->value->SetFormat($format);
		$bplot->value->Show();
		
		$bplot->SetWidth(0.5);		
		$bplot->SetYMin(0);
		
		//Fonts
		$bplot->value->SetFont(FF_ARIAL,FS_BOLD,8);
		$graph->title->SetFont(FF_ARIAL,FS_BOLD,18);
		$graph->yaxis->title->SetFont(FF_ARIAL,FS_BOLD,10);
		$graph->xaxis->title->SetFont(FF_ARIAL,FS_BOLD,6);
		
		$graph->yaxis->scale->SetGrace(5);
		$graph->Add($bplot);
		
		// Add the plot to the graph
		//$graph->Add($lineplot);

		// Display the graph
		$graph->Stroke(getcwd() . "/img/graphs/".$name);

		$this->load->view('authheader');
		$this->load->view('header');
		$this->load->view('menu');
		//http://localhost/img/graphs/1286607662.png
		$this->load->view('graphs/show_graph',array("name"=>$name,"title"=>$title));
		$this->load->view('footer');
	}

	function show_graph() {


		$report	= $this->input->post('report');

		//$agent	= $this->input->post('agent');

		if(isset($this->graph_list[$report])) {
			//ref: http://php.net/manual/en/functions.variable-functions.php
			$function=$this->graph_list[$report]["report_function"];
			$this->$function($this->graph_list[$report]["report_name"]);
		}




	}

	function total_sales_and_comm_per_agent($csv=false) {

		$from 	= $this->input->post('from');
		$to 	= $this->input->post('to');

		$report_title="Total Sales and Comm per Agent";
		$this->load->model('Agent_model','agent');
		$all_agents=$this->agent->get_all_agents();

		$this->load->model('Product_group_model','pg');
		$all_product_groups=$this->pg->get_all_product_groups();

		$this->load->model('Report_model','report');
		$report_data=$this->report->get_total_sales_and_comm_per_agent($from,$to);

		//print_r($report_data);

		////Create Headings
		$report_headings=array("Agent","Sales Amount","Comm Amount");

		//Create Row Data
		$report_rows=array();
		foreach($report_data as $i) {
			$report_rows[]=array($i["name"]." ".$i["surname"],number_format($i["sales"],2),number_format($i["comm"],2));
		}

		//Calculate Totals and Add to Row data
		$report_totals=array();
		for($i=0;$i<count($report_rows);$i++) {
			for($j=0;$j<count($report_rows[$i]);$j++) {
				if(is_numeric($report_rows[$i][$j])) {
					if(isset($report_totals[$j])&&$report_totals[$j]>=0) $report_totals[$j]+=$report_rows[$i][$j];
					else $report_totals[$j]=$report_rows[$i][$j];
				}
				else {
					if(isset($report_totals[$j])&&$report_totals[$j]>=0) ;
					else $report_totals[$j]=0;
				}
			}
		}
		$report_totals[0]="Totals:";
		$cnt=count($report_rows);
		$report_rows[$cnt]=$report_totals;

		//print_r($report_rows);
		$data = array("report_title"=>$report_title,
					"report_headings"=>$report_headings,
					"report_rows"=>$report_rows
		);

		$this->load->view('authheader');
		if($csv===true) {
			$this->_process_data($report_title,$report_headings,$report_rows);
		}
		else {
			$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('reports/standard', $data);
			$this->load->view('footer');
		}
	}

	function total_sales_report() {
		/*
		 * select sum(productsales.saleamount) as sales,
			sum(productsales.saleamount * product.comm/100) as comm,
			agent.agentcode as agent_code,
			agent.name as name,
			agent.surname as surname,
			territory.name as terr_name
			from productsales, product, agent, territory
			where datesold >= '2008/10/01 00:00:00' and datesold <= '2008/11/01 23:59:59'
			and productsales.product_id = product.id
			and productsales.Territory_Id = territory.Id
			and territory.agent_id = agent.agentcode
			group by agent.agentcode,territory.id
			order by name asc,surname,terr_name
		 */
	}

	function territories_per_agent() {

	}

	//also do this per agent
	function site_visits_per_agent($csv=false) {
		//get_site_visits_agent

		$from 	= $this->input->post('from');
		$to 	= $this->input->post('to');
		$agent	= $this->input->post('agent');

		$report_title="Site Visits per Agent";
		//$this->load->model('Agent_model','agent');
		//$all_agents=$this->agent->get_all_agents();

		//$this->load->model('Product_group_model','pg');
		//$all_product_groups=$this->pg->get_all_product_groups();

		$this->load->model('Report_model','report');
		$report_data=$this->report->get_site_visits_agent($from,$to,$agent);

		//print_r($report_data);

		////Create Headings
		$report_headings=array("Agent","Date Visited","Territory");

		//Create Row Data
		$report_rows=array();
		foreach($report_data as $i) {
			$report_rows[]=array($i["name"]." ".$i["surname"],substr($i["datevisited"],0,10),$i["t_name"]);
		}

		//print_r($report_rows);
		$data = array("report_title"=>$report_title,
					"report_headings"=>$report_headings,
					"report_rows"=>$report_rows
		);

		$this->load->view('authheader');
		if($csv===true) {
			$this->_process_data($report_title,$report_headings,$report_rows);
		}
		else {
			$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('reports/standard', $data);
			$this->load->view('footer');
		}

	}


	function agent_comm_per_product_group($csv=false) {

		$from 	= $this->input->post('from');
		$to 	= $this->input->post('to');

		$report_title="Agent comm per product group";
		$this->load->model('Agent_model','agent');
		$all_agents=$this->agent->get_all_agents();

		$this->load->model('Product_group_model','pg');
		$all_product_groups=$this->pg->get_all_product_groups();

		$this->load->model('Report_model','report');
		$report_data=$this->report->get_agent_comm_per_product_group($from,$to);

		////Create Headings
		$report_headings=array();
		$report_headings[0]="";
		foreach($all_product_groups as $product_group) {
			$report_headings[]=$product_group["name"];
		}

		////Create Row Data
		$report_rows=array();
		$l=0;
		foreach($all_agents as $agent) {
			$report_rows[$l][0]=$agent["name"]." ".$agent["surname"];
			$k=1;
			foreach($all_product_groups as $product_group) {
				if(isset($report_data[$product_group["id"]])&&isset($report_data[$product_group["id"]][$agent["agentcode"]])) {
					$report_rows[$l][$k]=$report_data[$product_group["id"]][$agent["agentcode"]]["comm"];
					$report_rows[$l][$k]=number_format($report_rows[$l][$k],2);
					//print "".number_format($report_data[$product_group["id"]][$agent["agentcode"]]["comm"],2)."";
				}
				else $report_rows[$l][$k]="0";
				$k++;
			}
			$l++;
		}

		//Calculate Totals and Add to Row data
		$report_totals=array();
		for($i=0;$i<count($report_rows);$i++) {
			for($j=0;$j<count($report_rows[$i]);$j++) {
				if(is_numeric($report_rows[$i][$j])) {
					if(isset($report_totals[$j])&&$report_totals[$j]>=0) $report_totals[$j]+=$report_rows[$i][$j];
					else $report_totals[$j]=$report_rows[$i][$j];
				}
				else {
					if(isset($report_totals[$j])&&$report_totals[$j]>=0) ;
					else $report_totals[$j]=0;
				}
			}
		}

		$report_totals[0]="Totals:";
		$cnt=count($report_rows);
		$report_rows[$cnt]=$report_totals;

		$data = array("report_title"=>$report_title,
					"report_headings"=>$report_headings,
					"report_rows"=>$report_rows
		);

		$this->load->view('authheader');
		if($csv===true) {
			$this->_process_data($report_title,$report_headings,$report_rows);
		}
		else {
			$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('reports/standard', $data);
			$this->load->view('footer');
		}
	}

	function _process_data($report_title,$report_headings,$report_rows) {
		$csv_data="";
		foreach($report_headings as $i) {
			$csv_data.='"'.$i.'",';
		}
		foreach($report_rows as $i) {
			$csv_data.="\n";
			foreach($i as $j) {
				$csv_data.='"'.$j.'",';
			}
		}

		$this->_download_csv($csv_data,$report_title);
	}

	function _download_csv($data,$filename) {
		$this->output->set_header("Content-Type: text/csv");
		//replace  space with underscore
		//add time and .csv
		$filename=str_replace(" ","_",$filename);
		$filename=$filename.time().".csv";
			
		$this->output->set_header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		$this->output->set_header("Expires: 0");
		//IE is the biggest pile of @##$@#@@#@$
		if (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
		{
			$this->output->set_header('Content-Disposition: inline; filename="'.$filename.'"');
			$this->output->set_header('Pragma: public');
		}
		else
		{
			$this->output->set_header('Content-Disposition: attachment; filename="'.$filename.'"');
			$this->output->set_header('Pragma: no-cache');
		}
		$this->output->set_output($data);
	}
}